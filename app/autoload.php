<?php

	use Doctrine\Common\Annotations\AnnotationRegistry;
	use Composer\Autoload\ClassLoader;

	/**
	 * @var ClassLoader $loader
	 */
	$loader = require __DIR__ . '/../vendor/autoload.php';

	// TODO: a commenter en production
	//"uneak/admin-bundle": "dev-master"
	//	$loader->set('Uneak\\AdminBundle', array('/Users/marc/Workspace/uneak/adminBundle'));

	//"uneak/flatskin-bundle": "dev-master"
	//	$loader->set('Uneak\\FlatSkinBundle', array('/Users/marc/Workspace/uneak/flatSkinBundle'));


//	$loader->set('Uneak\\AssetsManagerBundle', array('/Users/marc/Workspace/uneak/assetsManagerBundle'));
//	$loader->set('Uneak\\BlocksManagerBundle', array('/Users/marc/Workspace/uneak/blocksManagerBundle'));
//	$loader->set('Uneak\\FormsManagerBundle', array('/Users/marc/Workspace/uneak/formsManagerBundle'));
//	$loader->set('Uneak\\RoutesManagerBundle', array('/Users/marc/Workspace/uneak/routesManagerBundle'));
//
//	$loader->set('Uneak\\FlatSkinBundle', array('/Users/marc/Workspace/uneak/flatSkinBundle'));

	AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

	return $loader;
