<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel {

	public function registerBundles() {
		$bundles = array(

			//	SYMFONY
			new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
			new Symfony\Bundle\SecurityBundle\SecurityBundle(),
			new Symfony\Bundle\TwigBundle\TwigBundle(),
			new Symfony\Bundle\MonologBundle\MonologBundle(),
			new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
			new Symfony\Bundle\AsseticBundle\AsseticBundle(),
			new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
			new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
			// FRONTAL ASSETS,
			new Sp\BowerBundle\SpBowerBundle(),
			new Braincrafted\Bundle\BootstrapBundle\BraincraftedBootstrapBundle(),
			// FRONTAL INTEGRATION,
			new Liip\ImagineBundle\LiipImagineBundle(),
			new Knp\Bundle\MenuBundle\KnpMenuBundle(),
			new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
			// USER,
			new FOS\UserBundle\FOSUserBundle(),
			// DOCTRINE,
			new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
			new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
			new Vich\UploaderBundle\VichUploaderBundle(),
			// ADMIN,
			new Uneak\AssetsManagerBundle\UneakAssetsManagerBundle(),
			new Uneak\FormsManagerBundle\UneakFormsManagerBundle(),
			new Uneak\BlocksManagerBundle\UneakBlocksManagerBundle(),
			new Uneak\RoutesManagerBundle\UneakRoutesManagerBundle(),
			new Uneak\FlatSkinBundle\UneakFlatSkinBundle(),
			// OWN,
			new AppBundle\AppBundle(),
            new UserBundle\UserBundle(),
            new LocalisationBundle\LocalisationBundle(),
            new PosteBundle\PosteBundle(),
            new AgenceBundle\AgenceBundle(),
            new CollaborateurBundle\CollaborateurBundle(),
            new ProjetBundle\ProjetBundle(),
            new LotBundle\LotBundle(),
            new GalleryBundle\GalleryBundle(),
            new NewsBundle\NewsBundle(),
            new HomepageBundle\HomepageBundle(),
            new NavigationBundle\NavigationBundle(),
            new ContactBundle\ContactBundle(),
            new PropositionBundle\PropositionBundle(),
            new SliderBundle\SliderBundle(),
            new VideosBundle\VideosBundle(),
            new Uneak\CKFinderBundle\UneakCKFinderBundle(),
            new Uneak\GoogleMapBundle\UneakGoogleMapBundle(),
		);

		if (in_array($this->getEnvironment(), array('dev', 'test'))) {
			$bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
			$bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
			$bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
			$bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
			$bundles[] = new RaulFraile\Bundle\LadybugBundle\RaulFraileLadybugBundle();
		}

		return $bundles;
	}

	public function registerContainerConfiguration(LoaderInterface $loader) {
		$loader->load(__DIR__ . '/config/config_' . $this->getEnvironment() . '.yml');
	}

}
