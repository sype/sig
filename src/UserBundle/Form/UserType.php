<?php

	namespace UserBundle\Form;

	use UserBundle\Validator\Password;
	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\FormEvent;
	use Symfony\Component\Form\FormEvents;
	use Symfony\Component\Form\FormInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Callback;
	use Symfony\Component\Validator\Constraints\Collection;
	use Symfony\Component\Validator\Context\ExecutionContextInterface;

	class UserType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('username', null, array(
					'label' => "Nom d'utilisateur",
				))
				->add('gender', 'gender', array(
					'label' => "Civilité",
					'empty_value' => 'Sectionnez votre civilité',
				))

				->add('firstName', null, array(
					'label' => "Prénom",
				))
				->add('lastName', null, array(
					'label' => "Nom",
				))
				->add('imageFile', 'vich_file', array(
						'label'         => "Photo",
						'required'      => false,
						'allow_delete'  => true, // not mandatory, default is true
						'download_link' => true, // not mandatory, default is true
						'required'      => false,
					)
				)
//				->add('imageFile', 'file_picker', array(
//						'label'         => "Photo",
//						'required'      => false,
//					)
//				)
				->add('email', null, array(
					'label' => "Email",
				))


				->add('enabled', null, array(
					'label' => "Activé",
					'required'      => false,
				));


			$builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'addPasswordField'));
		}


		public function addPasswordField(FormEvent $event) {
			$user = $event->getData();
			$form = $event->getForm();

			if ($user && $user->getId() !== null) {
				$form->add('newPassword', 'repeated', array(
					'type'            => 'password',
					'invalid_message' => 'Les mots de passe doivent correspondre',
					'first_options'   => array('label' => 'Nouveau du mot de passe'),
					'second_options'  => array('label' => 'Confirmation du mot de passe'),
					'required'        => false,

				));

				$form->add('plainPassword', 'password', array(
					'label'    => "Confirmation de l'ancien mot de passe",
					'required' => false,
				));

			} else {
				$form->add('plainPassword', 'password', array(
					'label'    => "Mot de passe",
					'required' => false,
				));
			}
		}


		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'UserBundle\Entity\User',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'userbundle_user';
		}
	}
