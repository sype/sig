<?php

	namespace UserBundle\Entity;

	use Doctrine\ORM\Mapping as ORM;
	use FOS\UserBundle\Model\GroupInterface;

	/**
	 * User
	 *
	 * @ORM\Table(name="FOS_Group")
	 * @ORM\Entity(repositoryClass="UserBundle\Entity\GroupRepository")
	 *
	 */
	class Group implements GroupInterface {

		/**
		 * @var integer
		 *
		 * @ORM\Column(name="id", type="integer")
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="AUTO")
		 */
		protected $id;

		/**
		 * @ORM\Column(name="name", type="string", length=64)
		 */
		protected $name;

		/**
		 * @ORM\Column(name="roles", type="array")
		 */
		protected $roles;

		public function __construct() {
			$this->roles = array();
		}

		/**
		 * @param string $role
		 *
		 * @return Group
		 */
		public function addRole($role) {
			if (!$this->hasRole($role)) {
				$this->roles[] = strtoupper($role);
			}

			return $this;
		}

		/**
		 * @param string $role
		 *
		 * @return boolean
		 */
		public function hasRole($role) {
			return in_array(strtoupper($role), $this->roles, true);
		}

		public function getId() {
			return $this->id;
		}

		public function getRoles() {
			return $this->roles;
		}

		/**
		 * @param array $roles
		 *
		 * @return Group
		 */
		public function setRoles(array $roles) {
			$this->roles = $roles;

			return $this;
		}

		/**
		 * @param string $role
		 *
		 * @return Group
		 */
		public function removeRole($role) {
			if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
				unset($this->roles[$key]);
				$this->roles = array_values($this->roles);
			}

			return $this;
		}

		public function getEntityName() {
			return $this->getName();
		}

		public function getName() {
			return $this->name;
		}

		//

		/**
		 * @param string $name
		 *
		 * @return Group
		 */
		public function setName($name) {
			$this->name = $name;

			return $this;
		}

		public function getEntityDescription() {
			return null;
		}

		public function getEntityPhoto() {
			return null;
		}

	}
