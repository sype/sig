<?php

	namespace UserBundle\Validator\Constraints;

	use Symfony\Component\Validator\Constraint;

	/**
	 * @Annotation
	 */
	class Password extends Constraint {

		public function validatedBy() {
			return 'password';
		}

		public function getTargets() {
			return self::CLASS_CONSTRAINT;
		}

	}
