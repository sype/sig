<?php

	namespace UserBundle\Validator\Constraints;

	use FOS\UserBundle\Model\UserManagerInterface;
	use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
	use Symfony\Component\Validator\Constraint;
	use Symfony\Component\Validator\ConstraintValidator;

	/**
	 * @Annotation
	 */
	class PasswordChangeValidator extends ConstraintValidator {

		private $userManager;
		private $encoderFactory;

		public function __construct(UserManagerInterface $userManager, EncoderFactoryInterface $encoderFactory) {
			$this->userManager = $userManager;
			$this->encoderFactory = $encoderFactory;
		}

		public function validate($entity, Constraint $constraint) {

			if ($entity->getId()) {

				if ($entity->getPlainPassword() xor $entity->getNewPassword()) {
					if (!$entity->getPlainPassword()) {
						$this->context->addViolationAt('plainPassword', 'Pour modifier le mot de passe, vous devez renseigner votre ancien mot de passe');
					} else {
						$this->context->addViolationAt('newPassword', 'Pour modifier le mot de passe, vous devez renseigner un nouveau mot de passe');
					}
				}

				if ($entity->getPlainPassword()) {

//					// TODO: Password encoder : Symfony 2.5 >> Symfony 2.6
//					// Symfony 2.5
//					$user = new Acme\UserBundle\Entity\User();
//					$factory = $this->container->get('security.encoder_factory');
//					$encoder = $factory->getEncoder($user);
//					$password = $encoder->encodePassword($plainTextPassword, $user->getSalt());
//
//					// Symfony 2.6
//					$user = new Acme\UserBundle\Entity\User();
//					$encoder = $this->container->get('security.password_encoder');
//					$password = $encoder->encodePassword($user, $plainTextPassword);


					$user = $this->userManager->findUserBy(array('id' => $entity->getId()));
					$encoder = $this->encoderFactory->getEncoder($user);
					$old_pwd_encoded = $encoder->encodePassword($entity->getPlainPassword(), $user->getSalt());

					if ($user->getPassword() != $old_pwd_encoded) {
						$this->context->addViolationAt('plainPassword', 'Le mot de passe est incorrect');
					}
				}
			}

		}

	}
