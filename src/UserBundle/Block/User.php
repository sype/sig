<?php

	namespace UserBundle\Block;


	use Doctrine\ORM\EntityManager;
	use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
	use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
	use Uneak\FlatSkinBundle\Block\Info\Info;
	use Uneak\FlatSkinBundle\Block\Info\Infos;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\ProfileNav;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Uneak\FlatSkinBundle\Block\Stat\Stat;
	use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

	class User {

		private $blockManager;
		private $routeManager;
		private $menuHelper;
		private $vichHelper;
		private $autorization;
		private $em;

		public function __construct(Block $blockManager, FlattenRouteManager $routeManager, MenuHelper $menuHelper, UploaderHelper $vichHelper, AuthorizationChecker $autorization, EntityManager $em) {
			$this->blockManager = $blockManager;
			$this->routeManager = $routeManager;
			$this->menuHelper = $menuHelper;
			$this->vichHelper = $vichHelper;
			$this->autorization = $autorization;
			$this->em = $em;
		}


		public function getInfos($user) {

			$infos = new Infos();
			$infos
				->setColumns(1)
				->setStripeRow(true)
				->addBlock(new Info("Prenom", $user->getFirstName()), null, 60)
				->addBlock(new Info("Nom", $user->getLastName()), null, 50)
				->addBlock(new Info("Utilisateur", $user->getUserName()), null, 40)
				->addBlock(new Info("E-mail", $user->getEmail()), null, 30)
				->addBlock(new Info("Date de création", $user->getCreatedAt()->format('d/m/Y H:i')), null, 20)
				->addBlock(new Info("Compte actif", ($user->isEnabled()) ? "Oui" : "Non"), null, 10);
			if ($user->getLastLogin()) {
				$infos->addBlock(new Info("Dernière connection", $user->getLastLogin()->format('d/m/Y H:i'), null, 0));
			}

			$wrapper = new Wrapper();
			$wrapper
				->setTitle("Informations personelle")
				->addBlock($infos);

			return $wrapper;
		}



		public function getNavigation() {

			$flattenCrud = $this->routeManager->getFlattenRoute('user');

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($flattenCrud->getMetaData("_label"))
				->setDescription($flattenCrud->getMetaData("_description"))
				->setColor("green")
				->setHeaderAlign('vertical');
			$profileNav->addBlock($this->getNavigationMenu());

			return $profileNav;

		}

		public function getNavigationMenu() {

			$flattenCrud = $this->routeManager->getFlattenRoute('user');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud);
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}

		public function getUserNavigation($user) {

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($user->getFirstName() . " " . $user->getLastName())
				->setDescription($user->getEmail())
				->setColor("green")
				->setPhoto($this->vichHelper->asset($user, 'imageFile'))
				->setHeaderAlign('horizontal');
			$profileNav->addBlock($this->getUserNavigationMenu($user));

			return $profileNav;

		}

		public function getUserNavigationMenu($user) {

			$flattenCrud = $this->routeManager->getFlattenRoute('user/subject');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('user' => $user->getId()));
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}



	}
