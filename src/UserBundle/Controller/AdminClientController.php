<?php

	namespace UserBundle\Controller;

	use UserBundle\Entity\User;
	use UserBundle\Form\UserNewType;
	use UserBundle\Form\UserType;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Request;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoute;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
	use Uneak\FlatSkinBundle\Block\Component\DataTable;
	use Uneak\FlatSkinBundle\Block\DataTable\DataTableFilters;
	use Uneak\FlatSkinBundle\Block\Form\Form;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Doctrine\ORM\Query\Expr;

	class AdminClientController extends Controller {


		public function indexAction( FlattenRoute $route) {
			$blockManager = $this->get("uneak.blocksmanager");

			$userBlock = $this->get('uneak.admin.user.block.helper');
			$blockManager->addBlock($userBlock->getNavigation(), 'navigation');

			$gridNested = $route->getNestedRoute();

			$datatable = new DataTable();
			$datatable->setAjax($route->getChild('_grid')->getRoutePath());
			$datatable->setColumns($gridNested->getColumns());

			$datatableWrapper = new Wrapper();
			$datatableWrapper
				->setTitle("Resultats")
				->setCollapsable(false)
				->addBlock($datatable);
			$blockManager->addBlock($datatableWrapper, 'datatable');

			$datatableFilterWrapper = new Wrapper();
			$datatableFilterWrapper
				->setTitle("Filtres")
				->setCollapsable(true)
				->addBlock(new DataTableFilters($datatable));
			$blockManager->addBlock($datatableFilterWrapper, 'datatable_filters');

			return $this->render('UserBundle:Admin:index.html.twig');

		}



		public function showAction($user) {
			$blockManager = $this->get("uneak.blocksmanager");

			$userBlock = $this->get('uneak.admin.user.block.helper');

			$blockManager->addBlock($userBlock->getInfos($user), 'infos');
			$blockManager->addBlock($userBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($userBlock->getUserNavigation($user), 'user_navigation');

			return $this->render('UserBundle:Admin:show.html.twig');

		}


		public function editAction($user, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");


			$userBlock = $this->get('uneak.admin.user.block.helper');
			$blockManager->addBlock($userBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($userBlock->getUserNavigation($user), 'user_navigation');

			$form = $this->createForm(new UserType(), $user);
			$form->add('submit', 'submit', array('label' => 'Modifier'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

					if ($user->getNewPassword()) {
						$user->setPlainPassword($user->getNewPassword());
					}

					$userManager = $this->get('fos_user.user_manager');
					$userManager->updateUser($user);

					return $this->redirect($route->getChild('*/subject/show', array('user' => $user->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}




			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("UserBundle:Block:Form/block_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Formulaire d'édition")
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('UserBundle:Admin:edit.html.twig');

		}


		public function newAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$userBlock = $this->get('uneak.admin.user.block.helper');
			$blockManager->addBlock($userBlock->getNavigation(), 'navigation');

			$user = new User();

			$form = $this->createForm(new UserType(), $user);
			$form->add('submit', 'submit', array('label' => 'Creer'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

                    $user->setRoles(array("ROLE_SUPER_ADMIN"));
					$userManager = $this->get('fos_user.user_manager');
					$userManager->updateUser($user);

					return $this->redirect($route->getChild('*/subject/show', array('user' => $user->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("UserBundle:Block:Form/block_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Formulaire de création")
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');

			return $this->render('UserBundle:Admin:new.html.twig');


		}


		public function deleteAction($user, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$userBlock = $this->get('uneak.admin.user.block.helper');
			$blockManager->addBlock($userBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($userBlock->getUserNavigation($user), 'user_navigation');

			$form = $this->createFormBuilder(array());
			$form->add('cancel', 'submit', array('label' => 'Annuler'));
			$form->add('confirm', 'submit', array('label' => 'Confirmer'));
			$form = $form->getForm();


			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');

				$form->handleRequest($request);
				if ($form->isValid()) {
					if ($form->get('cancel')->isClicked()) {
						$flash->info('Suppression annulée');

						return $this->redirect($route->getChild('*/subject/show', array('user' => $user->getId()))->getRoutePath());
					}
					if ($form->get('confirm')->isClicked()) {

						$userManager = $this->get('fos_user.user_manager');
						$user = $userManager->findUserBy(array('id' => $user->getId()));
						$userManager->deleteUser($user);

						$flash->success('La suppression à été réalisée avec succès.');

						return $this->redirect($route->getChild('*/index')->getRoutePath());
					}

				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formBlock = new Form($form->createView());
			$formBlock->setTemplate("UserBundle:Block:Form/delete_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Formulaire de suppression")
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('UserBundle:Admin:delete.html.twig');

		}




		public function indexGridAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$gridHelper = $this->get("uneak.routesmanager.grid.helper");
			$menuHelper = $this->get("uneak.routesmanager.menu.helper");

			$params = $request->query->all();

			$gridData = $gridHelper->gridFields($gridHelper->createGridQueryBuilder('UserBundle\Entity\User', $params), $params);
			$recordsTotal = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('UserBundle\Entity\User', $params));
			$recordsFiltered = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('UserBundle\Entity\User', $params));


			$rowMenuTemplate = "UneakFlatSkinBundle:Block:Menu/block_menu.html.twig";

			$data = array();
			foreach ($gridData as $object) {
				$row = array();
				foreach ($params['columns'] as $columns) {
					if ($columns['name'] && substr($columns['name'], 0, 1) != '_') {
						$value = $object[str_replace(".", "_", $columns['name'])];
						if ($value instanceof \DateTime) {
							$value = $value->format('d/m/Y H:m:s');
						}
						$row[$columns['data']] = $value;
					} else {
						$row[$columns['data']] = "";
					}
				}
				$row['DT_RowId'] = $object['DT_RowId'];



				$menu = new Menu();
				$menu->setTemplate($rowMenuTemplate);
				$rowActions = $route->getParent()->getNestedRoute()->getRowActions();
				$root = $menuHelper->createMenu($rowActions, $route, array('user' => $row['DT_RowId']));
				$root->setChildrenAttribute('class', 'nav nav-pills nav-inline text-nowrap');
				$menu->setRoot($root);

				$blockManager->addBlock($menu, 'rowMenu');
				$row['_actions'] = $this->renderView("{{ renderBlock('rowMenu') }}");

				array_push($data, $row);
			}


			return new JsonResponse(array(
				'draw'            => $params["draw"],
				'recordsTotal'    => $recordsTotal,
				'recordsFiltered' => $recordsFiltered,
				'data'            => $data,
			));
		}


	}
