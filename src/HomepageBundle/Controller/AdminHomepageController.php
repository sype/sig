<?php

	namespace HomepageBundle\Controller;

	use HomepageBundle\Entity\Homepage;
    use HomepageBundle\Entity\Homepages;
    use HomepageBundle\Entity\HomepagesRepository;
    use HomepageBundle\Form\HomepageType;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoute;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
	use Uneak\FlatSkinBundle\Block\Component\DataTable;
	use Uneak\FlatSkinBundle\Block\DataTable\DataTableFilters;
	use Uneak\FlatSkinBundle\Block\Form\Form;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Doctrine\ORM\Query\Expr;

	class AdminHomepageController extends Controller {



		public function editAction(FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

            $em = $this->getDoctrine()->getManager();
            $homepage = $em->getRepository('HomepageBundle:Homepage')->findHomepage();


			$form = $this->createForm(new HomepageType(), $homepage);
			$form->add('submit', 'submit', array('label' => 'Modifier'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {


                    $em->flush();

					return $this->redirect($route->getChild('*/edit')->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("HomepageBundle:Block:Form/block_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Edition de la video")
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('HomepageBundle:Admin:edit.html.twig');

		}



	}
