<?php

	namespace HomepageBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;

	class HomepageType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('label', null, array(
					'label' => "Titre",
				))
                ->add('description', null, array(
					'label' => "Sous-titre",
				))
                ->add('video', null, array(
					'label' => "Video",
				))
            ;

		}


		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'HomepageBundle\Entity\Homepage',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'homepagebundle_homepage';
		}
	}
