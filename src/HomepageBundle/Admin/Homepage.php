<?php

	namespace HomepageBundle\Admin;

	use Uneak\RoutesManagerBundle\Routes\NestedAdminRoute;
	use Uneak\RoutesManagerBundle\Routes\NestedCRUDRoute;
	use Uneak\RoutesManagerBundle\Routes\NestedEntityRoute;
	use Uneak\RoutesManagerBundle\Routes\NestedGridRoute;

	class Homepage extends NestedCRUDRoute {

		protected $entity = 'HomepageBundle\Entity\Homepage';

		public function initialize() {
			parent::initialize();

			$this->setMetaData('_icon', 'youtube-play');
			$this->setMetaData('_label', "Vidéo");
			$this->setMetaData('_description', "Vidéo de la page d'accueil");
		}


		protected function buildCRUD() {
			$editRoute = new NestedAdminRoute('edit');
			$editRoute
				->setAction('edit')
				->setMetaData('_icon', 'edit')
				->setMetaData('_label', 'Edit');
			$this->addChild($editRoute);
		}


	}
