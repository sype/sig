<?php

	namespace GalleryBundle\Controller;

	use GalleryBundle\Entity\GalleryType;
    use GalleryBundle\Entity\GalleryTypeRepository;
    use GalleryBundle\Form\GalleryTypeType;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoute;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
	use Uneak\FlatSkinBundle\Block\Component\DataTable;
	use Uneak\FlatSkinBundle\Block\DataTable\DataTableFilters;
	use Uneak\FlatSkinBundle\Block\Form\Form;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Doctrine\ORM\Query\Expr;

	class AdminGalleryTypeController extends Controller {


		public function indexAction( FlattenRoute $route) {
			$blockManager = $this->get("uneak.blocksmanager");

			$userBlock = $this->get('uneak.admin.gallery.type.block.helper');
			$blockManager->addBlock($userBlock->getNavigation(), 'navigation');

			$gridNested = $route->getNestedRoute();

			$datatable = new DataTable();
			$datatable->setAjax($route->getChild('_grid')->getRoutePath());
			$datatable->setColumns($gridNested->getColumns());

			$datatableWrapper = new Wrapper();
			$datatableWrapper
				->setTitle("Resultats")
				->setCollapsable(false)
				->addBlock($datatable);
			$blockManager->addBlock($datatableWrapper, 'datatable');

			$datatableFilterWrapper = new Wrapper();
			$datatableFilterWrapper
				->setTitle("Filtres")
				->setCollapsable(true)
				->addBlock(new DataTableFilters($datatable));
			$blockManager->addBlock($datatableFilterWrapper, 'datatable_filters');

			return $this->render('GalleryBundle:Admin:GalleryType/index.html.twig');

		}



		public function showAction($gallerytype) {
			$blockManager = $this->get("uneak.blocksmanager");

			$userBlock = $this->get('uneak.admin.gallery.type.block.helper');

			$blockManager->addBlock($userBlock->getInfos($gallerytype), 'infos');
			$blockManager->addBlock($userBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($userBlock->getGalleryTypeNavigation($gallerytype), 'user_navigation');

			return $this->render('GalleryBundle:Admin:GalleryType/show.html.twig');

		}


		public function editAction($gallerytype, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");


			$userBlock = $this->get('uneak.admin.gallery.type.block.helper');
			$blockManager->addBlock($userBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($userBlock->getGalleryTypeNavigation($gallerytype), 'user_navigation');

			$form = $this->createForm(new GalleryTypeType(), $gallerytype);
			$form->add('submit', 'submit', array('label' => 'Modifier'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);

				if ($form->isValid()) {

//					ldd($gallerytype);
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

					return $this->redirect($route->getChild('*/subject/show', array('gallerytype' => $gallerytype->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("GalleryBundle:Block:Form/block_gallery_type_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Edition du type de galerie ".$gallerytype->getLabel())
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('GalleryBundle:Admin:GalleryType/edit.html.twig');

		}


		public function newAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$userBlock = $this->get('uneak.admin.gallery.type.block.helper');
			$blockManager->addBlock($userBlock->getNavigation(), 'navigation');

			$gallerytype = new GalleryType();

			$form = $this->createForm(new GalleryTypeType(), $gallerytype);
			$form->add('submit', 'submit', array('label' => 'Creer'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($gallerytype);
                    $em->flush();

					return $this->redirect($route->getChild('*/subject/show', array('gallerytype' => $gallerytype->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("GalleryBundle:Block:Form/block_gallery_type_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Création d'un type de galerie")
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');

			return $this->render('GalleryBundle:Admin:GalleryType/new.html.twig');


		}


		public function deleteAction($gallerytype, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$userBlock = $this->get('uneak.admin.gallery.type.block.helper');
			$blockManager->addBlock($userBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($userBlock->getGalleryTypeNavigation($gallerytype), 'user_navigation');

			$form = $this->createFormBuilder(array());
			$form->add('cancel', 'submit', array('label' => 'Annuler'));
			$form->add('confirm', 'submit', array('label' => 'Confirmer'));
			$form = $form->getForm();


			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');

				$form->handleRequest($request);
				if ($form->isValid()) {
					if ($form->get('cancel')->isClicked()) {
						$flash->info('Suppression annulée');

						return $this->redirect($route->getChild('*/subject/show', array('gallerytype' => $gallerytype->getId()))->getRoutePath());
					}
					if ($form->get('confirm')->isClicked()) {

                        $em = $this->getDoctrine()->getManager();
                        $em->remove($gallerytype);
                        $em->flush();

						$flash->success('La suppression à été réalisée avec succès.');

						return $this->redirect($route->getChild('*/index')->getRoutePath());
					}

				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formBlock = new Form($form->createView());
			$formBlock->setTemplate("GalleryBundle:Block:Form/delete_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Suppression du type de galerie ".$gallerytype->getLabel())
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('GalleryBundle:Admin:GalleryType/delete.html.twig');

		}




		public function indexGridAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$gridHelper = $this->get("uneak.routesmanager.grid.helper");
			$menuHelper = $this->get("uneak.routesmanager.menu.helper");

			$params = $request->query->all();

			$gridData = $gridHelper->gridFields($gridHelper->createGridQueryBuilder('GalleryBundle\Entity\GalleryType', $params), $params);
			$recordsTotal = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('GalleryBundle\Entity\GalleryType', $params));
			$recordsFiltered = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('GalleryBundle\Entity\GalleryType', $params));


			$rowMenuTemplate = "UneakFlatSkinBundle:Block:Menu/block_menu.html.twig";

			$data = array();

			foreach ($gridData as $object) {
				$row = array();
				foreach ($params['columns'] as $columns) {
					if ($columns['name'] && substr($columns['name'], 0, 1) != '_') {
						$value = $object[str_replace(".", "_", $columns['name'])];
						if ($value instanceof \DateTime) {
							$value = $value->format('d/m/Y H:m:s');
						}
						$row[$columns['data']] = $value;
					} else {
						$row[$columns['data']] = "";
					}
				}
				$row['DT_RowId'] = $object['DT_RowId'];


				$menu = new Menu();
				$menu->setTemplate($rowMenuTemplate);
				$rowActions = $route->getParent()->getNestedRoute()->getRowActions();
				$root = $menuHelper->createMenu($rowActions, $route, array('gallerytype' => $row['DT_RowId']));
				$root->setChildrenAttribute('class', 'nav nav-pills nav-inline text-nowrap');
				$menu->setRoot($root);

				$blockManager->addBlock($menu, 'rowMenu');
				$row['_actions'] = $this->renderView("{{ renderBlock('rowMenu') }}");

				array_push($data, $row);
			}

			return new JsonResponse(array(
				'draw'            => $params["draw"],
				'recordsTotal'    => $recordsTotal,
				'recordsFiltered' => $recordsFiltered,
				'data'            => $data,
			));
		}


	}
