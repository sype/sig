<?php

	namespace GalleryBundle\Controller;

	use Doctrine\Common\Collections\ArrayCollection;
	use GalleryBundle\Entity\Gallery;
    use GalleryBundle\Entity\GalleryRepository;
    use GalleryBundle\Form\GalleryType;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoute;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
	use Uneak\FlatSkinBundle\Block\Component\DataTable;
	use Uneak\FlatSkinBundle\Block\DataTable\DataTableFilters;
	use Uneak\FlatSkinBundle\Block\Form\Form;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Doctrine\ORM\Query\Expr;

	class AdminGalleryController extends Controller {


		public function indexAction( FlattenRoute $route) {
			$blockManager = $this->get("uneak.blocksmanager");

			$userBlock = $this->get('uneak.admin.gallery.block.helper');
			$blockManager->addBlock($userBlock->getNavigation(), 'navigation');

			$gridNested = $route->getNestedRoute();

			$datatable = new DataTable();
			$datatable->setAjax($route->getChild('_grid')->getRoutePath());
			$datatable->setColumns($gridNested->getColumns());

			$datatableWrapper = new Wrapper();
			$datatableWrapper
				->setTitle("Resultats")
				->setCollapsable(false)
				->addBlock($datatable);
			$blockManager->addBlock($datatableWrapper, 'datatable');

			$datatableFilterWrapper = new Wrapper();
			$datatableFilterWrapper
				->setTitle("Filtres")
				->setCollapsable(true)
				->addBlock(new DataTableFilters($datatable));
			$blockManager->addBlock($datatableFilterWrapper, 'datatable_filters');

			return $this->render('GalleryBundle:Admin:Gallery/index.html.twig');

		}



		public function showAction($gallery) {
			$blockManager = $this->get("uneak.blocksmanager");

			$galleryBlock = $this->get('uneak.admin.gallery.block.helper');

			$blockManager->addBlock($galleryBlock->getInfos($gallery), 'infos');
			$blockManager->addBlock($galleryBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($galleryBlock->getGalleryNavigation($gallery), 'user_navigation');
			$blockManager->addBlock($galleryBlock->getImages($gallery), 'imagesGallery');

			return $this->render('GalleryBundle:Admin:Gallery/show.html.twig');

		}


		public function editAction($gallery, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");


			$userBlock = $this->get('uneak.admin.gallery.block.helper');
			$blockManager->addBlock($userBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($userBlock->getGalleryNavigation($gallery), 'user_navigation');

			$em = $this->getDoctrine()->getManager();
			$images = $em->getRepository('GalleryBundle:Image')->findImages($gallery, false);
			$originalImages = new ArrayCollection();
			foreach ($images as $image) {
				$originalImages->add($image);
			}


			$form = $this->createForm(new GalleryType(), $gallery);
			$form->add('submit', 'submit', array('label' => 'Modifier'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

					foreach ($originalImages as $image) {
						if ($gallery->getImages()->contains($image) == false) {
							$gallery->getImages()->removeElement($image);
							$em->remove($image);
						}
					}

					$em->flush();

					return $this->redirect($route->getChild('*/subject/show', array('gallery' => $gallery->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("GalleryBundle:Block:Form/block_gallery_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Edition de la galerie ".$gallery->getLabel())
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('GalleryBundle:Admin:Gallery/edit.html.twig');

		}


		public function newAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$userBlock = $this->get('uneak.admin.gallery.block.helper');
			$blockManager->addBlock($userBlock->getNavigation(), 'navigation');

			$gallery = new Gallery();

			$form = $this->createForm(new GalleryType(), $gallery);
			$form->add('submit', 'submit', array('label' => 'Creer'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($gallery);
                    $em->flush();

					return $this->redirect($route->getChild('*/subject/show', array('gallery' => $gallery->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("GalleryBundle:Block:Form/block_gallery_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Création d'une galerie")
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');

			return $this->render('GalleryBundle:Admin:Gallery/new.html.twig');


		}


		public function deleteAction($gallery, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$userBlock = $this->get('uneak.admin.gallery.block.helper');
			$blockManager->addBlock($userBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($userBlock->getGalleryNavigation($gallery), 'user_navigation');

			$form = $this->createFormBuilder(array());
			$form->add('cancel', 'submit', array('label' => 'Annuler'));
			$form->add('confirm', 'submit', array('label' => 'Confirmer'));
			$form = $form->getForm();


			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');

				$form->handleRequest($request);
				if ($form->isValid()) {
					if ($form->get('cancel')->isClicked()) {
						$flash->info('Suppression annulée');

						return $this->redirect($route->getChild('*/subject/show', array('gallery' => $gallery->getId()))->getRoutePath());
					}
					if ($form->get('confirm')->isClicked()) {

                        $em = $this->getDoctrine()->getManager();
                        $em->remove($gallery);
                        $em->flush();

						$flash->success('La suppression à été réalisée avec succès.');

						return $this->redirect($route->getChild('*/index')->getRoutePath());
					}

				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formBlock = new Form($form->createView());
			$formBlock->setTemplate("GalleryBundle:Block:Form/delete_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Suppression d'une galerie ".$gallery->getLabel())
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('GalleryBundle:Admin:Gallery/delete.html.twig');

		}




		public function indexGridAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$gridHelper = $this->get("uneak.routesmanager.grid.helper");
			$menuHelper = $this->get("uneak.routesmanager.menu.helper");

			$params = $request->query->all();

			$gridData = $gridHelper->gridFields($gridHelper->createGridQueryBuilder('GalleryBundle\Entity\Gallery', $params), $params);
			$recordsTotal = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('GalleryBundle\Entity\Gallery', $params));
			$recordsFiltered = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('GalleryBundle\Entity\Gallery', $params));


			$rowMenuTemplate = "UneakFlatSkinBundle:Block:Menu/block_menu.html.twig";

			$data = array();

			foreach ($gridData as $object) {
				$row = array();
				foreach ($params['columns'] as $columns) {
					if ($columns['name'] && substr($columns['name'], 0, 1) != '_') {
						$value = $object[str_replace(".", "_", $columns['name'])];
						if ($value instanceof \DateTime) {
							$value = $value->format('d/m/Y H:m:s');
						}
						$row[$columns['data']] = $value;
					} else {
						$row[$columns['data']] = "";
					}
				}
				$row['DT_RowId'] = $object['DT_RowId'];


				$menu = new Menu();
				$menu->setTemplate($rowMenuTemplate);
				$rowActions = $route->getParent()->getNestedRoute()->getRowActions();
				$root = $menuHelper->createMenu($rowActions, $route, array('gallery' => $row['DT_RowId']));
				$root->setChildrenAttribute('class', 'nav nav-pills nav-inline text-nowrap');
				$menu->setRoot($root);

				$blockManager->addBlock($menu, 'rowMenu');
				$row['_actions'] = $this->renderView("{{ renderBlock('rowMenu') }}");

				array_push($data, $row);
			}

			return new JsonResponse(array(
				'draw'            => $params["draw"],
				'recordsTotal'    => $recordsTotal,
				'recordsFiltered' => $recordsFiltered,
				'data'            => $data,
			));
		}


	}
