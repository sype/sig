<?php

	namespace GalleryBundle\Entity;

	use Doctrine\ORM\Mapping as ORM;
	use Gedmo\Mapping\Annotation as Gedmo;
	use Symfony\Component\HttpFoundation\File\File;
	use Vich\UploaderBundle\Mapping\Annotation as Vich;
	use Symfony\Component\Validator\Constraints as Assert;

	/**
	 * Gallery
	 *
	 * @ORM\Table()
	 * @ORM\Entity(repositoryClass="GalleryBundle\Entity\GalleryRepository")
	 * @Vich\Uploadable
	 */
	class Gallery {

		/**
		 * @var integer
		 *
		 * @ORM\Column(name="id", type="integer")
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="AUTO")
		 */
		protected $id;

		/**
		 * @var datetime $created
		 *
		 * @Gedmo\Timestampable(on="create")
		 * @ORM\Column(type="datetime")
		 */
		protected $created;

		/**
		 * @var datetime $updated
		 *
		 * @Gedmo\Timestampable(on="update")
		 * @ORM\Column(type="datetime")
		 */
		protected $updated;

		/**
		 * @ORM\Column(name="label", type="string", length=128)
		 */
		protected $label;


        /**
         * @ORM\Column(name="slug", type="string", length=64, unique=false, nullable=true)
         * @Gedmo\Slug(fields={"label"}, updatable=false, unique=true)
         *
         */
        protected $slug;

		/**
		 * @ORM\Column(name="description", type="text")
		 */
		protected $description;

		/**
		 * @ORM\Column(name="active", type="boolean")
		 */
		protected $active = true;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="image", type="string", length=255, nullable=true)
		 */
		protected $image;

		/**
		 * @var File $imageFile
		 * @Vich\UploadableField(mapping="gallery_image", fileNameProperty="image")
		 */
		protected $imageFile;


		/**
		 *
		 * @ORM\OneToMany(targetEntity="\GalleryBundle\Entity\Image", mappedBy="gallery", cascade={"persist", "remove"})
		 */
		protected $images;

		/**
		 * @ORM\ManyToOne(targetEntity="GalleryBundle\Entity\GalleryType", inversedBy="galleries", cascade={"persist"})
		 * @ORM\JoinColumn(name="type_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
		 */
		protected $type;


		/**
		 * Constructor
		 */
		public function __construct() {
			$this->images = new \Doctrine\Common\Collections\ArrayCollection();
		}


		/**
		 * Add images
		 *
		 * @param \GalleryBundle\Entity\Image $image
		 *
		 * @return Gallery
		 */
		public function addImage(\GalleryBundle\Entity\Image $image) {
			$image->setGallery($this);
			$this->images[] = $image;
			return $this;
		}

		/**
		 * Remove images
		 *
		 * @param \GalleryBundle\Entity\Image $image
		 */
		public function removeImage(\GalleryBundle\Entity\Image $image) {
			$image->setGallery(null);
			$this->images->removeElement($image);
		}

		/**
		 * Get images
		 *
		 * @return \Doctrine\Common\Collections\Collection
		 */
		public function getImages() {
			return $this->images;
		}

		/**
		 * Set images
		 * @param \Doctrine\Common\Collections\ArrayCollection
		 *
		 * @return Gallery
		 */
		public function setImages(\Doctrine\Common\Collections\ArrayCollection $images) {
			foreach ($images as $image) {
				$image->setGallery($this);
			}
			$this->$images = $images;
			return $this;
		}


		/**
		 * Get id
		 *
		 * @return integer
		 */
		public function getId() {
			return $this->id;
		}

		/**
		 * Set created
		 *
		 * @param \DateTime $created
		 *
		 * @return Publish
		 */
		public function setCreated($created) {
			$this->created = $created;

			return $this;
		}

		/**
		 * Get created
		 *
		 * @return \DateTime
		 */
		public function getCreated() {
			return $this->created;
		}

		/**
		 * Set updated
		 *
		 * @param \DateTime $updated
		 *
		 * @return Publish
		 */
		public function setUpdated($updated) {
			$this->updated = $updated;

			return $this;
		}

		/**
		 * Get updated
		 *
		 * @return \DateTime
		 */
		public function getUpdated() {
			return $this->updated;
		}

		/**
		 * Set active
		 *
		 * @param boolean $active
		 *
		 * @return Publish
		 */
		public function setActive($active) {
			$this->active = $active;

			return $this;
		}

		/**
		 * Get active
		 *
		 * @return boolean
		 */
		public function getActive() {
			return $this->active;
		}


		/**
		 * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
		 * of 'UploadedFile' is injected into this setter to trigger the  update. If this
		 * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
		 * must be able to accept an instance of 'File' as the bundle will inject one here
		 * during Doctrine hydration.
		 *
		 * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
		 */
		public function setImageFile(File $image) {
			$this->imageFile = $image;

			if ($image) {
				// It is required that at least one field changes if you are using doctrine
				// otherwise the event listeners won't be called and the file is lost
				$this->updatedAt = new \DateTime('now');
			}
		}

		/**
		 * @return File
		 */
		public function getImageFile() {
			return $this->imageFile;
		}

		/**
		 * Set image path
		 *
		 * @param string $image
		 *
		 * @return Modele
		 */
		public function setImage($image = null) {
			$this->image = $image;
			return $this;
		}

		/**
		 * Get image path
		 *
		 * @return string $image
		 */
		public function getImage() {
			return $this->image;
		}


		/**
		 * Set label
		 *
		 * @param string $label
		 *
		 * @return Gallery
		 */
		public function setLabel($label) {
			$this->label = $label;

			return $this;
		}

		/**
		 * Get label
		 *
		 * @return string
		 */
		public function getLabel() {
			return $this->label;
		}


		/**
		 * @return mixed
		 */
		public function getDescription() {
			return $this->description;
		}

		/**
		 * @param mixed $description
		 */
		public function setDescription($description) {
			$this->description = $description;
		}


		/**
		 * Set type
		 *
		 * @param \GalleryBundle\Entity\GalleryType $type
		 * @return Image
		 */
		public function setType(\GalleryBundle\Entity\GalleryType $type = null) {
			$this->type = $type;
			return $this;
		}

		/**
		 * Get type
		 *
		 * @return \GalleryBundle\Entity\GalleryType
		 */
		public function getType() {
			return $this->type;
		}


		public function __toString() {
			return $this->label;
		}

        /**
         * @return mixed
         */
        public function getSlug()
        {
            return $this->slug;
        }

        /**
         * @param mixed $slug
         */
        public function setSlug($slug)
        {
            $this->slug = $slug;
        }


	}
