<?php

	namespace GalleryBundle\Form;

	use Doctrine\ORM\EntityRepository;
	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;

	class GalleryType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('type', 'entity_select2', array(
					'label'         => "Type",
					'class'         => 'GalleryBundle:GalleryType',
					'query_builder' => function (EntityRepository $er) {
						return $er->getGalleryTypesQuery();
					},
					'multiple'      => false,
					'empty_value'   => "Sectionnez le type",
				))
				->add('label', null, array(
					'label' => "Titre",
				))

                ->add('slug', null, array(
                    'label' => "Slug",
                ))

				->add('description', 'ckeditor', array(
					'label' => "Description",
                    'options' => array(
                        'customConfig' => '../../../bundles/app/js/ckeditor_config.js',
                    ),
				))
				->add('imageFile', 'vich_file', array(
						'label'         => "Photo",
						'allow_delete'  => true, // not mandatory, default is true
						'download_link' => true, // not mandatory, default is true
						'required'      => false,
					)
				)
				->add('images', 'bootstrap_collection', array(
					'label'              => "Galerie d'images",
					'by_reference'			=> false,
					'type'               => new ImageType(),
					'allow_add'          => true,
					'allow_delete'       => true,
					'add_button_text'    => "Ajouter une image",
					'delete_button_text' => "x",
					'sub_widget_col'     => 10,
					'button_col'         => 2
				))

				->add('active', null, array(
					'label'    => "Activé",
					'required' => false,
				));

		}


		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'GalleryBundle\Entity\Gallery',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'gallerybundle_gallery';
		}
	}
