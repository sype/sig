<?php

	namespace GalleryBundle\Block;


	use Doctrine\ORM\EntityManager;
	use GalleryBundle\Entity\Gallery as GalleryEntity;
	use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
    use Uneak\FlatSkinBundle\Block\Panel\PanelPeople;
    use Uneak\FlatSkinBundle\Block\Panel\PanelPeoples;
    use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
    use Uneak\FlatSkinBundle\Block\Info\Info;
	use Uneak\FlatSkinBundle\Block\Info\Infos;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\ProfileNav;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

	class Gallery {

		private $blockManager;
		private $routeManager;
		private $menuHelper;
		private $vichHelper;
		private $autorization;
		private $em;

		public function __construct(Block $blockManager, FlattenRouteManager $routeManager, MenuHelper $menuHelper, UploaderHelper $vichHelper, AuthorizationChecker $autorization, EntityManager $em) {
			$this->blockManager = $blockManager;
			$this->routeManager = $routeManager;
			$this->menuHelper = $menuHelper;
			$this->vichHelper = $vichHelper;
			$this->autorization = $autorization;
			$this->em = $em;
		}


        public function getImages($entity) {

            $images = $entity->getImages();

            $panelPeoples = new PanelPeoples();
            $panelPeoples->setTitle("<i class='fa fa-icon fa-picture-o'> Galerie d'images</i>");
            foreach ($images as $image) {
                $panelPeople = new PanelPeople();
                $panelPeople->setTitle($image->getLabel());
                $panelPeople->setDescription("Activé : ".(($image->getActive()) ? "Oui" : "Non"));
                $panelPeople->setPhoto($this->vichHelper->asset($image, 'imageFile'));
                $panelPeoples->addPanel($panelPeople);
            }

            return $panelPeoples;

        }


		public function getInfos(GalleryEntity $gallery) {


			$infos = new Infos();
			$infos
				->setColumns(1)
				->setStripeRow(true)
				->addBlock(new Info("Titre", $gallery->getLabel()), null, 70)
				->addBlock(new Info("Slug", $gallery->getSlug()), null, 60)
				->addBlock(new Info("Description", $gallery->getDescription()), null, 60)
                ->addBlock(new Info("Activé", ($gallery->getActive()) ? "Oui" : "Non"), null, 0);

			$wrapper = new Wrapper();
			$wrapper
				->setTitle("Informations")
				->addBlock($infos);

			return $wrapper;
		}



		public function getNavigation() {

			$flattenCrud = $this->routeManager->getFlattenRoute('gallery');

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($flattenCrud->getMetaData("_label"))
				->setDescription($flattenCrud->getMetaData("_description"))
				->setColor("green")
				->setHeaderAlign('vertical');
			$profileNav->addBlock($this->getNavigationMenu());

			return $profileNav;

		}

		public function getNavigationMenu() {

			$flattenCrud = $this->routeManager->getFlattenRoute('gallery');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud);
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}

		public function getGalleryNavigation(GalleryEntity $gallery) {

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($gallery->getLabel())
				->setPhoto($this->vichHelper->asset($gallery, 'imageFile'))
				->setColor("green")
				->setHeaderAlign('horizontal');
			$profileNav->addBlock($this->getGalleryNavigationMenu($gallery));

			return $profileNav;

		}

		public function getGalleryNavigationMenu(GalleryEntity $gallery) {

			$flattenCrud = $this->routeManager->getFlattenRoute('gallery/subject');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('gallery' => $gallery->getId()));
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}



	}
