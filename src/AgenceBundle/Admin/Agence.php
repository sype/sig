<?php

	namespace AgenceBundle\Admin;

	use Uneak\RoutesManagerBundle\Routes\NestedAdminRoute;
	use Uneak\RoutesManagerBundle\Routes\NestedCRUDRoute;
	use Uneak\RoutesManagerBundle\Routes\NestedEntityRoute;
	use Uneak\RoutesManagerBundle\Routes\NestedGridRoute;

	class Agence extends NestedCRUDRoute {

		protected $entity = 'AgenceBundle\Entity\Agence';

		public function initialize() {
			parent::initialize();

			$this->setMetaData('_icon', 'briefcase');
			$this->setMetaData('_label', 'Agences');
			$this->setMetaData('_description', 'Gestion des agences');
			$this->setMetaData('_menu', array(
				'index'  => '*/index',
				'new'    => '*/new',
			));
		}


		protected function buildCRUD() {
			$indexRoute = new NestedGridRoute('index');
			$indexRoute
				->setPath('')
				->setAction('index')
				->setMetaData('_icon', 'list')
				->setMetaData('_label', 'Liste')
				->addRowAction('show', '*/subject/show')
				->addRowAction('edit', '*/subject/edit')
				->addRowAction('delete', '*/subject/delete')

				->addColumn(array('title' => 'Nom', 'name' => 'label'))
				->addColumn(array('title' => 'Commune', 'name' => 'commune.label'))
			;
			$this->addChild($indexRoute);


			$newRoute = new NestedAdminRoute('new');
			$newRoute
				->setPath('new')
				->setAction('new')
				->setMetaData('_icon', 'plus-circle')
				->setMetaData('_label', 'New');
			$this->addChild($newRoute);


			$subjectRoute = new NestedEntityRoute('subject');
			$subjectRoute
				->setParameterName($this->getId())
				->setParameterPattern('\d+')
				->setEnabled(false)
				->setMetaData('_menu', array(
					'show'   => '*/subject/show',
					'edit'   => '*/subject/edit',
					'collaborateur'   => '*/subject/collaborateur/new',
					'delete' => '*/subject/delete',
				));
			$this->addChild($subjectRoute);


			$showRoute = new NestedAdminRoute('show');
			$showRoute
				->setAction('show')
				->setMetaData('_icon', 'eye')
				->setMetaData('_label', 'Show')
				->setRequirement('_method', 'GET');
			$subjectRoute->addChild($showRoute);


			$editRoute = new NestedAdminRoute('edit');
			$editRoute
				->setAction('edit')
				->setMetaData('_icon', 'edit')
				->setMetaData('_label', 'Edit');
			$subjectRoute->addChild($editRoute);

			$deleteRoute = new NestedAdminRoute('delete');
			$deleteRoute
				->setAction('delete')
				->setMetaData('_icon', 'times')
				->setMetaData('_label', 'Delete');
			$subjectRoute->addChild($deleteRoute);



            //
            $collabRoute = new NestedAdminRoute('collaborateur');
            $collabRoute->setEnabled(false);
            $subjectRoute->addChild($collabRoute);

            $newCollabRoute = new NestedAdminRoute('new');
            $newCollabRoute
                ->setAction('collaborateurNew')
                ->setMetaData('_icon', 'edit')
                ->setMetaData('_label', 'Ajouter un collaborateur');
            $collabRoute->addChild($newCollabRoute);

            $collabSubjectRoute = new NestedEntityRoute('subject');
            $collabSubjectRoute
                ->setParameterName('collaborateur')
                ->setParameterPattern('\d+')
                ->setEntity('AgenceBundle\Entity\AgenceCollaborateur')
                ->setEnabled(false)
                ->setMetaData('_menu', array(
                    'edit'   => 'edit',
                    'delete' => 'delete',
                ));
            $collabRoute->addChild($collabSubjectRoute);

            $collabEditRoute = new NestedAdminRoute('edit');
            $collabEditRoute
                ->setAction('collaborateurEdit')
                ->setMetaData('_icon', 'edit')
                ->setMetaData('_label', 'Edit');
            $collabSubjectRoute->addChild($collabEditRoute);

            $collabDeleteRoute = new NestedAdminRoute('delete');
            $collabDeleteRoute
                ->setAction('collaborateurDelete')
                ->setMetaData('_icon', 'times')
                ->setMetaData('_label', 'Delete');
            $collabSubjectRoute->addChild($collabDeleteRoute);

		}


	}
