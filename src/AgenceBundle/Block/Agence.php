<?php

	namespace AgenceBundle\Block;


	use Doctrine\ORM\EntityManager;
	use AgenceBundle\Entity\Agence as AgenceEntity;
	use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
	use Uneak\GoogleMapBundle\Block\GoogleMap;
	use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
    use Uneak\FlatSkinBundle\Block\Info\Info;
	use Uneak\FlatSkinBundle\Block\Info\Infos;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
    use Uneak\FlatSkinBundle\Block\Panel\PanelPeople;
    use Uneak\FlatSkinBundle\Block\Panel\PanelPeoples;
    use Uneak\FlatSkinBundle\Block\Panel\ProfileNav;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

	class Agence {

		private $blockManager;
		private $routeManager;
		private $menuHelper;
		private $vichHelper;
		private $autorization;
		private $em;

		public function __construct(Block $blockManager, FlattenRouteManager $routeManager, MenuHelper $menuHelper, UploaderHelper $vichHelper, AuthorizationChecker $autorization, EntityManager $em) {
			$this->blockManager = $blockManager;
			$this->routeManager = $routeManager;
			$this->menuHelper = $menuHelper;
			$this->vichHelper = $vichHelper;
			$this->autorization = $autorization;
			$this->em = $em;
		}


		public function getInfos(AgenceEntity $agence) {


			$infos = new Infos();
			$infos
				->setColumns(1)
				->setStripeRow(true)
                ->addBlock(new Info("Agence", $agence->getLabel()), null, 50)
                ->addBlock(new Info("Adresse", $agence->getAdresse1()), null, 40)
                ->addBlock(new Info("", $agence->getAdresse2()), null, 30)
                ->addBlock(new Info("", $agence->getCommune()->getLabel()." - ".$agence->getCommune()->getDepartement()->getLabel()), null, 20)
                ->addBlock(new GoogleMap("Position", $agence->getLocation(), $agence->getCommune()->getDepartement()->getLabel().", ".$agence->getCommune()->getLabel()), null, 15)
                ->addBlock(new Info("Téléphone", $agence->getPhone()), null, 10)
                ->addBlock(new Info("Email", $agence->getEmail()), null, 5)
                ->addBlock(new Info("Activé", ($agence->getActive()) ? "Oui" : "Non"), null, 0);

			$wrapper = new Wrapper();
			$wrapper
				->setTitle("Informations")
				->addBlock($infos);

			return $wrapper;
		}

        public function getCollaborateur(AgenceEntity $agence) {

                $agenceRepo = $this->em->getRepository('AgenceBundle:AgenceCollaborateur');
                $collabs = $agenceRepo->findCollaborateurs(false, $agence);

                $panelPeoples = new PanelPeoples();
                $panelPeoples->setTitle("<i class='fa fa-icon fa-users'> Collaborateurs</i>");
                foreach ($collabs as $collab) {

                    $flattenCrud = $this->routeManager->getFlattenRoute('agence/subject/collaborateur/subject');
                    $menu = new Menu();
                    $menu->setTemplate("UneakFlatSkinBundle:Block:Menu/block_menu.html.twig");
                    $menu->setMeta('style', 'social-links');
                    $root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('agence' => $agence->getId(), 'collaborateur' => $collab->getId()) );
                    $menu->setRoot($root);

                    $description = $collab->getEmail().'<br/>';
                    $description .= $collab->getPhone();

                    $panelPeople = new PanelPeople();
                    $panelPeople->setTitle($collab->getCollaborateur()->getFirstName()." ".$collab->getCollaborateur()->getLastName());
                    $panelPeople->setSubtitle($collab->getPoste()->getLabel());
                    $panelPeople->setDescription($description);
                    $panelPeople->setPhoto($this->vichHelper->asset($collab->getCollaborateur(), 'imageFile'));
                    $panelPeople->setMenu($menu);

                    $panelPeoples->addPanel($panelPeople);
                }

                return $panelPeoples;

        }


        public function getProjets(AgenceEntity $agence) {

            $projetRepo = $this->em->getRepository('ProjetBundle:Projet');
            $projets = $projetRepo->findProjets(false, $agence);

            $panelPeoples = new PanelPeoples();
            $panelPeoples->setTitle("<i class='fa fa-icon fa-building'> Projets</i>");
            foreach ($projets as $projet) {

                $flattenCrud = $this->routeManager->getFlattenRoute('projet/subject');
                $menu = new Menu();
                $menu->setTemplate("UneakFlatSkinBundle:Block:Menu/block_menu.html.twig");
                $menu->setMeta('style', 'social-links');
                $root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('projet' => $projet->getId()) );
                $menu->setRoot($root);

                $panelPeople = new PanelPeople();
                $panelPeople->setTitle($projet->getLabel());
                $panelPeople->setSubtitle($projet->getYear());
                $panelPeople->setDescription($projet->getDescription());
                $panelPeople->setPhoto($this->vichHelper->asset($projet, 'imageFile'));
                $panelPeople->setMenu($menu);

                $panelPeoples->addPanel($panelPeople);
            }

            return $panelPeoples;

        }


		public function getNavigation() {

			$flattenCrud = $this->routeManager->getFlattenRoute('agence');

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($flattenCrud->getMetaData("_label"))
				->setDescription($flattenCrud->getMetaData("_description"))
				->setColor("green")
				->setHeaderAlign('vertical');
			$profileNav->addBlock($this->getNavigationMenu());

			return $profileNav;

		}

		public function getNavigationMenu() {

			$flattenCrud = $this->routeManager->getFlattenRoute('agence');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud);
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}

		public function getAgenceNavigation(AgenceEntity $agence) {

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($agence->getLabel())
				->setColor("green")
				->setHeaderAlign('horizontal');
			$profileNav->addBlock($this->getAgenceNavigationMenu($agence));

			return $profileNav;

		}

		public function getAgenceNavigationMenu(AgenceEntity $agence) {

			$flattenCrud = $this->routeManager->getFlattenRoute('agence/subject');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('agence' => $agence->getId()));
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}



	}
