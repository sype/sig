<?php

	namespace AgenceBundle\Controller;

	use AgenceBundle\Entity\Agence;
    use AgenceBundle\Entity\AgenceCollaborateur;
    use AgenceBundle\Entity\Agences;
    use AgenceBundle\Entity\AgencesRepository;
    use AgenceBundle\Form\AgenceCollaborateurType;
    use AgenceBundle\Form\AgenceType;
	use Doctrine\ORM\EntityRepository;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoute;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
	use Uneak\FlatSkinBundle\Block\Component\DataTable;
	use Uneak\FlatSkinBundle\Block\DataTable\DataTableFilters;
	use Uneak\FlatSkinBundle\Block\Form\Form;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Doctrine\ORM\Query\Expr;

	class AdminAgenceController extends Controller {


		public function indexAction(FlattenRoute $route) {
			$blockManager = $this->get("uneak.blocksmanager");


			$agenceBlock = $this->get('uneak.admin.agence.block.helper');
			$blockManager->addBlock($agenceBlock->getNavigation(), 'navigation');

			$gridNested = $route->getNestedRoute();

			$datatable = new DataTable();
			$datatable->setAjax($route->getChild('_grid')->getRoutePath());
			$datatable->setColumns($gridNested->getColumns());

			$datatableWrapper = new Wrapper();
			$datatableWrapper
				->setTitle("Resultats")
				->setCollapsable(false)
				->addBlock($datatable);
			$blockManager->addBlock($datatableWrapper, 'datatable');

			$datatableFilterWrapper = new Wrapper();
			$datatableFilterWrapper
				->setTitle("Filtres")
				->setCollapsable(true)
				->addBlock(new DataTableFilters($datatable));
			$blockManager->addBlock($datatableFilterWrapper, 'datatable_filters');

			return $this->render('AgenceBundle:Admin:index.html.twig');

		}



		public function showAction($agence) {
			$blockManager = $this->get("uneak.blocksmanager");


			$agenceBlock = $this->get('uneak.admin.agence.block.helper');

			$blockManager->addBlock($agenceBlock->getInfos($agence), 'infos');
			$blockManager->addBlock($agenceBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($agenceBlock->getAgenceNavigation($agence), 'user_navigation');
            $blockManager->addBlock($agenceBlock->getCollaborateur($agence), 'panelPeople');
            $blockManager->addBlock($agenceBlock->getProjets($agence), 'panelProjets');

			return $this->render('AgenceBundle:Admin:show.html.twig');

		}


		public function editAction($agence, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");



			$agenceBlock = $this->get('uneak.admin.agence.block.helper');
			$blockManager->addBlock($agenceBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($agenceBlock->getAgenceNavigation($agence), 'user_navigation');

			$form = $this->createForm(new AgenceType(), $agence);
			$form->add('submit', 'submit', array('label' => 'Modifier'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

					return $this->redirect($route->getChild('*/subject/show', array('agence' => $agence->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");
			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("AgenceBundle:Block:Form/block_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Edition de l'agence ".$agence->getLabel())
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('AgenceBundle:Admin:edit.html.twig');

		}


        public function collaborateurEditAction($agence, $collaborateur, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");


            $agenceBlock = $this->get('uneak.admin.agence.block.helper');
            $blockManager->addBlock($agenceBlock->getNavigation(), 'navigation');
            $blockManager->addBlock($agenceBlock->getAgenceNavigation($agence), 'user_navigation');


            $form = $this->createForm(new AgenceCollaborateurType(), $collaborateur);
            $form->add('submit', 'submit', array('label' => 'Modifier'));

            if ($request->getMethod() == 'POST') {

                $flash = $this->get('braincrafted_bootstrap.flash');
                $form->handleRequest($request);
                if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    return $this->redirect($route->getChild('*/subject/show', array('agence' => $agence->getId()))->getRoutePath());
                } else {
                    $flash->error('Votre formulaire est invalide.');
                }
            }


            $formManager = $this->get("uneak.formsmanager");

            $formBlock = new Form($formManager->createView($form));
            $formBlock->setTemplate("AgenceBundle:Block:Form/block_collaborateur_form.html.twig");

            $formWrapper = new Wrapper();
            $formWrapper
                ->setTitle("Edition du collaborateur ".$collaborateur->getCollaborateur()->getFirstName()." ".$collaborateur->getCollaborateur()->getLastName())
                ->addBlock($formBlock);
            $blockManager->addBlock($formWrapper, 'form');

            return $this->render('AgenceBundle:Admin:collaborateur/new.html.twig');


        }




        public function newAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");


			$agenceBlock = $this->get('uneak.admin.agence.block.helper');
			$blockManager->addBlock($agenceBlock->getNavigation(), 'navigation');

			$agence = new Agence();

			$form = $this->createForm(new AgenceType(), $agence);
			$form->add('submit', 'submit', array('label' => 'Creer'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($agence);
                    $em->flush();

					return $this->redirect($route->getChild('*/subject/show', array('agence' => $agence->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");
			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("AgenceBundle:Block:Form/block_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Création d'une agence")
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');

			return $this->render('AgenceBundle:Admin:new.html.twig');


		}



        public function collaborateurNewAction($agence, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");


            $agenceBlock = $this->get('uneak.admin.agence.block.helper');
            $blockManager->addBlock($agenceBlock->getNavigation(), 'navigation');
            $blockManager->addBlock($agenceBlock->getAgenceNavigation($agence), 'user_navigation');

            $agenceCollaborateur = new AgenceCollaborateur();
            $agenceCollaborateur->setAgence($agence);

            $form = $this->createForm(new AgenceCollaborateurType(), $agenceCollaborateur);
            $form->add('submit', 'submit', array('label' => 'Creer'));

            if ($request->getMethod() == 'POST') {

                $flash = $this->get('braincrafted_bootstrap.flash');
                $form->handleRequest($request);
                if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($agenceCollaborateur);
                    $em->flush();

                    return $this->redirect($route->getChild('*/subject/show', array('agence' => $agence->getId()))->getRoutePath());
                } else {
                    $flash->error('Votre formulaire est invalide.');
                }
            }


            $formManager = $this->get("uneak.formsmanager");

            $formBlock = new Form($formManager->createView($form));
            $formBlock->setTemplate("AgenceBundle:Block:Form/block_collaborateur_form.html.twig");

            $formWrapper = new Wrapper();
            $formWrapper
                ->setTitle("Création d'un collaborateur de l'agence ".$agence->getLabel())
                ->addBlock($formBlock);
            $blockManager->addBlock($formWrapper, 'form');

            return $this->render('AgenceBundle:Admin:collaborateur/new.html.twig');


        }






		public function deleteAction($agence, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");


			$agenceBlock = $this->get('uneak.admin.agence.block.helper');
			$blockManager->addBlock($agenceBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($agenceBlock->getAgenceNavigation($agence), 'user_navigation');


			$em = $this->getDoctrine()->getManager();
			$projetLiens = $em->getRepository('ProjetBundle:Projet')->findBy(array('agence' => $agence));

			$message = "";
			if (count($projetLiens)) {
				$message .= "<h3>Les projets suivants sont ratachés a cette agence :</h3>";
				$message .= "<ul>";
				foreach ($projetLiens as $projetLien) {
					$message .= "<li>".$projetLien->getLabel()."</li>";
				}
				$message .= "</ul>";
			}


			$form = $this->createFormBuilder(array());

			if (count($projetLiens)) {
				$form->add('agence', 'entity_select2', array(
					'label' => "Déplacer vers",
					'class' => 'AgenceBundle:Agence',
					'query_builder'   => function (EntityRepository $er) use ($agence) {
						return $er->getRemoveAgencesQuery($agence);
					},
					'options' => array(
						'placeholder' => "Sectionnez l'agence",
						'language' => 'fr'
					),
					'multiple'  => false,
					'required' => true,

					'empty_value' => "Sectionnez l'agence",
				));
			}

			$form->add('cancel', 'submit', array('label' => 'Annuler'));
			$form->add('confirm', 'submit', array('label' => 'Confirmer'));
			$form = $form->getForm();


			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');

				$form->handleRequest($request);
				if ($form->isValid()) {
					if ($form->get('cancel')->isClicked()) {
						$flash->info('Suppression annulée');

						return $this->redirect($route->getChild('*/subject/show', array('agence' => $agence->getId()))->getRoutePath());
					}
					if ($form->get('confirm')->isClicked()) {

						if ($form->has("agence")) {
							$nAgence = $form->get("agence")->getData();
							foreach ($projetLiens as $projetLien) {
								$projetLien->setAgence($nAgence);
							}
							$em->flush();
						}

                        $em->remove($agence);
                        $em->flush();

						$flash->success('La suppression à été réalisée avec succès.');

						return $this->redirect($route->getChild('*/index')->getRoutePath());
					}

				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formBlock = new Form($form->createView());
			$formBlock->setTemplate("AgenceBundle:Block:Form/delete_form.html.twig");
			$formBlock->setDescription($message);

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Suppression de l'agence ".$agence->getLabel())
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('AgenceBundle:Admin:delete.html.twig');

		}


        public function collaborateurDeleteAction($agence, $collaborateur, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");


            $agenceBlock = $this->get('uneak.admin.agence.block.helper');
            $blockManager->addBlock($agenceBlock->getNavigation(), 'navigation');
            $blockManager->addBlock($agenceBlock->getAgenceNavigation($agence), 'user_navigation');

            $form = $this->createFormBuilder(array());
            $form->add('cancel', 'submit', array('label' => 'Annuler'));
            $form->add('confirm', 'submit', array('label' => 'Confirmer'));
            $form = $form->getForm();


            if ($request->getMethod() == 'POST') {

                $flash = $this->get('braincrafted_bootstrap.flash');

                $form->handleRequest($request);
                if ($form->isValid()) {
                    if ($form->get('cancel')->isClicked()) {
                        $flash->info('Suppression annulée');
                    }
                    if ($form->get('confirm')->isClicked()) {

                        $em = $this->getDoctrine()->getManager();
                        $em->remove($collaborateur);
                        $em->flush();

                        $flash->success('La suppression à été réalisée avec succès.');
                    }

                    return $this->redirect($route->getChild('*/subject/show', array('agence' => $agence->getId()))->getRoutePath());

                } else {
                    $flash->error('Votre formulaire est invalide.');
                }
            }


            $formBlock = new Form($form->createView());
            $formBlock->setTemplate("AgenceBundle:Block:Form/delete_form.html.twig");

            $formWrapper = new Wrapper();
            $formWrapper
                ->setTitle("Suppression du collaborateur ".$collaborateur->getCollaborateur()->getFirstName()." ".$collaborateur->getCollaborateur()->getLastName()." pour l'agence ".$agence->getLabel())
                ->addBlock($formBlock);
            $blockManager->addBlock($formWrapper, 'form');


            return $this->render('AgenceBundle:Admin:delete.html.twig');

        }



		public function indexGridAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");


			$gridHelper = $this->get("uneak.routesmanager.grid.helper");
			$menuHelper = $this->get("uneak.routesmanager.menu.helper");

			$params = $request->query->all();

			$gridData = $gridHelper->gridFields($gridHelper->createGridQueryBuilder('AgenceBundle\Entity\Agence', $params), $params);
			$recordsTotal = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('AgenceBundle\Entity\Agence', $params));
			$recordsFiltered = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('AgenceBundle\Entity\Agence', $params));


			$rowMenuTemplate = "UneakFlatSkinBundle:Block:Menu/block_menu.html.twig";

			$data = array();

			foreach ($gridData as $object) {
				$row = array();
				foreach ($params['columns'] as $columns) {
					if ($columns['name'] && substr($columns['name'], 0, 1) != '_') {
						$value = $object[str_replace(".", "_", $columns['name'])];
						if ($value instanceof \DateTime) {
							$value = $value->format('d/m/Y H:m:s');
						}
						$row[$columns['data']] = $value;
					} else {
						$row[$columns['data']] = "";
					}
				}
				$row['DT_RowId'] = $object['DT_RowId'];


				$menu = new Menu();
				$menu->setTemplate($rowMenuTemplate);
				$rowActions = $route->getParent()->getNestedRoute()->getRowActions();
				$root = $menuHelper->createMenu($rowActions, $route, array('agence' => $row['DT_RowId']));
				$root->setChildrenAttribute('class', 'nav nav-pills nav-inline text-nowrap');
				$menu->setRoot($root);

				$blockManager->addBlock($menu, 'rowMenu');
				$row['_actions'] = $this->renderView("{{ renderBlock('rowMenu') }}");

				array_push($data, $row);
			}

			return new JsonResponse(array(
				'draw'            => $params["draw"],
				'recordsTotal'    => $recordsTotal,
				'recordsFiltered' => $recordsFiltered,
				'data'            => $data,
			));
		}


	}
