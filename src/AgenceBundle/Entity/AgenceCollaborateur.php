<?php

namespace AgenceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AgenceCollaborateur
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AgenceBundle\Entity\AgenceCollaborateurRepository")
 */
class AgenceCollaborateur {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="\AgenceBundle\Entity\Agence", cascade={"persist"})
     * @ORM\JoinColumn(name="agence_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    protected $agence;


    /**
     * @ORM\ManyToOne(targetEntity="\PosteBundle\Entity\Poste", cascade={"persist"})
     * @ORM\JoinColumn(name="poste_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    protected $poste;

    /**
     * @ORM\ManyToOne(targetEntity="\CollaborateurBundle\Entity\Collaborateur", cascade={"persist"})
     * @ORM\JoinColumn(name="collaborateur_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    protected $collaborateur;

    /**
     * @ORM\Column(name="phone", type="string", length=10, nullable=true)
     * @Assert\Regex(
     *     pattern="/^\d{10}$/",
     *     message="Le numéro de téléphone doit obligatoirement contenir 10 chiffres"
     * )
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\Email(
     *     message = "'{{ value }}' n'est pas un email valide.",
     *
     * )
     */
    protected $email;




	/**
	 * @var datetime $created
	 *
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var datetime $updated
	 *
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(type="datetime")
	 */
	protected $updated;

	/**
	 * @ORM\Column(name="active", type="boolean")
	 */
	protected $active = true;




	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set created
	 *
	 * @param \DateTime $created
	 * @return Publish
	 */
	public function setCreated($created) {
		$this->created = $created;

		return $this;
	}

	/**
	 * Get created
	 *
	 * @return \DateTime
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Set updated
	 *
	 * @param \DateTime $updated
	 * @return Publish
	 */
	public function setUpdated($updated) {
		$this->updated = $updated;

		return $this;
	}

	/**
	 * Get updated
	 *
	 * @return \DateTime
	 */
	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * Set active
	 *
	 * @param boolean $active
	 * @return Publish
	 */
	public function setActive($active) {
		$this->active = $active;

		return $this;
	}

	/**
	 * Get active
	 *
	 * @return boolean
	 */
	public function getActive() {
		return $this->active;
	}




	public function __toString() {
		return $this->collaborateur->__toString();
	}


    /**
     * Set agence
     *
     * @param \AgenceBundle\Entity\Agence $agence
     *
     * @return AgenceCollaborateur
     */
    public function setAgence(\AgenceBundle\Entity\Agence $agence = null) {
        $this->agence = $agence;
        return $this;
    }

    /**
     * Get agence
     *
     * @return \AgenceBundle\Entity\Agence
     */
    public function getAgence() {
        return $this->agence;
    }


    /**
     * Set poste
     *
     * @param \PosteBundle\Entity\Poste $poste
     *
     * @return Contact
     */
    public function setPoste(\PosteBundle\Entity\Poste $poste = null) {
        $this->poste = $poste;
        return $this;
    }

    /**
     * Get poste
     *
     * @return \PosteBundle\Entity\Poste
     */
    public function getPoste() {
        return $this->poste;
    }


    /**
     * Set collaborateur
     *
     * @param \CollaborateurBundle\Entity\Collaborateur $collaborateur
     *
     * @return Contact
     */
    public function setCollaborateur(\CollaborateurBundle\Entity\Collaborateur $collaborateur = null) {
        $this->collaborateur = $collaborateur;
        return $this;
    }

    /**
     * Get collaborateur
     *
     * @return \CollaborateurBundle\Entity\Collaborateur
     */
    public function getCollaborateur() {
        return $this->collaborateur;
    }
    
    

    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    public function getPhone() {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdresse1()
    {
        return $this->adresse1;
    }

    /**
     * @param mixed $adresse1
     */
    public function setAdresse1($adresse1)
    {
        $this->adresse1 = $adresse1;
    }

    /**
     * @return mixed
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * @param mixed $adresse2
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;
    }

    /**
     * @return mixed
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * @param mixed $newPassword
     */
    public function setLocation($location) {
        $this->location = $location;
    }
}
