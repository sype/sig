<?php

namespace AgenceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Agence
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AgenceBundle\Entity\AgenceRepository")
 */
class Agence {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\Column(name="label", type="string", length=64, nullable=false)
     * @Assert\NotBlank()
	 */
	protected $label;

    /**
     * @ORM\Column(name="slug", type="string", length=64, unique=false, nullable=true)
     * @Gedmo\Slug(fields={"label"}, updatable=false, unique=true)
     *
     */
    protected $slug;

    /**
     * @ORM\Column(name="adresse1", type="string", length=128, nullable=false)
     * @Assert\NotBlank()
     */
    protected $adresse1;

    /**
     * @ORM\Column(name="adresse2", type="string", length=128, nullable=true)
     */
    protected $adresse2;

    /**
     * @ORM\Column(name="phone", type="string", length=10, nullable=true)
     * @Assert\Regex(
     *     pattern="/^\d{10}$/",
     *     message="Le numéro de téléphone doit obligatoirement contenir 10 chiffres"
     * )
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\Email(
     *     message = "'{{ value }}' n'est pas un email valide.",
     *
     * )
     */
    protected $email;


    /**
     * @ORM\ManyToOne(targetEntity="\LocalisationBundle\Entity\Communes", cascade={"persist"})
     * @ORM\JoinColumn(name="commune_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    protected $commune;

	/**
	 * @var datetime $created
	 *
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var datetime $updated
	 *
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(type="datetime")
	 */
	protected $updated;

	/**
	 * @ORM\Column(name="active", type="boolean")
	 */
	protected $active = true;

    /**
     * @ORM\Column(name="location", type="json_array", nullable=true)
     */
    protected $location;


    /**
     * @ORM\OneToMany(targetEntity="\AgenceBundle\Entity\AgenceCollaborateur", mappedBy="agence", cascade={"persist", "remove"})
     */
    protected $collaborateurs;


    /**
     * Constructor
     */
    public function __construct() {
        $this->collaborateurs = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Add collaborateurs
     *
     * @param \AgenceBundle\Entity\AgenceCollaborateur $collaborateur
     *
     * @return Agence
     */
    public function addCollaborateur(\AgenceBundle\Entity\AgenceCollaborateur $collaborateur) {
        $collaborateur->setAgence($this);
        $this->collaborateurs[] = $collaborateur;
        return $this;
    }

    /**
     * Remove collaborateurs
     *
     * @param \AgenceBundle\Entity\AgenceCollaborateur $collaborateur
     */
    public function removeCollaborateur(\AgenceBundle\Entity\AgenceCollaborateur $collaborateur) {
        $collaborateur->setAgence(null);
        $this->collaborateurs->removeElement($collaborateur);
    }

    /**
     * Get collaborateurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCollaborateurs() {
        return $this->collaborateurs;
    }

    /**
     * Set collaborateurs
     * @param \Doctrine\Common\Collections\ArrayCollection
     *
     * @return Agence
     */
    public function setCollaborateurs(\Doctrine\Common\Collections\ArrayCollection $collaborateurs) {
        foreach ($collaborateurs as $collaborateur) {
            $collaborateur->setAgence($this);
        }
        $this->$collaborateurs = $collaborateurs;
        return $this;
    }


    
    
    /**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set created
	 *
	 * @param \DateTime $created
	 * @return Publish
	 */
	public function setCreated($created) {
		$this->created = $created;

		return $this;
	}

	/**
	 * Get created
	 *
	 * @return \DateTime
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Set updated
	 *
	 * @param \DateTime $updated
	 * @return Publish
	 */
	public function setUpdated($updated) {
		$this->updated = $updated;

		return $this;
	}

	/**
	 * Get updated
	 *
	 * @return \DateTime
	 */
	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * Set active
	 *
	 * @param boolean $active
	 * @return Publish
	 */
	public function setActive($active) {
		$this->active = $active;

		return $this;
	}

	/**
	 * Get active
	 *
	 * @return boolean
	 */
	public function getActive() {
		return $this->active;
	}

	/**
	 * Set label
	 *
	 * @param string $label
	 * @return MenuItem
	 */
	public function setLabel($label) {
		$this->label = $label;

		return $this;
	}

	/**
	 * Get label
	 *
	 * @return string
	 */
	public function getLabel() {
		return $this->label;
	}



	public function __toString() {
		return $this->label;
	}


    /**
     * Set commune
     *
     * @param \LocalisationBundle\Entity\Communes $commune
     *
     * @return Contact
     */
    public function setCommune(\LocalisationBundle\Entity\Communes $commune = null) {
        $this->commune = $commune;
        return $this;
    }

    /**
     * Get commune
     *
     * @return \LocalisationBundle\Entity\Communes
     */
    public function getCommune() {
        return $this->commune;
    }


    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    public function getPhone() {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdresse1()
    {
        return $this->adresse1;
    }

    /**
     * @param mixed $adresse1
     */
    public function setAdresse1($adresse1)
    {
        $this->adresse1 = $adresse1;
    }

    /**
     * @return mixed
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * @param mixed $adresse2
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;
    }

    /**
     * @return mixed
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * @param mixed $newPassword
     */
    public function setLocation($location) {
        $this->location = $location;
    }


    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }
}
