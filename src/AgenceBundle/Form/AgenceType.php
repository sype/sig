<?php

	namespace AgenceBundle\Form;

	use AgenceBundle\Entity\Agence;
    use Doctrine\ORM\EntityRepository;
    use LocalisationBundle\Entity\Departements;
	use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
    use Symfony\Component\Form\FormView;
    use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;
	use Uneak\AssetsManagerBundle\Assets\AssetBuilder;
	use Uneak\AssetsManagerBundle\Assets\Js\AssetInternalJs;
	use Uneak\FormsManagerBundle\Forms\AssetsComponentType;

	class AgenceType extends AssetsComponentType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('label', null, array(
					'label' => "Nom",
				))

                ->add('slug', null, array(
					'label' => "Slug",
				))

                ->add('adresse1', null, array(
                    'label' => "Adresse",
                ))

                ->add('adresse2', null, array(
                    'label' => "Adresse suite",
                ))

				->add('location', 'uneak_googlemap', array(
					'label' => "Position géographique",
					'search_fields' => array('departement', 'commune'),
					'bounds' => array(
						'editable' => false,
					)
				))

                ->add('email', 'email', array(
                    'label'    => 'Email',
                    'attr'     => array(
                        'input_group' => array(
                            'prepend' => '.icon-envelope'
                        )
                    ),
                    'required'      => false,
                ))
                ->add('phone', null, array(
                    'label'    => 'Numéro de téléphone',
                    'attr'     => array(
                        'input_group' => array(
                            'prepend' => '.icon-phone'
                        )
                    ),
                    'required'      => false,
                ))

				->add('active', null, array(
					'label' => "Activé",
					'required'      => false,
				));


            $factory = $builder->getFormFactory();

            $refreshCommunes = function ($form, $departement) use ($factory) {

                $form->add($factory->createNamed('commune', 'entity_select2', null, array(
                    'class'           => 'LocalisationBundle:Communes',
                    'property'        => 'label',
                    'empty_value'     => 'Sélectionnez votre commune',
                    'auto_initialize' => false,
                    'required'        => true,
                    'attr'            => array(
                        'input_group' => array(
                            'prepend' => '.icon-map-marker'
                        )
                    ),
                    'query_builder'   => function (EntityRepository $er) use ($departement) {
                        $qb = $er->getCommunesQuery();

                        if ($departement instanceof Departements) {
                            $qb->where('communes.departement = :departement')->setParameter('departement', $departement);
                        } elseif (is_numeric($departement)) {
                            $qb->where('departement.id = :departement')->setParameter('departement', $departement);
                        } else {
                            $qb->where('departement.id = :departement')->setParameter('departement', 0);
                        }

                        return $qb;
                    }
                )));
            };

            $setDepartement = function ($form, $departement) use ($factory) {
                $form->add($factory->createNamed('departement', 'entity_select2', null, array(
                    'class'           => 'LocalisationBundle:Departements',
                    'property'        => 'label',
                    'query_builder'   => function (EntityRepository $er) {
                        return $er->getDepartementsQuery();
                    },
                    'empty_value'     => 'Sélectionnez votre département',
                    'empty_data'      => null,
                    'auto_initialize' => false,
                    'data'            => $departement,
                    'label'           => 'Département',
                    'required'        => true,
                    'mapped'          => false,
                    'attr'            => array(
                        'input_group' => array(
                            'prepend' => '.icon-globe'
                        )
                    )
                )));
            };


            $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($refreshCommunes, $setDepartement) {
                $form = $event->getForm();
                $data = $event->getData();


                if ($data == null) {
                    return;
                }


                if ($data instanceof Agence) {

                    $departement = ($data->getCommune() && $data->getId()) ? $data->getCommune()->getDepartement() : null;
                    $refreshCommunes($form, $departement);
                    $setDepartement($form, $departement);

                }
            });

            $builder->addEventListener(FormEvents::PRE_BIND, function (FormEvent $event) use ($refreshCommunes) {
                $form = $event->getForm();
                $data = $event->getData();

                if (array_key_exists('departement', $data)) {
                    $refreshCommunes($form, $data['departement']);
                }
            });

        }


		public function buildAsset(AssetBuilder $builder, $parameters) {

			$builder
				->add("script_agence", new AssetInternalJs(), array(
					"template" => "LocalisationBundle:Form:localisation_script.html.twig",
					"parameters" => array('item' => $parameters)
				));

		}





		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'AgenceBundle\Entity\Agence',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'agencebundle_agence';
		}
	}
