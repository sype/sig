<?php

	namespace AgenceBundle\Form;

	use AgenceBundle\Entity\Agence;
    use Doctrine\ORM\EntityRepository;
    use LocalisationBundle\Entity\Departements;
	use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
    use Symfony\Component\Form\FormView;
    use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;
	use Uneak\FormsManagerBundle\Forms\AssetsComponentType;

	class AgenceCollaborateurType extends AssetsComponentType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder

                ->add('poste', 'entity_select2', array(
                    'label' => "Poste",
                    'class' => 'PosteBundle:Poste',
                    'query_builder'   => function (EntityRepository $er) {
                        return $er->getPosteQuery();
                    },
                    'options' => array(
                        'placeholder' => "Sectionnez le poste",
                        'language' => 'fr'
                    ),
                    'multiple'  => false,

                    'empty_value' => 'Sectionnez le poste',
                ))

                ->add('collaborateur', 'entity_select2', array(
                    'label' => "Collaborateur",
                    'class' => 'CollaborateurBundle:Collaborateur',
                    'query_builder'   => function (EntityRepository $er) {
                        return $er->getCollaborateurQuery();
                    },
                    'options' => array(
                        'placeholder' => "Sectionnez le collaborateur",
                        'language' => 'fr'
                    ),
                    'multiple'  => false,

                    'empty_value' => 'Sectionnez le collaborateur',
                ))



                ->add('email', 'email', array(
                    'label'    => 'Email',
                    'attr'     => array(
                        'input_group' => array(
                            'prepend' => '.icon-envelope'
                        )
                    ),
                    'required'      => false,
                ))
                ->add('phone', null, array(
                    'label'    => 'Numéro de téléphone',
                    'attr'     => array(
                        'input_group' => array(
                            'prepend' => '.icon-phone'
                        )
                    ),
                    'required'      => false,
                ))

				->add('active', null, array(
					'label' => "Activé",
					'required'      => false,
				));


        }





		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'AgenceBundle\Entity\AgenceCollaborateur',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'agencebundle_agencecollaborateur';
		}
	}
