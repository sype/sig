<?php

namespace AgenceBundle\Form;


	use Doctrine\ORM\EntityRepository;
    use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\FormInterface;
	use Symfony\Component\Form\FormView;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;
	use Uneak\AssetsManagerBundle\Assets\AssetBuilder;
	use Uneak\AssetsManagerBundle\Assets\Js\AssetInternalJs;
    use Uneak\FlatSkinBundle\Form\DataTransformer\IdToEntityTransformer;
	use Uneak\FormsManagerBundle\Forms\AssetsComponentType;

	class RouteAgenceType extends AssetsComponentType {

        protected $em;

        public function __construct($em) {
            $this->em = $em;
        }

		public function buildForm(FormBuilderInterface $builder, array $options) {

            $builder->add(
                $builder->create('id', 'entity_select2', array(
                    'label' => "Selectionnez une agence",
                    'class' => 'AgenceBundle:Agence',
                    'property' => 'label',
                    'query_builder'   => function (EntityRepository $er) {
                        return $er->getAgencesQuery();
                    },
                    'options' => array(
                        'language' => 'fr',
                    ),
                    'multiple'  => false,
                    'required' => false,
                    'empty_value' => "Sectionnez l'agence",

                ))->addModelTransformer(new IdToEntityTransformer($this->em, 'AgenceBundle:Agence'))
            );

		}




		public function setDefaultOptions(OptionsResolverInterface $resolver) {

			$resolver->setDefaults(array(
				'compound' => true
			));

		}



		public function getName() {
            return 'agencebundle_routeagence';
		}

	}
