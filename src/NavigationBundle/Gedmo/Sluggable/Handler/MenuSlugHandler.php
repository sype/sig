<?php

namespace NavigationBundle\Gedmo\Sluggable\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Gedmo\Sluggable\Handler\SlugHandlerInterface;
use Gedmo\Sluggable\SluggableListener;
use Gedmo\Sluggable\Mapping\Event\SluggableAdapter;
use Gedmo\Tool\Wrapper\AbstractWrapper;
use Gedmo\Exception\InvalidMappingException;

/**
 * Sluggable handler which slugs all parent nodes
 * recursively and synchronizes on updates. For instance
 * category tree slug could look like "food/fruits/apples"
 *
 * @author Gediminas Morkevicius <gediminas.morkevicius@gmail.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
class MenuSlugHandler implements SlugHandlerInterface
{
    const SEPARATOR = '/';

    /**
     * @var ObjectManager
     */
    protected $om;

    /**
     * @var SluggableListener
     */
    protected $sluggable;

    /**
     * @var string
     */
    private $prefix;

    /**
     * @var string
     */
    private $suffix;

    /**
     * True if node is being inserted
     *
     * @var boolean
     */
    private $isInsert = false;

    /**
     * Transliterated parent slug
     *
     * @var string
     */
    private $parentSlug;

    /**
     * Used path separator
     *
     * @var string
     */
    private $usedPathSeparator;

    /**
     * {@inheritDoc}
     */
    public function __construct(SluggableListener $sluggable)
    {
        $this->sluggable = $sluggable;
    }

    /**
     * {@inheritDoc}
     */
    public function onChangeDecision(SluggableAdapter $ea, array &$config, $object, &$slug, &$needToChangeSlug)
    {
        $this->om = $ea->getObjectManager();
        $this->isInsert = $this->om->getUnitOfWork()->isScheduledForInsert($object);
        $options = $config['handlers'][get_called_class()];

        $this->usedPathSeparator = isset($options['separator']) ? $options['separator'] : self::SEPARATOR;
        $this->prefix = isset($options['prefix']) ? $options['prefix'] : '';
        $this->suffix = isset($options['suffix']) ? $options['suffix'] : '';

        if (!$this->isInsert && !$needToChangeSlug) {
            $changeSet = $ea->getObjectChangeSet($this->om->getUnitOfWork(), $object);
            if (isset($changeSet[$options['parentRelationField']])) {
                $needToChangeSlug = true;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public function postSlugBuild(SluggableAdapter $ea, array &$config, $object, &$slug)
    {

        $options = $config['handlers'][get_called_class()];
        $this->parentSlug = '';

        $wrapped = AbstractWrapper::wrap($object, $this->om);
        $parent = $wrapped->getPropertyValue($options['parentRelationField']);

//        if ($parent) {  // pour avoir le pathSlug COMPLET de la racine au lien
        if ($parent && $parent->getRoot() != $parent->getId()) { // pour avoir le pathSlug sans la racine
            $parentWrapper = AbstractWrapper::wrap($parent, $this->om);

            $parentSlug = $parentWrapper->getPropertyValue($config['slug']);

            // if needed, remove suffix from parentSlug, so we can use it to prepend it to our slug
            if (isset($options['suffix'])) {
                $suffix = $options['suffix'];

                if (substr($parentSlug, -strlen($suffix)) === $suffix) { //endsWith
                    $parentSlug = substr_replace($parentSlug, '', -1 * strlen($suffix));
                }
            }

            if ($this->parentSlug) {
                $parentSlug = $parentSlug.$this->usedPathSeparator.$this->parentSlug;
            }
            $this->parentSlug = $parentSlug;

        }

    }


    /**
     * {@inheritDoc}
     */
    public static function validate(array $options, ClassMetadata $meta)
    {
        if (!$meta->isSingleValuedAssociation($options['parentRelationField'])) {
            throw new InvalidMappingException("Unable to find tree parent slug relation through field - [{$options['parentRelationField']}] in class - {$meta->name}");
        }
    }

    /**
     * {@inheritDoc}
     */
    public function onSlugCompletion(SluggableAdapter $ea, array &$config, $object, &$slug)
    {
        $options = $config['handlers'][get_called_class()];
        $parentSlug = $this->parentSlug;

        $wrapped = AbstractWrapper::wrap($object, $this->om);
        $meta = $wrapped->getMetadata();

        if ($options['relationSlugField']) {
            $slug = $wrapped->getPropertyValue($options['relationSlugField']);
        }

        $slug = $this->transliterate($parentSlug, $slug, $config['separator'], $object);

        if ($config['unique'] && null !== $slug) {
            $slug = $this->makeUniqueSlug($ea, $object, $slug, $config);
        }


        if (!$this->isInsert) {
            $target = $wrapped->getPropertyValue($config['slug']);
            $config['pathSeparator'] = $this->usedPathSeparator;
            $oldSlug = $target.$config['pathSeparator'];

            $ea->replaceRelative($object, $config, $oldSlug, $slug);

            $uow = $this->om->getUnitOfWork();
            // update in memory objects

            foreach ($uow->getIdentityMap() as $className => $objects) {
                // for inheritance mapped classes, only root is always in the identity map
                if ($className !== $wrapped->getRootObjectName()) {
                    continue;
                }
                foreach ($objects as $object) {
                    if (property_exists($object, '__isInitialized__') && !$object->__isInitialized__) {
                        continue;
                    }
                    $oid = spl_object_hash($object);
                    $objectSlug = $meta->getReflectionProperty($config['slug'])->getValue($object);
                    if (preg_match("@^{$target}{$config['pathSeparator']}@smi", $objectSlug)) {
                        $objectSlug = str_replace($target, $slug, $objectSlug);

                        $meta->getReflectionProperty($config['slug'])->setValue($object, $objectSlug);
                        $ea->setOriginalObjectProperty($uow, $oid, $config['slug'], $objectSlug);
                    }
                }

            }
        }




    }







    /**
     * Generates the unique slug
     *
     * @param SluggableAdapter $ea
     * @param object           $object
     * @param string           $preferredSlug
     * @param array            $config[$slugField]
     *
     * @return string - unique slug
     */
    private function makeUniqueSlug(SluggableAdapter $ea, $object, $preferredSlug, $config = array())
    {
        $om = $ea->getObjectManager();
        $meta = $om->getClassMetadata(get_class($object));
        $exponent = 0;

        $result = (array) $ea->getSimilarSlugs($object, $meta, $config, $preferredSlug);

        foreach ($result as $key => $similar) {
            if (!preg_match("@{$preferredSlug}($|{$config['separator']}[\d]+$)@smi", $similar[$config['slug']])) {
                unset($result[$key]);
            }
        }

        if ($result) {
            $sameSlugs = array();

            foreach ((array) $result as $list) {
                $sameSlugs[] = $list[$config['slug']];
            }

            $i = pow(10, $exponent);
            do {
                $generatedSlug = $preferredSlug.$config['separator'].$i++;
            } while (in_array($generatedSlug, $sameSlugs));

            $mapping = $meta->getFieldMapping($config['slug']);
            if (isset($mapping['length']) && strlen($generatedSlug) > $mapping['length']) {
                $generatedSlug = substr(
                    $generatedSlug,
                    0,
                    $mapping['length'] - (strlen($i) + strlen($config['separator']))
                );
                $this->exponent = strlen($i) - 1;
                if (substr($generatedSlug,-strlen($config['separator'])) == $config['separator']) {
                    $generatedSlug = substr($generatedSlug,0,strlen($generatedSlug) - strlen($config['separator']));
                }
                $generatedSlug = $this->makeUniqueSlug($ea, $object, $generatedSlug, true, $config);
            }
            $preferredSlug = $generatedSlug;
        }

        return $preferredSlug;
    }



    /**
     * Transliterates the slug and prefixes the slug
     * by collection of parent slugs
     *
     * @param string $text
     * @param string $separator
     * @param object $object
     *
     * @return string
     */
    public function transliterate($parentSlug, $text, $separator, $object)
    {
        $slug = $text . $this->suffix;

        if (strlen($parentSlug)) {
            $slug = $parentSlug.$this->usedPathSeparator.$slug;
        } else {
            // if no parentSlug, apply our prefix
            $slug = $this->prefix.$slug;
        }

        return $slug;
    }




    /**
     * {@inheritDoc}
     */
    public function handlesUrlization()
    {
        return false;
    }
}
