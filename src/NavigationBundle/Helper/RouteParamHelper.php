<?php

	namespace NavigationBundle\Helper;


	use Doctrine\ORM\EntityManager;


	class RouteParamHelper {

		private $em;

		public function __construct(EntityManager $em) {
			$this->em = $em;
		}


        public function getParams($route) {

            $routeId = $route["route"];
            $routeParams = $route[$routeId];
            $params = array();

            switch ($routeId) {
                case "page":
                    $entity = $this->em->getRepository('NavigationBundle:Page')->findPageBy(true, $routeParams);
                    $params['slug'] = $entity->getSlug();
                    break;
                case "agence":
                    $entity = $this->em->getRepository('AgenceBundle:Agence')->findAgenceBy(true, $routeParams);
                    $params['slug'] = $entity->getSlug();
                    break;
                case "projet":
                    $entity = $this->em->getRepository('ProjetBundle:Projet')->findProjetBy(true, $routeParams);
                    $params['slug'] = $entity->getSlug();
                    break;
                case "news":
                    $entity = $this->em->getRepository('NewsBundle:News')->findNewsBy(true, $routeParams);
                    $params['slug'] = $entity->getSlug();
                    break;
                case "newscategorie":
                    $entity = $this->em->getRepository('NewsBundle:NewsCategory')->findNewsCategoryBy(true, $routeParams);
                    $params['slug'] = $entity->getSlug();
                    break;
                case "gallery":
                    $entity = $this->em->getRepository('GalleryBundle:Gallery')->findGalleryBy(true, $routeParams);
                    $params['slug'] = $entity->getSlug();
                    break;
            }


            return $params;
        }



	}
