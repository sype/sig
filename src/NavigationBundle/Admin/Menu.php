<?php

	namespace NavigationBundle\Admin;

	use Uneak\RoutesManagerBundle\Routes\NestedAdminRoute;
	use Uneak\RoutesManagerBundle\Routes\NestedCRUDRoute;
	use Uneak\RoutesManagerBundle\Routes\NestedEntityRoute;
	use Uneak\RoutesManagerBundle\Routes\NestedGridRoute;

	class Menu extends NestedCRUDRoute {

		protected $entity = 'NavigationBundle\Entity\MenuItem';

		public function initialize() {
			parent::initialize();

			$this->setMetaData('_icon', 'sitemap');
			$this->setMetaData('_label', 'Menus de navigation');
			$this->setMetaData('_description', 'Gestion des menus');
			$this->setMetaData('_menu', array(
				'index'  => '*/index',
				'new'    => '*/new',
			));
		}


		protected function buildCRUD() {
			$indexRoute = new NestedGridRoute('index');
			$indexRoute
				->setPath('')
				->setAction('index')
				->setMetaData('_icon', 'list')
				->setMetaData('_label', 'Liste')
				->addRowAction('show', '*/subject/show')
				->addRowAction('edit', '*/subject/edit')
				->addRowAction('delete', '*/subject/delete')

				->addColumn(array('title' => 'Titre', 'name' => 'label'))
			;
			$this->addChild($indexRoute);


			$newRoute = new NestedAdminRoute('new');
			$newRoute
				->setPath('new')
				->setAction('new')
				->setMetaData('_icon', 'plus-circle')
				->setMetaData('_label', 'New');
			$this->addChild($newRoute);


			$subjectRoute = new NestedEntityRoute('subject');
			$subjectRoute
				->setParameterName($this->getId())
				->setParameterPattern('\d+')
				->setEnabled(false)
				->setMetaData('_menu', array(
					'show'   => '*/subject/show',
					'edit'   => '*/subject/edit',
					'delete' => '*/subject/delete',
				));
			$this->addChild($subjectRoute);


			$showRoute = new NestedAdminRoute('show');
			$showRoute
				->setAction('show')
				->setMetaData('_icon', 'eye')
				->setMetaData('_label', 'Show')
				->setRequirement('_method', 'GET')
                ->setGrantFunction(array($this, "isRootGranted"));
			$subjectRoute->addChild($showRoute);


			$editRoute = new NestedAdminRoute('edit');
			$editRoute
				->setAction('edit')
				->setMetaData('_icon', 'edit')
				->setMetaData('_label', 'Edit')
                ->setGrantFunction(array($this, "isPrivateMenuGranted"));
			$subjectRoute->addChild($editRoute);


			$deleteRoute = new NestedAdminRoute('delete');
			$deleteRoute
				->setAction('delete')
				->setMetaData('_icon', 'times')
				->setMetaData('_label', 'Delete')
                ->setGrantFunction(array($this, "isPrivateMenuGranted"));
			$subjectRoute->addChild($deleteRoute);
		}


        public function isRootGranted($attribute, $flattenRoute, $user = null) {
            $menuItem = $flattenRoute->getParameter("menu")->getParameterSubject();
            return (!$menuItem->getParent());
        }

        public function isPrivateMenuGranted($attribute, $flattenRoute, $user = null) {
            $menuItem = $flattenRoute->getParameter("menu")->getParameterSubject();
            return (!$menuItem->isPrivate());
        }


    }
