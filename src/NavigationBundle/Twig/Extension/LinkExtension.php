<?php

	namespace NavigationBundle\Twig\Extension;

    use NavigationBundle\Helper\RouteParamHelper;
    use Symfony\Component\DependencyInjection\ContainerInterface;
	use Twig_Extension;

	class LinkExtension extends Twig_Extension {

        private $routeParamHelper;
        private $container;

        public function __construct(RouteParamHelper $routeParamHelper, ContainerInterface $container) {
            $this->routeParamHelper = $routeParamHelper;
            $this->container = $container;
        }

		public function getFunctions() {
			$options = array('pre_escape' => 'html', 'is_safe' => array('html'));

			return array(
				'route'   => new \Twig_Function_Method($this, 'routeFunction', $options)
			);
		}

		public function routeFunction($route) {

            if ($route["route"] == "uri") {
                return $route["uri"];
            }

            return $this->container->get('router')->generate($route["route"], $this->routeParamHelper->getParams($route));

		}

		public function getName() {
			return 'uneak_navigation_route';
		}

	}
