<?php

namespace NavigationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NavigationBundle\Entity\MenuItem;

class LoadMenuData implements FixtureInterface {

    private $data_menus = array(
        array(
            'label' => 'Menu principal',
            'slug' => 'site-primary',
            'private' => true
        ),
        array(
            'label' => 'Menu secondaire',
            'slug' => 'site-secondary',
            'private' => true
        ),
        array(
            'label' => 'Menu pied de page',
            'slug' => 'site-footer',
            'private' => true
        ),

    );

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager) {

        foreach ($this->data_menus as $data_menu) {
            $menuItem = new MenuItem();
            $menuItem->setLabel($data_menu['label']);
            $menuItem->setSlug($data_menu['slug']);
            $menuItem->setPrivate($data_menu['private']);
            $manager->persist($menuItem);
        }
        $manager->flush();
    }

}
