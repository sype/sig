<?php

namespace NavigationBundle\Controller;

use NavigationBundle\Entity\MenuItem;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{



    /**
     * @Route("/_navigation/menu/add", name="_addNavigationMenu")
     */
    public function addNavigationMenu(Request $request) {

        $render = '';
        $id = 0;

        if ($request->isXmlHttpRequest()) {

            $em = $this->get('doctrine')->getEntityManager();
            $repository = $em->getRepository('NavigationBundle:MenuItem');
            $menuitem_id = $request->request->get('data');

            $parentItem = $repository->find($menuitem_id);

            $childItem = new MenuItem();
            $childItem->setLabel("Nouveau menu");
            $childItem->setParent($parentItem);

            $em->persist($childItem);
            $em->flush();

            $userBlock = $this->get('uneak.admin.menu.block.helper');
            $item = $userBlock->getMenuItemEditor($childItem);

            $render = $this->renderView('UneakFlatSkinBundle:Block:NestedSortable/nested_sortable_item.html.twig', $item);
            $id = $childItem->getId();
        }

        return new JsonResponse(array('id' => $id, 'render' => $render));
    }


    /**
     * @Route("/_navigation/menu/reorder", name="_reorderNavigationMenu")
     */
    public function reorderNavigationMenu(Request $request) {

        if ($request->isXmlHttpRequest()) {
            $data = $request->request->get("data");
            $em = $this->get('doctrine')->getEntityManager();
            $repository = $em->getRepository('NavigationBundle:MenuItem');

            $menuObject = array();
            foreach ($data as $link) {
                $menuObject[$link["item_id"]] = $repository->find($link["item_id"]);
            }
            foreach ($data as $link) {
                $menuItem = $menuObject[$link["item_id"]];
                $menuItem->setLvl($link["depth"]);
                $menuItem->setLft($link["left"]);
                $menuItem->setRgt($link["right"]);
                if ($link["parent_id"] && $link["parent_id"] != 'none') {
                    $menuItem->setParent($menuObject[$link["parent_id"]]);
                } else {
                    $menuItem->setParent();
                }
            }
            $em->flush();
        }

        return new Response('');

    }

}


