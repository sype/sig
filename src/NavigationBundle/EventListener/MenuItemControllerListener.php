<?php

namespace NavigationBundle\EventListener;

use Doctrine\ORM\EntityManager;
use NavigationBundle\Helper\RouteParamHelper;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class MenuItemControllerListener {

    protected $router;
    protected $routes;
    protected $controllerResolver;
    protected $em;
    protected $routeParamHelper;

    public function __construct(Router $router, $routes, ControllerResolver $controllerResolver, EntityManager $em, RouteParamHelper $routeParamHelper) {
        $this->router = $router;
        $this->routes = $routes;
        $this->controllerResolver = $controllerResolver;
        $this->em = $em;
        $this->routeParamHelper = $routeParamHelper;
    }

    public function onKernelController(FilterControllerEvent $event) {

        $request = $event->getRequest();
        $routeCollection = $this->router->getRouteCollection();
        if ($routeCollection->get($request->attributes->get('_route'))) {
            return;
        }

        $pathInfo = trim($request->getPathInfo(), "/");
        $repository = $this->em->getRepository('NavigationBundle:MenuItem');
        $menuItem = $repository->findByPathSlug($pathInfo);

        if (!$menuItem || false === $route = $menuItem->getRoute()) {
            return;
        }

        if ($route["route"] == 'uri') {
            return;
        }


        $params = array_merge($routeCollection->get($route["route"])->getDefaults(), $this->routeParamHelper->getParams($route));
        $defaults = $this->routes[$route["route"]]["route"]["defaults"];
        $attributes = array_merge($defaults, $params, array('menuItem' => $menuItem));

        foreach ($attributes as $key => $attribute) {
            $request->attributes->set($key, $attribute);
        }


        if (false === $controller = $this->controllerResolver->getController($request)) {
            return;
        }

        $event->setController($controller);

    }


}
