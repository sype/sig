<?php

	namespace NavigationBundle\Block;


	use Doctrine\ORM\EntityManager;
	use NavigationBundle\Entity\MenuItem;
	use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
    use Uneak\FlatSkinBundle\Block\Component\NestedSortable;
    use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
    use Uneak\FlatSkinBundle\Block\Info\Info;
	use Uneak\FlatSkinBundle\Block\Info\Infos;
	use Uneak\FlatSkinBundle\Block\Menu\Menu as BlockMenu;
	use Uneak\FlatSkinBundle\Block\Panel\ProfileNav;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

	class Menu {

		private $blockManager;
		private $routeManager;
		private $menuHelper;
		private $vichHelper;
		private $autorization;
		private $em;

		public function __construct(Block $blockManager, FlattenRouteManager $routeManager, MenuHelper $menuHelper, UploaderHelper $vichHelper, AuthorizationChecker $autorization, EntityManager $em) {
			$this->blockManager = $blockManager;
			$this->routeManager = $routeManager;
			$this->menuHelper = $menuHelper;
			$this->vichHelper = $vichHelper;
			$this->autorization = $autorization;
			$this->em = $em;
		}



        public function getMenuEditor(MenuItem $menu) {

            $repository = $this->em->getRepository('NavigationBundle:MenuItem');
            $completeMenu = $repository->findMenu(false, $menu->getId());

            $menuLinks = array();
            $nestedSortable = new NestedSortable();

            foreach ($completeMenu as $menuItem) {
                $menuLinks[] = $this->getMenuItemEditor($menuItem);
            }

            $nestedSortable->setMenu($menuLinks);

            $nsWrapper = new Wrapper();
            $nsWrapper
                ->setTitle($menu->getLabel())
                ->setCollapsable(false)
                ->addBlock($nestedSortable);

            return $nsWrapper;
        }


        public function getMenuItemEditor(MenuItem $menuItem) {

            $rowMenuTemplate = "UneakFlatSkinBundle:Block:Menu/block_menu.html.twig";

            $blockMenu = $this->getMenuNavigationMenu($menuItem);
            $blockMenu->setTemplate($rowMenuTemplate);
            $blockMenu->setMeta("parentTag", "div");
            $blockMenu->setMeta("childrenTag", "span");

            $menuLink = array(
                "item" => $menuItem,
                "blockMenu" => $blockMenu
            );

            return $menuLink;
        }


		public function getInfos(MenuItem $menu) {


			$infos = new Infos();
			$infos
				->setColumns(1)
				->setStripeRow(true)
				->addBlock(new Info("Titre", $menu->getLabel()), null, 40)
//				->addBlock(new Info("Slug", $menu->getSlug()), null, 30)
                ->addBlock(new Info("Activé", ($menu->getActive()) ? "Oui" : "Non"), null, 0);

			$wrapper = new Wrapper();
			$wrapper
				->setTitle("Informations")
				->addBlock($infos);

			return $wrapper;
		}



		public function getNavigation() {

			$flattenCrud = $this->routeManager->getFlattenRoute('menu');

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($flattenCrud->getMetaData("_label"))
				->setDescription($flattenCrud->getMetaData("_description"))
				->setColor("green")
				->setHeaderAlign('vertical');
			$profileNav->addBlock($this->getNavigationMenu());

			return $profileNav;

		}

		public function getNavigationMenu() {

			$flattenCrud = $this->routeManager->getFlattenRoute('menu');

			$menu = new BlockMenu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud);
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}

		public function getMenuNavigation(MenuItem $menu) {

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($menu->getLabel())
				->setColor("green")
				->setHeaderAlign('horizontal');
			$profileNav->addBlock($this->getMenuNavigationMenu($menu));

			return $profileNav;

		}

		public function getMenuNavigationMenu(MenuItem $menu) {

			$flattenCrud = $this->routeManager->getFlattenRoute('menu/subject');

			$blockMenu = new BlockMenu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('menu' => $menu->getId()));
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
            $blockMenu->setRoot($root);

			return $blockMenu;

		}



	}
