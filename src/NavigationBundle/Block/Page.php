<?php

	namespace NavigationBundle\Block;


	use Doctrine\ORM\EntityManager;
	use NavigationBundle\Entity\Page as PageEntity;
	use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
	use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
    use Uneak\FlatSkinBundle\Block\Info\Info;
	use Uneak\FlatSkinBundle\Block\Info\Infos;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\ProfileNav;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

	class Page {

		private $blockManager;
		private $routeManager;
		private $menuHelper;
		private $vichHelper;
		private $autorization;
		private $em;

		public function __construct(Block $blockManager, FlattenRouteManager $routeManager, MenuHelper $menuHelper, UploaderHelper $vichHelper, AuthorizationChecker $autorization, EntityManager $em) {
			$this->blockManager = $blockManager;
			$this->routeManager = $routeManager;
			$this->menuHelper = $menuHelper;
			$this->vichHelper = $vichHelper;
			$this->autorization = $autorization;
			$this->em = $em;
		}


		public function getInfos(PageEntity $page) {


			$infos = new Infos();
			$infos
				->setColumns(1)
				->setStripeRow(true)
				->addBlock(new Info("Titre", $page->getLabel()), null, 80)
				->addBlock(new Info("Slug", $page->getSlug()), null, 70)
				->addBlock(new Info("Description", $page->getDescription()), null, 60)
                ->addBlock(new Info("Activé", ($page->getActive()) ? "Oui" : "Non"), null, 0);

			$wrapper = new Wrapper();
			$wrapper
				->setTitle("Informations")
				->addBlock($infos);

			return $wrapper;
		}



		public function getNavigation() {

			$flattenCrud = $this->routeManager->getFlattenRoute('page');

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($flattenCrud->getMetaData("_label"))
				->setDescription($flattenCrud->getMetaData("_description"))
				->setColor("green")
				->setHeaderAlign('vertical');
			$profileNav->addBlock($this->getNavigationMenu());

			return $profileNav;

		}

		public function getNavigationMenu() {

			$flattenCrud = $this->routeManager->getFlattenRoute('page');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud);
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}

		public function getPageNavigation(PageEntity $page) {

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($page->getLabel())
				->setPhoto($this->vichHelper->asset($page, 'imageFile'))
				->setColor("green")
				->setHeaderAlign('horizontal');
			$profileNav->addBlock($this->getPageNavigationMenu($page));

			return $profileNav;

		}

		public function getPageNavigationMenu(PageEntity $page) {

			$flattenCrud = $this->routeManager->getFlattenRoute('page/subject');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('page' => $page->getId()));
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}



	}
