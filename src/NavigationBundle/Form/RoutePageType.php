<?php

namespace NavigationBundle\Form;


	use Doctrine\ORM\EntityRepository;
    use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\FormInterface;
	use Symfony\Component\Form\FormView;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;
	use Uneak\AssetsManagerBundle\Assets\AssetBuilder;
	use Uneak\AssetsManagerBundle\Assets\Js\AssetInternalJs;
    use Uneak\FlatSkinBundle\Form\DataTransformer\IdToEntityTransformer;
	use Uneak\FormsManagerBundle\Forms\AssetsComponentType;

	class RoutePageType extends AssetsComponentType {

        protected $em;

        public function __construct($em) {
            $this->em = $em;
        }

		public function buildForm(FormBuilderInterface $builder, array $options) {

            $builder->add(
                $builder->create('id', 'entity_select2', array(
                    'label' => "Selectionnez une page",
                    'class' => 'NavigationBundle:Page',
                    'property' => 'label',
                    'query_builder'   => function (EntityRepository $er) {
                        return $er->getPagesQuery();
                    },
                    'options' => array(
                        'language' => 'fr',
                    ),
                    'multiple'  => false,
                    'required' => false,
                    'empty_value' => "Sectionnez la page",

                ))->addModelTransformer(new IdToEntityTransformer($this->em, 'NavigationBundle:Page'))
            );

		}


		public function buildView(FormView $view, FormInterface $form, array $options) {

		}


		public function setDefaultOptions(OptionsResolverInterface $resolver) {

			$resolver->setDefaults(array(
				'compound' => true
			));

		}





		public function buildAsset(AssetBuilder $builder, $parameters) {
            $builder
                ->add("script_route", new AssetInternalJs(), array(
                    "template" => "NavigationBundle:Block/Form:route_page_script.html.twig",
                    "parameters" => array('item' => $parameters)
                ));
		}

//		public function getTheme() {
//			return "UneakFlatSkinBundle:Form:route/route.html.twig";
//		}


		public function getName() {
            return 'navigationbundle_routepage';
		}

	}
