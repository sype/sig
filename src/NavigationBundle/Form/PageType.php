<?php

	namespace NavigationBundle\Form;

	use Doctrine\ORM\EntityRepository;
	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;

	class PageType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('label', null, array(
					'label' => "Titre",
				))

                ->add('slug', null, array(
                    'label' => "Slug",
                ))

				->add('description', 'ckeditor', array(
					'label' => "Description",
                    'options' => array(
                        'customConfig' => '../../../bundles/app/js/ckeditor_config.js',
                    ),
				))
				->add('imageFile', 'vich_file', array(
						'label'         => "Photo",
						'allow_delete'  => true, // not mandatory, default is true
						'download_link' => true, // not mandatory, default is true
						'required'      => false,
					)
				)

				->add('active', null, array(
					'label'    => "Activé",
					'required' => false,
				));

		}


		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'NavigationBundle\Entity\Page',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'navigationbundle_page';
		}
	}
