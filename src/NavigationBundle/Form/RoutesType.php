<?php

namespace NavigationBundle\Form;

	use AgenceBundle\Form\RouteAgenceType;
    use GalleryBundle\Form\RouteGalleryType;
    use NewsBundle\Form\RouteCategoryType;
    use NewsBundle\Form\RouteNewsType;
    use ProjetBundle\Form\RouteProjetType;
    use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\FormInterface;
	use Symfony\Component\Form\FormView;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;
	use Uneak\AssetsManagerBundle\Assets\AssetBuilder;
	use Uneak\AssetsManagerBundle\Assets\Js\AssetInternalJs;
	use Uneak\FormsManagerBundle\Forms\AssetsComponentType;

	class RoutesType extends AssetsComponentType {

        protected $em;

        protected $routes = array(
            'page' => 'Page',
            'agence' => 'Agence',
            'projet' => 'Projet',
            'news' => 'Article',
            'newscategorie' => "Categorie d'article",
            'gallery' => 'Galerie',
            'uri' => "Lien externe",
        );

        public function __construct($em) {
            $this->em = $em;
        }

		public function buildForm(FormBuilderInterface $builder, array $options) {

            $builder->add('route', 'choice_select2', array(
                'label' => "Selectionnez le type de route",
                'choices'   => $this->routes,

                'options' => array(
                    'language' => 'fr',
                ),
                'multiple'  => false,

                'empty_value' => 'Sectionnez la route',
                'required' => false,
            ));


            $builder->add('page', new RoutePageType($this->em), array(
                'label' => "Page",
                'required' => false,
            ));

            $builder->add('agence', new RouteAgenceType($this->em), array(
                'label' => "Agence",
                'required' => false,
            ));

            $builder->add('projet', new RouteProjetType($this->em), array(
                'label' => "Projet",
                'required' => false,
            ));

            $builder->add('news', new RouteNewsType($this->em), array(
                'label' => "Article",
                'required' => false,
            ));

            $builder->add('newscategorie', new RouteCategoryType($this->em), array(
                'label' => "Catégorie d'article",
                'required' => false,
            ));

            $builder->add('gallery', new RouteGalleryType($this->em), array(
                'label' => "Galerie",
                'required' => false,
            ));

            $builder->add('uri', 'url', array(
                'label' => "Url",
                'required' => false,
            ));

		}


		public function buildView(FormView $view, FormInterface $form, array $options) {
            $view->vars['routes'] = $this->routes;

		}


		public function setDefaultOptions(OptionsResolverInterface $resolver) {

			$resolver->setDefaults(array(
				'compound' => true
			));


		}





		public function buildAsset(AssetBuilder $builder, $parameters) {


//            ldd($parameters);

            $builder

                ->add("script_routes", new AssetInternalJs(), array(
                    "template" => "NavigationBundle:Block/Form:routes_script.html.twig",
                    "parameters" => array('item' => $parameters)
                ));

		}

//		public function getTheme() {
//			return "UneakFlatSkinBundle:Form:route/route.html.twig";
//		}


		public function getName() {
			return 'routes';
		}

	}
