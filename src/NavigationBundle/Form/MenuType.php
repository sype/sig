<?php

	namespace NavigationBundle\Form;

	use Doctrine\ORM\EntityRepository;
	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;

	class MenuType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('label', null, array(
					'label' => "Titre",
				))

                ->add('slug', null, array(
                    'label' => "Slug",
                    'required' => false,
                ))

                ->add('icon', null, array(
                    'label' => "Icon",
                    'attr' => array(
                        'help_text' => 'Voir le site <a target="_blank" href="https://fortawesome.github.io/Font-Awesome/icons/">https://fortawesome.github.io/Font-Awesome/icons</a>'
                    )
                ))

                ->add('route', 'routes', array(
                    'label' => "Route",
                    'required' => false,
                ))

				->add('active', null, array(
					'label'    => "Activé",
					'required' => false,
				));

		}


		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'NavigationBundle\Entity\MenuItem',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'navigationbundle_menu';
		}
	}
