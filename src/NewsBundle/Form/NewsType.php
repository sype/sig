<?php

namespace NewsBundle\Form;

use Doctrine\ORM\EntityRepository;
use NewsBundle\Entity\News;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Uneak\FormsManagerBundle\Forms\AssetsComponentType;

class NewsType extends AssetsComponentType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

			->add('category', 'entity_select2', array(
				'label' => "Catégorie",
				'class' => 'NewsBundle:NewsCategory',
				'query_builder'   => function (EntityRepository $er) {
					return $er->getNewsCategoryQuery();
				},
				'options' => array(
					'language' => 'fr'
				),
				'multiple'  => false,

				'empty_value' => 'Sectionnez la catégorie',
			))


            ->add('published', 'date_picker', array(
                'label' => "Date de publication",
                'input' => "datetime",
                //					'format' => 'dd/MM/yyyy',

                'options' => array(
                    'locale' => 'fr',
                    'icons' => array(
                        'time' => "fa fa-clock-o",
                        'date' => "fa fa-calendar",
                        'up' => "fa fa-arrow-up",
                        'down' => "fa fa-arrow-down",
                        'previous' => 'fa fa-chevron-left',
                        'next' => 'fa fa-chevron-right',
                        'today' => 'fa fa-check-circle-o',
                        'clear' => 'fa fa-trash',
                    ),
                    'showTodayButton' => true,
                    'sideBySide' => true,
                    'useCurrent' => true,
                ),
            ))
            ->add('date', 'date_picker', array(
                'label' => "Date de l'article",
                'input' => "datetime",
                'format' => 'dd/MM/yyyy',

                'options' => array(
                    'locale' => 'fr',
                    'icons' => array(
                        'time' => "fa fa-clock-o",
                        'date' => "fa fa-calendar",
                        'up' => "fa fa-arrow-up",
                        'down' => "fa fa-arrow-down",
                        'previous' => 'fa fa-chevron-left',
                        'next' => 'fa fa-chevron-right',
                        'today' => 'fa fa-check-circle-o',
                        'clear' => 'fa fa-trash',
                    ),
                    'showTodayButton' => true,
                    'useCurrent' => true,
                ),
            ))
            ->add('label', null, array(
                'label' => "Titre",
            ))

            ->add('slug', null, array(
                'label' => "Slug",
            ))
            ->add('shortDescription', 'ckeditor', array(
                'label' => "Description courte",
                'options' => array(
                    'customConfig' => '../../../bundles/app/js/ckeditor_config.js',
                ),
            ))
            ->add('description', 'ckeditor', array(
                'label' => "Description",
                'options' => array(
                    'customConfig' => '../../../bundles/app/js/ckeditor_config.js',
                ),
            ))
            ->add('imageFile', 'vich_file', array(
                    'label' => "Photo",
                    'required' => false,
                    'allow_delete' => true, // not mandatory, default is true
                    'download_link' => true, // not mandatory, default is true
                    'required' => false,
                )
            )
            ->add('tags', 'entity_select2', array(
                'label' => "Etiquettes",
                'class' => 'NewsBundle:Tag',
                'property' => 'label',

                'options' => array(
                    'language' => 'fr',
                    'tags' => true,
                    'tokenSeparators' => array(',', ' '),
                ),

                'multiple' => true,

                'empty_value' => 'Définissez les étiquettes',
            ))

            ->add('images', 'bootstrap_collection', array(
                'label' => "Galerie d'images",
                'by_reference' => false,
                'type' => new NewsImageType(),
                'allow_add' => true,
                'allow_delete' => true,
                'add_button_text' => "Ajouter une image",
                'delete_button_text' => "x",
                'sub_widget_col' => 10,
                'button_col' => 2
            ))
            ->add('active', null, array(
                'label' => "Activé",
                'required' => false,
            ));


    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'NewsBundle\Entity\News',
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'newsbundle_news';
    }
}
