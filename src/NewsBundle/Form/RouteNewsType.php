<?php

namespace NewsBundle\Form;


	use Doctrine\ORM\EntityRepository;
    use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\FormInterface;
	use Symfony\Component\Form\FormView;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;
	use Uneak\AssetsManagerBundle\Assets\AssetBuilder;
	use Uneak\AssetsManagerBundle\Assets\Js\AssetInternalJs;
    use Uneak\FlatSkinBundle\Form\DataTransformer\IdToEntityTransformer;
	use Uneak\FormsManagerBundle\Forms\AssetsComponentType;

	class RouteNewsType extends AssetsComponentType {

        protected $em;

        public function __construct($em) {
            $this->em = $em;
        }

		public function buildForm(FormBuilderInterface $builder, array $options) {

            $builder->add(
                $builder->create('id', 'entity_select2', array(
                    'label' => "Selectionnez un article",
                    'class' => 'NewsBundle:News',
                    'property' => 'label',
                    'query_builder'   => function (EntityRepository $er) {
                        return $er->getNewsQuery();
                    },
                    'options' => array(
                        'language' => 'fr',
                    ),
                    'multiple'  => false,
                    'required' => false,
                    'empty_value' => "Sectionnez l'article",

                ))->addModelTransformer(new IdToEntityTransformer($this->em, 'NewsBundle:News'))
            );

		}


		public function buildView(FormView $view, FormInterface $form, array $options) {

		}


		public function setDefaultOptions(OptionsResolverInterface $resolver) {

			$resolver->setDefaults(array(
				'compound' => true
			));

		}





		public function buildAsset(AssetBuilder $builder, $parameters) {
            $builder
                ->add("script_route", new AssetInternalJs(), array(
                    "template" => "NavigationBundle:Block/Form:route_page_script.html.twig",
                    "parameters" => array('item' => $parameters)
                ));
		}

//		public function getTheme() {
//			return "UneakFlatSkinBundle:Form:route/route.html.twig";
//		}


		public function getName() {
            return 'newsbundle_routenews';
		}

	}
