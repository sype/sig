<?php

	namespace NewsBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;

	class NewsCategoryType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder

				->add('label', null, array(
					'label' => "Titre",
				))

                ->add('slug', null, array(
                    'label' => "Slug",
                ))

				->add('active', null, array(
					'label' => "Activé",
					'required'      => false
				));

		}



		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'NewsBundle\Entity\NewsCategory',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'newsbundle_newscategory';
		}
	}
