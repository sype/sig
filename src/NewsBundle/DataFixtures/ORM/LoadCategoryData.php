<?php

namespace NewsBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NewsBundle\Entity\NewsCategory;

class LoadCategoryData implements FixtureInterface {

    private $data_categories = array(
        array(
            'label' => 'Article de blog',
            'slug' => 'blog-category',
            'private' => true
        ),

    );

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager) {

        foreach ($this->data_categories as $data_category) {
            $category = new NewsCategory();
            $category->setLabel($data_category['label']);
            $category->setSlug($data_category['slug']);
            $category->setPrivate($data_category['private']);
            $manager->persist($category);
        }
        $manager->flush();
    }

}
