<?php

	namespace NewsBundle\Controller;

    use NewsBundle\Entity\NewsCategory;
    use NewsBundle\Form\NewsCategoryType;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoute;
	use Uneak\FlatSkinBundle\Block\Component\DataTable;
	use Uneak\FlatSkinBundle\Block\DataTable\DataTableFilters;
	use Uneak\FlatSkinBundle\Block\Form\Form;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Doctrine\ORM\Query\Expr;

	class AdminNewsCategoryController extends Controller {


		public function indexAction( FlattenRoute $route) {
			$blockManager = $this->get("uneak.blocksmanager");

			$newscategoryBlock = $this->get('uneak.admin.newsCategory.block.helper');
			$blockManager->addBlock($newscategoryBlock->getNavigation(), 'navigation');

			$gridNested = $route->getNestedRoute();

			$datatable = new DataTable();
			$datatable->setAjax($route->getChild('_grid')->getRoutePath());
			$datatable->setColumns($gridNested->getColumns());

			$datatableWrapper = new Wrapper();
			$datatableWrapper
				->setTitle("Resultats")
				->setCollapsable(false)
				->addBlock($datatable);
			$blockManager->addBlock($datatableWrapper, 'datatable');

			$datatableFilterWrapper = new Wrapper();
			$datatableFilterWrapper
				->setTitle("Filtres")
				->setCollapsable(true)
				->addBlock(new DataTableFilters($datatable));
			$blockManager->addBlock($datatableFilterWrapper, 'datatable_filters');

			return $this->render('NewsBundle:Admin:newsCategory/index.html.twig');

		}



		public function showAction($newscategory) {
			$blockManager = $this->get("uneak.blocksmanager");

			$newscategoryBlock = $this->get('uneak.admin.newsCategory.block.helper');

			$blockManager->addBlock($newscategoryBlock->getInfos($newscategory), 'infos');
			$blockManager->addBlock($newscategoryBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($newscategoryBlock->getNewsCategoryNavigation($newscategory), 'user_navigation');

			return $this->render('NewsBundle:Admin:newsCategory/show.html.twig');

		}


		public function editAction($newscategory, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");


			$newscategoryBlock = $this->get('uneak.admin.newsCategory.block.helper');
			$blockManager->addBlock($newscategoryBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($newscategoryBlock->getNewsCategoryNavigation($newscategory), 'user_navigation');


            $em = $this->getDoctrine()->getManager();


            $form = $this->createForm(new NewsCategoryType(), $newscategory);
			$form->add('submit', 'submit', array('label' => 'Modifier'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

                    $em->flush();


					return $this->redirect($route->getChild('*/subject/show', array('newscategory' => $newscategory->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("NewsBundle:Block:Form/block_newscategory_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Edition de la catégorie ".$newscategory->getLabel())
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('NewsBundle:Admin:newsCategory/edit.html.twig');

		}


		public function newAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$newscategoryBlock = $this->get('uneak.admin.newsCategory.block.helper');
			$blockManager->addBlock($newscategoryBlock->getNavigation(), 'navigation');

			$newscategory = new NewsCategory();

			$form = $this->createForm(new NewsCategoryType(), $newscategory);
			$form->add('submit', 'submit', array('label' => 'Creer'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($newscategory);
                    $em->flush();

					return $this->redirect($route->getChild('*/subject/show', array('newscategory' => $newscategory->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("NewsBundle:Block:Form/block_newscategory_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Création d'une catégorie")
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');

			return $this->render('NewsBundle:Admin:newsCategory/new.html.twig');


		}


		public function deleteAction($newscategory, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$newscategoryBlock = $this->get('uneak.admin.newsCategory.block.helper');
			$blockManager->addBlock($newscategoryBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($newscategoryBlock->getNewsCategoryNavigation($newscategory), 'user_navigation');

			$form = $this->createFormBuilder(array());
			$form->add('cancel', 'submit', array('label' => 'Annuler'));
			$form->add('confirm', 'submit', array('label' => 'Confirmer'));
			$form = $form->getForm();


			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');

				$form->handleRequest($request);
				if ($form->isValid()) {
					if ($form->get('cancel')->isClicked()) {
						$flash->info('Suppression annulée');

						return $this->redirect($route->getChild('*/subject/show', array('newscategory' => $newscategory->getId()))->getRoutePath());
					}
					if ($form->get('confirm')->isClicked()) {

                        $em = $this->getDoctrine()->getManager();
                        $em->remove($newscategory);
                        $em->flush();

						$flash->success('La suppression à été réalisée avec succès.');

						return $this->redirect($route->getChild('*/index')->getRoutePath());
					}

				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formBlock = new Form($form->createView());
			$formBlock->setTemplate("NewsBundle:Block:Form/delete_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Suppression de la catégorie ".$newscategory->getLabel())
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('NewsBundle:Admin:newsCategory/delete.html.twig');

		}




		public function indexGridAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$gridHelper = $this->get("uneak.routesmanager.grid.helper");
			$menuHelper = $this->get("uneak.routesmanager.menu.helper");

			$params = $request->query->all();

			$gridData = $gridHelper->gridFields($gridHelper->createGridQueryBuilder('NewsBundle\Entity\NewsCategory', $params), $params);
			$recordsTotal = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('NewsBundle\Entity\NewsCategory', $params));
			$recordsFiltered = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('NewsBundle\Entity\NewsCategory', $params));


			$rowMenuTemplate = "UneakFlatSkinBundle:Block:Menu/block_menu.html.twig";

			$data = array();

			foreach ($gridData as $object) {
				$row = array();
				foreach ($params['columns'] as $columns) {
					if ($columns['name'] && substr($columns['name'], 0, 1) != '_') {
						$value = $object[str_replace(".", "_", $columns['name'])];
						if ($value instanceof \DateTime) {
							$value = $value->format('d/m/Y H:m:s');
						}
						$row[$columns['data']] = $value;
					} else {
						$row[$columns['data']] = "";
					}
				}
				$row['DT_RowId'] = $object['DT_RowId'];


				$menu = new Menu();
				$menu->setTemplate($rowMenuTemplate);
				$rowActions = $route->getParent()->getNestedRoute()->getRowActions();
				$root = $menuHelper->createMenu($rowActions, $route, array('newscategory' => $row['DT_RowId']));
				$root->setChildrenAttribute('class', 'nav nav-pills nav-inline text-nowrap');
				$menu->setRoot($root);

				$blockManager->addBlock($menu, 'rowMenu');
				$row['_actions'] = $this->renderView("{{ renderBlock('rowMenu') }}");

				array_push($data, $row);
			}

			return new JsonResponse(array(
				'draw'            => $params["draw"],
				'recordsTotal'    => $recordsTotal,
				'recordsFiltered' => $recordsFiltered,
				'data'            => $data,
			));
		}


	}
