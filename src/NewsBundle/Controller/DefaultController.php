<?php

	namespace NewsBundle\Controller;

	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;

	class DefaultController extends Controller {


		/**
		 * @Route("/ckeditor/plugins/news/icons/icon.png", name="ckeditor_news_icon")
		 */
		public function iconAction(Request $request) {
			$image = "icon.png";
			$filePath = $request->getScheme() . '://' . $request->getHttpHost() . '/bundles/news/ckeditor/plugins/news/icons/news.png';
			$file = readfile($filePath);
			$headers = array(
				'Content-Type'        => 'image/png',
				'Content-Disposition' => 'inline; filename="' . $image . '"'
			);
			return new Response($file, 200, $headers);
		}

		/**
		 * @Route("/ckeditor/plugins/news/plugin.js", name="ckeditor_news_plugin")
		 * @Template("NewsBundle:CKEditor/plugins/news:plugin.js.twig")
		 */
		public function pluginAction() {
			return array('name' => 'marc');
		}

		/**
		 * @Route("/ckeditor/plugins/news/dialogs/news.js", name="ckeditor_news_dialogs")
		 * @Template("NewsBundle:CKEditor/plugins/news:dialogs/news.js.twig")
		 */
		public function dialogsAction() {
            $newsRepo = $this->getDoctrine()->getRepository('NewsBundle:News');
            $news = $newsRepo->findNews();
			return array('items' => $news);
		}


        /**
         * @Route("/ckeditor/plugins/news/data/news.json", name="ckeditor_news_data_news")
         */
        public function dataNewsAction(Request $request) {
            $newsRepo = $this->getDoctrine()->getRepository('NewsBundle:News');
            $news = $newsRepo->getNews($request->query->get("id"));
            $render = $this->renderView('NewsBundle:CKEditor/plugins/news/template:template.html.twig', array('item' => $news));
            return new Response($render);
        }

	}
