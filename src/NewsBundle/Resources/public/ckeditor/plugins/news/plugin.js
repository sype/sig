CKEDITOR.plugins.add('news', {
    requires: 'widget',
    icons: 'news',

    init: function( editor ) {
        editor.widgets.add( 'news', {
            button: 'Ajouter une news',
            dialog: 'news',
            template:
            '<div class="simplebox">' +
            '<h2 class="simplebox-title">Title</h2>' +
            '<div class="simplebox-content"><p>Content...</p></div>' +
            '</div>',
            editables: {
                title: {
                    selector: '.simplebox-title'
                },
                content: {
                    selector: '.simplebox-content'
                }
            },

            init: function() {
                var width = this.element.getStyle( 'width' );
                if ( width )
                    this.setData( 'width', width );
                if ( this.element.hasClass( 'align-left' ) )
                    this.setData( 'align', 'left' );
                if ( this.element.hasClass( 'align-right' ) )
                    this.setData( 'align', 'right' );
                if ( this.element.hasClass( 'align-center' ) )
                    this.setData( 'align', 'center' );
            }

        } );

        CKEDITOR.dialog.add( 'news', this.path + 'dialogs/news.js' );

    }
} );