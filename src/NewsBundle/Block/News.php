<?php

	namespace NewsBundle\Block;


	use Doctrine\ORM\EntityManager;
	use NewsBundle\Entity\News as NewsEntity;
	use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
	use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
    use Uneak\FlatSkinBundle\Block\Info\Info;
	use Uneak\FlatSkinBundle\Block\Info\Infos;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\ProfileNav;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

	class News {

		private $blockManager;
		private $routeManager;
		private $menuHelper;
		private $vichHelper;
		private $autorization;
		private $em;

		public function __construct(Block $blockManager, FlattenRouteManager $routeManager, MenuHelper $menuHelper, UploaderHelper $vichHelper, AuthorizationChecker $autorization, EntityManager $em) {
			$this->blockManager = $blockManager;
			$this->routeManager = $routeManager;
			$this->menuHelper = $menuHelper;
			$this->vichHelper = $vichHelper;
			$this->autorization = $autorization;
			$this->em = $em;
		}


		public function getInfos(NewsEntity $news) {


			$tags = array();
			foreach ($news->getTags() as $tag) {
				$tags[] = "<i class='fa fa-icon fa-tag'> ".$tag->getLabel()."</i>";
			}


			$infos = new Infos();
			$infos
				->setColumns(1)
				->setStripeRow(true)
                ->addBlock(new Info("Date", $news->getDate()->format('d/m/Y H:i')), null, 90)
                ->addBlock(new Info("Titre", $news->getLabel()), null, 80)
                ->addBlock(new Info("Slug", $news->getSlug()), null, 70)
                ->addBlock(new Info("Article", $news->getDescription()), null, 60)
                ->addBlock(new Info("Tags", join(", ", $tags)), null, 50)
                ->addBlock(new Info("Publication", $news->getPublished()->format('d/m/Y H:i')), null, 40)
                ->addBlock(new Info("Activé", ($news->getActive()) ? "Oui" : "Non"), null, 0);

			$wrapper = new Wrapper();
			$wrapper
				->setTitle("Informations")
				->addBlock($infos);

			return $wrapper;
		}



		public function getNavigation() {

			$flattenCrud = $this->routeManager->getFlattenRoute('news');

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($flattenCrud->getMetaData("_label"))
				->setDescription($flattenCrud->getMetaData("_description"))
				->setColor("green")
				->setHeaderAlign('vertical');
			$profileNav->addBlock($this->getNavigationMenu());

			return $profileNav;

		}

		public function getNavigationMenu() {

			$flattenCrud = $this->routeManager->getFlattenRoute('news');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud);
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}

		public function getNewsNavigation(NewsEntity $news) {

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($news->getLabel())
				->setColor("green")
				->setHeaderAlign('horizontal')
				->setPhoto($this->vichHelper->asset($news, 'imageFile'));
			$profileNav->addBlock($this->getNewsNavigationMenu($news));

			return $profileNav;

		}

		public function getNewsNavigationMenu(NewsEntity $news) {

			$flattenCrud = $this->routeManager->getFlattenRoute('news/subject');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('news' => $news->getId()));
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}



	}
