<?php

	namespace NewsBundle\Entity;

	use Doctrine\ORM\Mapping as ORM;
	use Gedmo\Mapping\Annotation as Gedmo;


	/**
	 * Tag
	 *
	 * @ORM\Table("Tag")
	 * @ORM\Entity(repositoryClass="NewsBundle\Entity\TagRepository")
	 */
	class Tag {

		/**
		 * @var integer
		 *
		 * @ORM\Column(name="id", type="integer")
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="AUTO")
		 */
		protected $id;

		/**
		 * @var datetime $created
		 *
		 * @Gedmo\Timestampable(on="create")
		 * @ORM\Column(type="datetime")
		 */
		protected $created;

		/**
		 * @var datetime $updated
		 *
		 * @Gedmo\Timestampable(on="update")
		 * @ORM\Column(type="datetime")
		 */
		protected $updated;

		/**
		 * @ORM\Column(name="label", type="string", length=64)
		 */
		protected $label;

        /**
         * @ORM\ManyToMany(targetEntity="\NewsBundle\Entity\News", mappedBy="tags")
         **/
        protected $news;

        public function __construct() {
            $this->news = new \Doctrine\Common\Collections\ArrayCollection();
        }

		/**
		 * @ORM\Column(name="active", type="boolean")
		 */
		protected $active = true;


		/**
		 * Get id
		 *
		 * @return integer
		 */
		public function getId() {
			return $this->id;
		}


		/**
		 * Set created
		 *
		 * @param \DateTime $created
		 *
		 * @return Publish
		 */
		public function setCreated($created) {
			$this->created = $created;

			return $this;
		}

		/**
		 * Get created
		 *
		 * @return \DateTime
		 */
		public function getCreated() {
			return $this->created;
		}

		/**
		 * Set updated
		 *
		 * @param \DateTime $updated
		 *
		 * @return Publish
		 */
		public function setUpdated($updated) {
			$this->updated = $updated;

			return $this;
		}

		/**
		 * Get updated
		 *
		 * @return \DateTime
		 */
		public function getUpdated() {
			return $this->updated;
		}

		/**
		 * Set active
		 *
		 * @param boolean $active
		 *
		 * @return Publish
		 */
		public function setActive($active) {
			$this->active = $active;

			return $this;
		}

		/**
		 * Get active
		 *
		 * @return boolean
		 */
		public function getActive() {
			return $this->active;
		}

		/**
		 * Set label
		 *
		 * @param string $label
		 *
		 * @return MenuItem
		 */
		public function setLabel($label) {
			$this->label = $label;

			return $this;
		}

		/**
		 * Get label
		 *
		 * @return string
		 */
		public function getLabel() {
			return $this->label;
		}



        /**
         * Add news
         *
         * @param \NewsBundle\Entity\News $news
         *
         * @return News
         */
        public function addNews(\NewsBundle\Entity\News $news) {
            if (!$this->news->contains($news)) {
                $this->news->add($news);
            }

            return $this;
        }

        /**
         * Remove news
         *
         * @param \NewsBundle\Entity\News $news
         */
        public function removeNews(\NewsBundle\Entity\News $news) {
            if (!$this->news->contains($news)) {
                $this->news->removeElement($news);
            }
        }

        /**
         * Get news
         *
         * @return \Doctrine\Common\Collections\Collection
         */
        public function getNews() {
            return $this->news;
        }

        /**
         * Set news
         *
         * @param \Doctrine\Common\Collections\ArrayCollection
         *
         * @return Tag
         */
        public function setNews(\Doctrine\Common\Collections\ArrayCollection $news) {
            $this->$news = $news;

            return $this;
        }
        
        

		public function __toString() {
			return $this->label;
		}


	}
