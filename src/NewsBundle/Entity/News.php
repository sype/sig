<?php

	namespace NewsBundle\Entity;

	use Doctrine\ORM\Mapping as ORM;
	use Gedmo\Mapping\Annotation as Gedmo;

	use Symfony\Component\HttpFoundation\File\File;
	use Vich\UploaderBundle\Mapping\Annotation as Vich;
	use Symfony\Component\Validator\Constraints as Assert;


	/**
	 * News
	 *
	 * @ORM\Table()
	 * @ORM\Entity(repositoryClass="NewsBundle\Entity\NewsRepository")
	 * @Vich\Uploadable
	 */
	class News {

		/**
		 * @var integer
		 *
		 * @ORM\Column(name="id", type="integer")
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="AUTO")
		 */
		protected $id;

		/**
		 * @var datetime $created
		 *
		 * @Gedmo\Timestampable(on="create")
		 * @ORM\Column(type="datetime")
		 */
		protected $created;

		/**
		 * @var datetime $updated
		 *
		 * @Gedmo\Timestampable(on="update")
		 * @ORM\Column(type="datetime")
		 */
		protected $updated;

        /**
         * @var datetime $published
         *
         * @ORM\Column(type="datetime")
         * @Assert\NotBlank()
         */
        protected $published;

        /**
         * @var datetime $date
         *
         * @ORM\Column(type="datetime")
         * @Assert\NotBlank()
         */
        protected $date;


		/**
		 * @ORM\ManyToOne(targetEntity="\NewsBundle\Entity\NewsCategory", cascade={"persist"})
		 * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
		 * @Assert\NotNull()
		 */
		protected $category;


		/**
		 * @ORM\Column(name="label", type="string", length=64, nullable=false)
         * @Assert\NotBlank()
		 */
		protected $label;

        /**
         * @ORM\Column(name="slug", type="string", length=64, unique=false, nullable=true)
         * @Gedmo\Slug(fields={"label"}, updatable=false, unique=true)
         *
         */
        protected $slug;

		/**
		 * @ORM\Column(name="shortDescription", type="text", nullable=false)
		 * @Assert\NotBlank()
		 */
		protected $shortDescription;

		/**
		 * @ORM\Column(name="description", type="text", nullable=false)
         * @Assert\NotBlank()
		 */
		protected $description;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="image", type="string", length=255, nullable=true)
		 */
		protected $image;

		/**
		 * @var File $imageFile
		 * @Vich\UploadableField(mapping="news_image", fileNameProperty="image")
		 */
		protected $imageFile;

		/**
		 *
		 * @ORM\OneToMany(targetEntity="\NewsBundle\Entity\NewsImage", mappedBy="news", cascade={"persist", "remove"})
		 */
		protected $images;

        /**
         * @ORM\ManyToMany(targetEntity="\NewsBundle\Entity\Tag")
         * @ORM\JoinTable(name="NewsTag",
         *      joinColumns={@ORM\JoinColumn(name="news_id", referencedColumnName="id")},
         *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
         *      )
         **/
        protected $tags;

		/**
		 * @ORM\Column(name="active", type="boolean")
		 */
		protected $active = true;


		/**
		 * Constructor
		 */
		public function __construct() {
			$this->images = new \Doctrine\Common\Collections\ArrayCollection();
			$this->tags = new \Doctrine\Common\Collections\ArrayCollection();
		}


		/**
		 * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
		 * of 'UploadedFile' is injected into this setter to trigger the  update. If this
		 * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
		 * must be able to accept an instance of 'File' as the bundle will inject one here
		 * during Doctrine hydration.
		 *
		 * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
		 */
		public function setImageFile(File $image) {
			$this->imageFile = $image;

			if ($image) {
				// It is required that at least one field changes if you are using doctrine
				// otherwise the event listeners won't be called and the file is lost
				$this->updatedAt = new \DateTime('now');
			}
		}

		/**
		 * @return File
		 */
		public function getImageFile() {
			return $this->imageFile;
		}

		/**
		 * Set image path
		 *
		 * @param string $image
		 *
		 * @return Modele
		 */
		public function setImage($image = null) {
			$this->image = $image;

			return $this;
		}

		/**
		 * Get image path
		 *
		 * @return string $image
		 */
		public function getImage() {
			return $this->image;
		}


		/**
		 * Add images
		 *
		 * @param \NewsBundle\Entity\NewsImage $image
		 *
		 * @return Gallery
		 */
		public function addImage(\NewsBundle\Entity\NewsImage $image) {
			$image->setNews($this);
			$this->images[] = $image;

			return $this;
		}

		/**
		 * Remove images
		 *
		 * @param \NewsBundle\Entity\NewsImage $image
		 */
		public function removeImage(\NewsBundle\Entity\NewsImage $image) {
			$image->setNews(null);
			$this->images->removeElement($image);
		}

		/**
		 * Get images
		 *
		 * @return \Doctrine\Common\Collections\Collection
		 */
		public function getImages() {
			return $this->images;
		}

		/**
		 * Set images
		 *
		 * @param \Doctrine\Common\Collections\ArrayCollection
		 *
		 * @return Gallery
		 */
		public function setImages(\Doctrine\Common\Collections\ArrayCollection $images) {
			foreach ($images as $image) {
				$image->setNews($this);
			}
			$this->$images = $images;

			return $this;
		}


		/**
		 * Get id
		 *
		 * @return integer
		 */
		public function getId() {
			return $this->id;
		}


		/**
		 * Set created
		 *
		 * @param \DateTime $created
		 *
		 * @return Publish
		 */
		public function setCreated($created) {
			$this->created = $created;

			return $this;
		}

		/**
		 * Get created
		 *
		 * @return \DateTime
		 */
		public function getCreated() {
			return $this->created;
		}

		/**
		 * Set updated
		 *
		 * @param \DateTime $updated
		 *
		 * @return Publish
		 */
		public function setUpdated($updated) {
			$this->updated = $updated;

			return $this;
		}

		/**
		 * Get updated
		 *
		 * @return \DateTime
		 */
		public function getUpdated() {
			return $this->updated;
		}

		/**
		 * Set active
		 *
		 * @param boolean $active
		 *
		 * @return Publish
		 */
		public function setActive($active) {
			$this->active = $active;

			return $this;
		}

		/**
		 * Get active
		 *
		 * @return boolean
		 */
		public function getActive() {
			return $this->active;
		}

		/**
		 * Set label
		 *
		 * @param string $label
		 *
		 * @return MenuItem
		 */
		public function setLabel($label) {
			$this->label = $label;

			return $this;
		}

		/**
		 * Get label
		 *
		 * @return string
		 */
		public function getLabel() {
			return $this->label;
		}

		/**
		 * @return mixed
		 */
		public function getDescription() {
			return $this->description;
		}

		/**
		 * @param mixed $description
		 */
		public function setDescription($description) {
			$this->description = $description;
		}


        /**
         * Set published
         *
         * @param \DateTime $published
         *
         * @return News
         */
        public function setPublished($published) {
            $this->published = $published;

            return $this;
        }

        /**
         * Get published
         *
         * @return \DateTime
         */
        public function getPublished() {
            return $this->published;
        }


        /**
         * Set date
         *
         * @param \DateTime $date
         *
         * @return News
         */
        public function setDate($date) {
            $this->date = $date;

            return $this;
        }

        /**
         * Get date
         *
         * @return \DateTime
         */
        public function getDate() {
            return $this->date;
        }




        /**
         * Add tags
         *
         * @param \NewsBundle\Entity\Tag $tag
         *
         * @return News
         */
        public function addTag(\NewsBundle\Entity\Tag $tag) {
            $tag->addNews($this);
            $this->tags[] = $tag;

            return $this;
        }

        /**
         * Remove tags
         *
         * @param \NewsBundle\Entity\Tag $tag
         */
        public function removeTag(\NewsBundle\Entity\Tag $tag) {
            $tag->addNews(null);
            $this->tags->removeElement($tag);
        }

        /**
         * Get tags
         *
         * @return \Doctrine\Common\Collections\Collection
         */
        public function getTags() {
            return $this->tags;
        }

        /**
         * Set tags
         *
         * @param \Doctrine\Common\Collections\ArrayCollection
         *
         * @return Gallery
         */
        public function setTags(\Doctrine\Common\Collections\ArrayCollection $tags) {
            foreach ($tags as $tag) {
                $tag->addNews($this);
            }
            $this->$tags = $tags;

            return $this;
        }

		/**
		 * @return mixed
		 */
		public function getCategory() {
			return $this->category;
		}

		/**
		 * @param mixed $category
		 */
		public function setCategory($category) {
			$this->category = $category;
		}



        public function __toString() {
			return $this->label;
		}

        /**
         * @return mixed
         */
        public function getSlug()
        {
            return $this->slug;
        }

        /**
         * @param mixed $slug
         */
        public function setSlug($slug)
        {
            $this->slug = $slug;
        }

		/**
		 * @return mixed
		 */
		public function getShortDescription() {
			return $this->shortDescription;
		}

		/**
		 * @param mixed $shortDescription
		 */
		public function setShortDescription($shortDescription) {
			$this->shortDescription = $shortDescription;
		}


	}
