<?php

namespace NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * NewsImage
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="NewsBundle\Entity\NewsImageRepository")
 * @Vich\Uploadable
 */
class NewsImage {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var datetime $created
	 *
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var datetime $updated
	 *
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(type="datetime")
	 */
	protected $updated;

	/**
	 * @ORM\Column(name="label", type="string", length=128, nullable=true)
	 */
	protected $label;

	/**
	 * @ORM\Column(name="active", type="boolean")
	 */
	protected $active = true;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    protected $image;

    /**
     * @var File $imageFile
     * @Vich\UploadableField(mapping="news_image_image", fileNameProperty="image")
     */
    protected $imageFile;

	/**
	 * @ORM\ManyToOne(targetEntity="NewsBundle\Entity\News", inversedBy="images", cascade={"persist"})
	 * @ORM\JoinColumn(name="news_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
	 */
	protected $news;




	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set created
	 *
	 * @param \DateTime $created
	 * @return Publish
	 */
	public function setCreated($created) {
		$this->created = $created;

		return $this;
	}

	/**
	 * Get created
	 *
	 * @return \DateTime
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Set updated
	 *
	 * @param \DateTime $updated
	 * @return Publish
	 */
	public function setUpdated($updated) {
		$this->updated = $updated;

		return $this;
	}

	/**
	 * Get updated
	 *
	 * @return \DateTime
	 */
	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * Set active
	 *
	 * @param boolean $active
	 * @return Publish
	 */
	public function setActive($active) {
		$this->active = $active;

		return $this;
	}

	/**
	 * Get active
	 *
	 * @return boolean
	 */
	public function getActive() {
		return $this->active;
	}




    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image) {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile() {
        return $this->imageFile;
    }

    /**
     * Set image path
     *
     * @param string $image
     *
     * @return Modele
     */
    public function setImage($image = null) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image path
     *
     * @return string $image
     */
    public function getImage() {
        return $this->image;
    }



	/**
	 * Set label
	 *
	 * @param string $label
	 * @return NewsImage
	 */
	public function setLabel($label) {
		$this->label = $label;
		return $this;
	}

	/**
	 * Get label
	 *
	 * @return string
	 */
	public function getLabel() {
		return $this->label;
	}


	/**
	 * Set News
	 *
	 * @param \NewsBundle\Entity\News $news
	 * @return NewsImage
	 */
	public function setNews(\NewsBundle\Entity\News $news = null) {
		$this->news = $news;
		return $this;
	}

	/**
	 * Get News
	 *
	 * @return \NewsBundle\Entity\News
	 */
	public function getNews() {
		return $this->news;
	}


	public function __toString() {
		return $this->label;
	}



}
