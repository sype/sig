<?php

	namespace ProjetBundle\Block;


	use Doctrine\ORM\EntityManager;
	use ProjetBundle\Entity\Projet as ProjetEntity;
	use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
	use Uneak\GoogleMapBundle\Block\GoogleMap;
	use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
    use Uneak\FlatSkinBundle\Block\Info\Info;
	use Uneak\FlatSkinBundle\Block\Info\Infos;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
    use Uneak\FlatSkinBundle\Block\Panel\PanelPeople;
    use Uneak\FlatSkinBundle\Block\Panel\PanelPeoples;
    use Uneak\FlatSkinBundle\Block\Panel\ProfileNav;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

	class Projet {

		private $blockManager;
		private $routeManager;
		private $menuHelper;
		private $vichHelper;
		private $autorization;
		private $em;

		public function __construct(Block $blockManager, FlattenRouteManager $routeManager, MenuHelper $menuHelper, UploaderHelper $vichHelper, AuthorizationChecker $autorization, EntityManager $em) {
			$this->blockManager = $blockManager;
			$this->routeManager = $routeManager;
			$this->menuHelper = $menuHelper;
			$this->vichHelper = $vichHelper;
			$this->autorization = $autorization;
			$this->em = $em;
		}


		public function getInfos(ProjetEntity $projet) {


			$infos = new Infos();
			$infos
				->setColumns(1)
				->setStripeRow(true)
                ->addBlock(new Info("Nom", $projet->getLabel()), null, 90)
                ->addBlock(new Info("Slug", $projet->getSlug()), null, 80)
                ->addBlock(new Info("Année", $projet->getYear()), null, 60)
                ->addBlock(new Info("Position", $projet->getSection()->getCommune()->getDepartement()->getLabel()." / ".$projet->getSection()->getCommune()->getLabel()." / ".$projet->getSection()->getLabel()), null, 50)
				->addBlock(new GoogleMap("Localisation", $projet->getLocation(), $projet->getSection()->getCommune()->getDepartement()->getLabel().", ".$projet->getSection()->getCommune()->getLabel().", ".$projet->getSection()->getLabel()), null, 40)
				->addBlock(new Info("Agence", $projet->getAgence()->getLabel()), null, 30)
				->addBlock(new Info("Activé", ($projet->getActive()) ? "Oui" : "Non"), null, 0);

			$wrapper = new Wrapper();
			$wrapper
				->setTitle("Informations")
				->addBlock($infos);

			return $wrapper;
		}

        public function getLot(ProjetEntity $projet) {

            $projetRepo = $this->em->getRepository('ProjetBundle:Projet');
            $lots = $projetRepo->findProjetLots(false, $projet);

            $panelPeoples = new PanelPeoples();
            $panelPeoples->setTitle("<i class='fa fa-icon fa-bed'> Lots</i>");
            foreach ($lots as $lot) {

                $flattenCrud = $this->routeManager->getFlattenRoute('projet/subject/lot/subject');
                $menu = new Menu();
                $menu->setTemplate("UneakFlatSkinBundle:Block:Menu/block_menu.html.twig");
                $menu->setMeta('style', 'social-links');
                $root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('projet' => $projet->getId(), 'lot' => $lot->getId()) );
                $menu->setRoot($root);


                $description = $lot->getDescription()."<br/>";
                $description .= "<b>Nombre de lot :</b>".$lot->getCount()."<br/>";

                $panelPeople = new PanelPeople();
                $panelPeople->setTitle($lot->getLot()->getLabel());
                $panelPeople->setSubtitle($lot->getLot()->getType()->getLabel());
                $panelPeople->setDescription($description);
                $panelPeople->setMenu($menu);

                $panelPeoples->addPanel($panelPeople);
            }

            return $panelPeoples;

        }

		public function getNavigation() {

			$flattenCrud = $this->routeManager->getFlattenRoute('projet');

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($flattenCrud->getMetaData("_label"))
				->setDescription($flattenCrud->getMetaData("_description"))
				->setColor("green")
				->setHeaderAlign('vertical');
			$profileNav->addBlock($this->getNavigationMenu());

			return $profileNav;

		}

		public function getNavigationMenu() {

			$flattenCrud = $this->routeManager->getFlattenRoute('projet');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud);
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}

		public function getProjetNavigation(ProjetEntity $projet) {

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($projet->getLabel())
				->setDescription($projet->getYear())
				->setColor("green")
                ->setPhoto($this->vichHelper->asset($projet, 'imageFile'))
				->setHeaderAlign('horizontal');
			$profileNav->addBlock($this->getProjetNavigationMenu($projet));

			return $profileNav;

		}

		public function getProjetNavigationMenu(ProjetEntity $projet) {

			$flattenCrud = $this->routeManager->getFlattenRoute('projet/subject');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('projet' => $projet->getId()));
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}



	}
