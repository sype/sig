<?php

namespace ProjetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;



/**
 * Projet
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ProjetBundle\Entity\ProjetRepository")
 * @Vich\Uploadable()
 *
 */
class Projet {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\Column(name="label", type="string", length=64, nullable=false)
     * @Assert\NotBlank()
	 */
	protected $label;

    /**
     * @ORM\Column(name="slug", type="string", length=64, unique=false, nullable=true)
     * @Gedmo\Slug(fields={"label"}, updatable=false, unique=true)
     *
     */
    protected $slug;

    /**
     * @ORM\Column(name="year", type="string", length=4, nullable=true)
     */
    protected $year;

    /**
     * @ORM\Column(name="location", type="json_array", nullable=true)
     */
    protected $location;


    /**
     * @ORM\ManyToOne(targetEntity="\LocalisationBundle\Entity\Sections", cascade={"persist"})
     * @ORM\JoinColumn(name="section_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    protected $section;


    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    protected $image;

    /**
     * @var File $imageFile
     * @Vich\UploadableField(mapping="projet_image", fileNameProperty="image")
     */
    protected $imageFile;


    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
	 * @var datetime $created
	 *
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var datetime $updated
	 *
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(type="datetime")
	 */
	protected $updated;

	/**
	 * @ORM\Column(name="active", type="boolean")
	 */
	protected $active = true;


    /**
     * @ORM\ManyToOne(targetEntity="\AgenceBundle\Entity\Agence", inversedBy="projets", cascade={"persist"})
     * @ORM\JoinColumn(name="agence_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     * @Assert\NotNull()
     */
    protected $agence;



	/**
	 * @ORM\OneToMany(targetEntity="\ProjetBundle\Entity\ProjetImage", mappedBy="projet", cascade={"persist", "remove"})
	 */
	protected $images;


    /**
     * @ORM\OneToMany(targetEntity="\ProjetBundle\Entity\ProjetLot", mappedBy="projet", cascade={"persist", "remove"})
     */
    protected $lots;


	/**
	 * Constructor
	 */
	public function __construct() {
		$this->images = new \Doctrine\Common\Collections\ArrayCollection();
		$this->lots = new \Doctrine\Common\Collections\ArrayCollection();
	}


	/**
	 * Add images
	 *
	 * @param \ProjetBundle\Entity\ProjetImage $image
	 *
	 * @return Projet
	 */
	public function addImage(\ProjetBundle\Entity\ProjetImage $image) {
		$image->setProjet($this);
		$this->images[] = $image;
		return $this;
	}

	/**
	 * Remove images
	 *
	 * @param \ProjetBundle\Entity\ProjetImage $image
	 */
	public function removeImage(\ProjetBundle\Entity\ProjetImage $image) {
		$image->setProjet(null);
		$this->images->removeElement($image);
	}

	/**
	 * Get images
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getImages() {
		return $this->images;
	}

	/**
	 * Set images
	 * @param \Doctrine\Common\Collections\ArrayCollection
	 *
	 * @return Projet
	 */
	public function setImages(\Doctrine\Common\Collections\ArrayCollection $images) {
		foreach ($images as $image) {
			$image->setProjet($this);
		}
		$this->$images = $images;
		return $this;
	}



    /**
     * Add lots
     *
     * @param \ProjetBundle\Entity\ProjetLot $lot
     *
     * @return Projet
     */
    public function addLot(\ProjetBundle\Entity\ProjetLot $lot) {
        $lot->setProjet($this);
        $this->lots[] = $lot;
        return $this;
    }

    /**
     * Remove lots
     *
     * @param \ProjetBundle\Entity\ProjetLot $lot
     */
    public function removeLot(\ProjetBundle\Entity\ProjetLot $lot) {
        $lot->setProjet(null);
        $this->lots->removeElement($lot);
    }

    /**
     * Get lots
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLots() {
        return $this->lots;
    }

    /**
     * Set lots
     * @param \Doctrine\Common\Collections\ArrayCollection
     *
     * @return Projet
     */
    public function setLots(\Doctrine\Common\Collections\ArrayCollection $lots) {
        foreach ($lots as $lot) {
            $lot->setProjet($this);
        }
        $this->$lots = $lots;
        return $this;
    }


    /**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set created
	 *
	 * @param \DateTime $created
	 * @return Publish
	 */
	public function setCreated($created) {
		$this->created = $created;

		return $this;
	}

	/**
	 * Get created
	 *
	 * @return \DateTime
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Set updated
	 *
	 * @param \DateTime $updated
	 * @return Publish
	 */
	public function setUpdated($updated) {
		$this->updated = $updated;

		return $this;
	}

	/**
	 * Get updated
	 *
	 * @return \DateTime
	 */
	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * Set active
	 *
	 * @param boolean $active
	 * @return Publish
	 */
	public function setActive($active) {
		$this->active = $active;

		return $this;
	}

	/**
	 * Get active
	 *
	 * @return boolean
	 */
	public function getActive() {
		return $this->active;
	}

	/**
	 * Set label
	 *
	 * @param string $label
	 * @return MenuItem
	 */
	public function setLabel($label) {
		$this->label = $label;

		return $this;
	}

	/**
	 * Get label
	 *
	 * @return string
	 */
	public function getLabel() {
		return $this->label;
	}

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * @param mixed $newPassword
     */
    public function setLocation($location) {
        $this->location = $location;
    }


    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image) {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile() {
        return $this->imageFile;
    }

    /**
     * Set image path
     *
     * @param string $image
     *
     * @return Modele
     */
    public function setImage($image = null) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image path
     *
     * @return string $image
     */
    public function getImage() {
        return $this->image;
    }



    /**
     * Set agence
     *
     * @param \AgenceBundle\Entity\Agence $agence
     * @return Projet
     */
    public function setAgence(\AgenceBundle\Entity\Agence $agence = null) {
        $this->agence = $agence;
        return $this;
    }

    /**
     * Get agence
     *
     * @return \AgenceBundle\Entity\Agence
     */
    public function getAgence() {
        return $this->agence;
    }

	/**
	 * @return mixed
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param mixed $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}


    /**
     * Set section
     *
     * @param \LocalisationBundle\Entity\Sections $section
     *
     * @return Projet
     */
    public function setSection(\LocalisationBundle\Entity\Sections $section = null) {
        $this->section = $section;
        return $this;
    }

    /**
     * Get commune
     *
     * @return \LocalisationBundle\Entity\Sections
     */
    public function getSection() {
        return $this->section;
    }




    public function __toString() {
		return $this->label;
	}

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }


}
