<?php

namespace ProjetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProjetLot
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ProjetBundle\Entity\ProjetLotRepository")
 */
class ProjetLot {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="\ProjetBundle\Entity\Projet", cascade={"persist"})
     * @ORM\JoinColumn(name="projet_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    protected $projet;


    /**
     * @ORM\ManyToOne(targetEntity="\LotBundle\Entity\Lot", cascade={"persist"})
     * @ORM\JoinColumn(name="lot_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    protected $lot;


    /**
     * @var integer
     *
     * @ORM\Column(name="count", type="integer", nullable=true)
     */
    protected $count;

    /**
     * @var integer
     *
     * @ORM\Column(name="loyer", type="float", nullable=true)
     */
    protected $loyer;

    /**
     * @var integer
     *
     * @ORM\Column(name="surface", type="float", nullable=true)
     */
    protected $surface;


    /**
     * @ORM\Column(name="description", type="text")
     */
    protected $description;


	/**
	 * @var datetime $created
	 *
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var datetime $updated
	 *
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(type="datetime")
	 */
	protected $updated;

	/**
	 * @ORM\Column(name="active", type="boolean")
	 */
	protected $active = true;

    /**
     * @ORM\Column(name="amenage", type="boolean")
     */
    protected $amenage = false;

    /**
     * @ORM\Column(name="accessibilite", type="boolean")
     */
    protected $accessibilite = false;



	/**
	 *
	 * @ORM\OneToMany(targetEntity="\ProjetBundle\Entity\ProjetLotImage", mappedBy="projetLot", cascade={"persist", "remove"})
	 */
	protected $images;



	/**
	 * Constructor
	 */
	public function __construct() {
		$this->images = new \Doctrine\Common\Collections\ArrayCollection();
	}


	/**
	 * Add images
	 *
	 * @param \ProjetBundle\Entity\ProjetImage $image
	 *
	 * @return Gallery
	 */
	public function addImage(\ProjetBundle\Entity\ProjetLotImage $image) {
		$image->setProjetLot($this);
		$this->images[] = $image;
		return $this;
	}

	/**
	 * Remove images
	 *
	 * @param \ProjetBundle\Entity\ProjetLotImage $image
	 */
	public function removeImage(\ProjetBundle\Entity\ProjetLotImage $image) {
		$image->setProjetLot(null);
		$this->images->removeElement($image);
	}

	/**
	 * Get images
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getImages() {
		return $this->images;
	}

	/**
	 * Set images
	 * @param \Doctrine\Common\Collections\ArrayCollection
	 *
	 * @return Gallery
	 */
	public function setImages(\Doctrine\Common\Collections\ArrayCollection $images) {
		foreach ($images as $image) {
			$image->setProjetLot($this);
		}
		$this->$images = $images;
		return $this;
	}



	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set created
	 *
	 * @param \DateTime $created
	 * @return Publish
	 */
	public function setCreated($created) {
		$this->created = $created;

		return $this;
	}

	/**
	 * Get created
	 *
	 * @return \DateTime
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Set updated
	 *
	 * @param \DateTime $updated
	 * @return Publish
	 */
	public function setUpdated($updated) {
		$this->updated = $updated;

		return $this;
	}

	/**
	 * Get updated
	 *
	 * @return \DateTime
	 */
	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * Set active
	 *
	 * @param boolean $active
	 * @return Publish
	 */
	public function setActive($active) {
		$this->active = $active;

		return $this;
	}

	/**
	 * Get active
	 *
	 * @return boolean
	 */
	public function getActive() {
		return $this->active;
	}




	public function __toString() {
		return $this->lot->__toString();
	}


    /**
     * Set projet
     *
     * @param \ProjetBundle\Entity\Projet $projet
     *
     * @return ProjetLot
     */
    public function setProjet(\ProjetBundle\Entity\Projet $projet = null) {
        $this->projet = $projet;
        return $this;
    }

    /**
     * Get projet
     *
     * @return \ProjetBundle\Entity\Projet
     */
    public function getProjet() {
        return $this->projet;
    }



    /**
     * Set lot
     *
     * @param \LocalisationBundle\Entity\Lots $lot
     *
     * @return Contact
     */
    public function setLot(\LotBundle\Entity\Lot $lot = null) {
        $this->lot = $lot;
        return $this;
    }

    /**
     * Get lot
     *
     * @return \LotBundle\Entity\Lot
     */
    public function getLot() {
        return $this->lot;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getLoyer()
    {
        return $this->loyer;
    }

    /**
     * @param int $loyer
     */
    public function setLoyer($loyer)
    {
        $this->loyer = $loyer;
    }

    /**
     * @return int
     */
    public function getSurface()
    {
        return $this->surface;
    }

    /**
     * @param int $surface
     */
    public function setSurface($surface)
    {
        $this->surface = $surface;
    }

    /**
     * @return mixed
     */
    public function getAmenage()
    {
        return $this->amenage;
    }

    /**
     * @param mixed $amenage
     */
    public function setAmenage($amenage)
    {
        $this->amenage = $amenage;
    }

    /**
     * @return mixed
     */
    public function getAccessibilite()
    {
        return $this->accessibilite;
    }

    /**
     * @param mixed $accessibilite
     */
    public function setAccessibilite($accessibilite)
    {
        $this->accessibilite = $accessibilite;
    }



    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    

}
