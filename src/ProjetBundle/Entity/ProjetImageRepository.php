<?php

namespace ProjetBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ProjetImageRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProjetImageRepository extends EntityRepository {

	public function findImages($projet = null, $active = true) {
		$qb = $this->getImagesQuery($projet, $active);
		return $qb->getQuery()->getResult();
	}

	public function getImagesQuery($projet = null, $active = true) {
		$qb = $this->_em->createQueryBuilder();
		$qb->select(array('image'));
		$qb->from('ProjetBundle:ProjetImage', 'image');

		if ($active) {
			$qb->andWhere($qb->expr()->eq('image.active', ':active'));
			$qb->setParameter("active", 1);
		}

		if ($projet) {
			$qb->andWhere($qb->expr()->eq('image.projet', ':projet'));
			$qb->setParameter("projet", $projet);
		}

		return $qb;
	}
}
