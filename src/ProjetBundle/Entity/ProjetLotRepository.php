<?php

namespace ProjetBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * ProjetLotRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProjetLotRepository extends EntityRepository {


    public function search(array $data) {
        return null;

//        $qb = $this->getProjetsQuery(true, null);
//
//        if (isset($data['content']) && $data['content']) {
//            $qb->andWhere($qb->expr()->orX(
//                $qb->expr()->like('projet.label', ':content')),
//                $qb->expr()->like('projet.description', ':content')
//            );
//            $qb->setParameter('content', '%'.$data['content'].'%');
//        }
//
//        return $qb->getQuery()->getResult();
    }



    public function findProjetLotByType($active, $type) {
        $qb = $this->_em->createQueryBuilder();
        $qb->select(array('projet'));
        $qb->from('ProjetBundle:Projet', 'projet');
        $qb->innerjoin('projet.lots', 'projet_lot');
        $qb->innerjoin('projet_lot.lot', 'lot');
        $qb->innerjoin('lot.type', 'lottype');

        $qb->andWhere($qb->expr()->eq('lot.type', ':type'));
        $qb->setParameter('type', $type);

        if ($active) {
            $qb->andWhere($qb->expr()->eq('projet.active', true));
            $qb->andWhere($qb->expr()->eq('lot.active', true));
            $qb->andWhere($qb->expr()->eq('lottype.active', true));
            $qb->andWhere($qb->expr()->eq('projet_lot.active', true));
        }

        return $qb->getQuery()->getResult();
    }
//
//
//    public function findProjetLotByType($active, $type) {
//        $qb = $this->_em->createQueryBuilder();
//        $qb->select(array('projet_lot'));
//        $qb->from('ProjetBundle:ProjetLot', 'projet_lot');
//        $qb->innerjoin('projet_lot.projet', 'projet');
//        $qb->innerjoin('projet_lot.lot', 'lot');
//        $qb->innerjoin('lot.type', 'lottype');
//
//        $qb->andWhere($qb->expr()->eq('lot.type', ':type'));
//        $qb->setParameter('type', $type);
//
//        if ($active) {
//            $qb->andWhere($qb->expr()->eq('projet.active', true));
//            $qb->andWhere($qb->expr()->eq('lot.active', true));
//            $qb->andWhere($qb->expr()->eq('lottype.active', true));
//            $qb->andWhere($qb->expr()->eq('projet_lot.active', true));
//        }
//
//        return $qb->getQuery()->getResult();
//    }


    public function findProjetLotBySlug($active, $slug, $lot) {
        $qb = $this->_em->createQueryBuilder();
        $qb->select(array('projet_lot'));
        $qb->from('ProjetBundle:ProjetLot', 'projet_lot');
        $qb->innerjoin('projet_lot.projet', 'projet');
        $qb->innerjoin('projet_lot.lot', 'lot');
        $qb->innerjoin('lot.type', 'lottype');


        $qb->andWhere($qb->expr()->eq('projet.slug', ':slug'));
        $qb->setParameter('slug', $slug);

        $qb->andWhere($qb->expr()->eq('lot.slug', ':lot'));
        $qb->setParameter('lot', $lot);


        if ($active) {
            $qb->andWhere($qb->expr()->eq('projet.active', true));
            $qb->andWhere($qb->expr()->eq('lot.active', true));
            $qb->andWhere($qb->expr()->eq('lottype.active', true));
            $qb->andWhere($qb->expr()->eq('projet_lot.active', true));
        }

        return $qb->getQuery()->getOneOrNullResult();
    }





}
