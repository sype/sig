<?php

	namespace ProjetBundle\Controller;

	use Doctrine\Common\Collections\ArrayCollection;
	use ProjetBundle\Entity\Projet;
    use ProjetBundle\Entity\ProjetLot;
    use ProjetBundle\Entity\Projets;
    use ProjetBundle\Entity\ProjetsRepository;
    use ProjetBundle\Form\ProjetLotType;
    use ProjetBundle\Form\ProjetType;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoute;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
	use Uneak\FlatSkinBundle\Block\Component\DataTable;
	use Uneak\FlatSkinBundle\Block\DataTable\DataTableFilters;
	use Uneak\FlatSkinBundle\Block\Form\Form;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Doctrine\ORM\Query\Expr;

	class AdminProjetController extends Controller {


		public function indexAction( FlattenRoute $route) {
			$blockManager = $this->get("uneak.blocksmanager");

			$projetBlock = $this->get('uneak.admin.projet.block.helper');
			$blockManager->addBlock($projetBlock->getNavigation(), 'navigation');

			$gridNested = $route->getNestedRoute();

			$datatable = new DataTable();
			$datatable->setAjax($route->getChild('_grid')->getRoutePath());
			$datatable->setColumns($gridNested->getColumns());

			$datatableWrapper = new Wrapper();
			$datatableWrapper
				->setTitle("Resultats")
				->setCollapsable(false)
				->addBlock($datatable);
			$blockManager->addBlock($datatableWrapper, 'datatable');

			$datatableFilterWrapper = new Wrapper();
			$datatableFilterWrapper
				->setTitle("Filtres")
				->setCollapsable(true)
				->addBlock(new DataTableFilters($datatable));
			$blockManager->addBlock($datatableFilterWrapper, 'datatable_filters');

			return $this->render('ProjetBundle:Admin:index.html.twig');

		}



		public function showAction($projet) {
			$blockManager = $this->get("uneak.blocksmanager");

			$projetBlock = $this->get('uneak.admin.projet.block.helper');

			$blockManager->addBlock($projetBlock->getInfos($projet), 'infos');
			$blockManager->addBlock($projetBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($projetBlock->getProjetNavigation($projet), 'user_navigation');
            $blockManager->addBlock($projetBlock->getLot($projet), 'panelPeople');

            $galleryBlock = $this->get('uneak.admin.gallery.block.helper');
            $blockManager->addBlock($galleryBlock->getImages($projet), 'imagesGallery');

			return $this->render('ProjetBundle:Admin:show.html.twig');

		}


		public function editAction($projet, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");


			$projetBlock = $this->get('uneak.admin.projet.block.helper');
			$blockManager->addBlock($projetBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($projetBlock->getProjetNavigation($projet), 'user_navigation');


			$em = $this->getDoctrine()->getManager();
			$images = $em->getRepository('ProjetBundle:ProjetImage')->findImages($projet, false);
			$originalImages = new ArrayCollection();
			foreach ($images as $image) {
				$originalImages->add($image);
			}



			$form = $this->createForm(new ProjetType(), $projet);
			$form->add('submit', 'submit', array('label' => 'Modifier'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

					foreach ($originalImages as $image) {
						if ($projet->getImages()->contains($image) == false) {
							$projet->getImages()->removeElement($image);
							$em->remove($image);
						}
					}

                    $em->flush();

					return $this->redirect($route->getChild('*/subject/show', array('projet' => $projet->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("ProjetBundle:Block:Form/block_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Edition du projet ".$projet->getLabel())
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('ProjetBundle:Admin:edit.html.twig');

		}


		public function newAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$projetBlock = $this->get('uneak.admin.projet.block.helper');
			$blockManager->addBlock($projetBlock->getNavigation(), 'navigation');

			$projet = new Projet();

			$form = $this->createForm(new ProjetType(), $projet);
			$form->add('submit', 'submit', array('label' => 'Creer'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($projet);
                    $em->flush();

					return $this->redirect($route->getChild('*/subject/show', array('projet' => $projet->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("ProjetBundle:Block:Form/block_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Création d'un projet")
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');

			return $this->render('ProjetBundle:Admin:new.html.twig');


		}


		public function deleteAction($projet, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$projetBlock = $this->get('uneak.admin.projet.block.helper');
			$blockManager->addBlock($projetBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($projetBlock->getProjetNavigation($projet), 'user_navigation');

			$form = $this->createFormBuilder(array());
			$form->add('cancel', 'submit', array('label' => 'Annuler'));
			$form->add('confirm', 'submit', array('label' => 'Confirmer'));
			$form = $form->getForm();


			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');

				$form->handleRequest($request);
				if ($form->isValid()) {
					if ($form->get('cancel')->isClicked()) {
						$flash->info('Suppression annulée');

						return $this->redirect($route->getChild('*/subject/show', array('projet' => $projet->getId()))->getRoutePath());
					}
					if ($form->get('confirm')->isClicked()) {

                        $em = $this->getDoctrine()->getManager();
                        $em->remove($projet);
                        $em->flush();

						$flash->success('La suppression à été réalisée avec succès.');

						return $this->redirect($route->getChild('*/index')->getRoutePath());
					}

				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formBlock = new Form($form->createView());
			$formBlock->setTemplate("ProjetBundle:Block:Form/delete_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Suppression du projet ".$projet->getLabel())
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('ProjetBundle:Admin:delete.html.twig');

		}


        public function lotEditAction($projet, $lot, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

            $projetBlock = $this->get('uneak.admin.projet.block.helper');
            $blockManager->addBlock($projetBlock->getNavigation(), 'navigation');
            $blockManager->addBlock($projetBlock->getProjetNavigation($projet), 'user_navigation');

			$em = $this->getDoctrine()->getManager();
			$images = $em->getRepository('ProjetBundle:ProjetLotImage')->findImages($lot, false);
			$originalImages = new ArrayCollection();
			foreach ($images as $image) {
				$originalImages->add($image);
			}


            $form = $this->createForm(new ProjetLotType($projet, $lot->getLot()), $lot);
            $form->add('submit', 'submit', array('label' => 'Modifier'));

            if ($request->getMethod() == 'POST') {

                $flash = $this->get('braincrafted_bootstrap.flash');
                $form->handleRequest($request);
                if ($form->isValid()) {

					foreach ($originalImages as $image) {
						if ($lot->getImages()->contains($image) == false) {
							$lot->getImages()->removeElement($image);
							$em->remove($image);
						}
					}

					$em->flush();

                    return $this->redirect($route->getChild('*/subject/show', array('projet' => $projet->getId()))->getRoutePath());
                } else {
                    $flash->error('Votre formulaire est invalide.');
                }
            }


            $formManager = $this->get("uneak.formsmanager");

            $formBlock = new Form($formManager->createView($form));
            $formBlock->setTemplate("ProjetBundle:Block:Form/block_lot_form.html.twig");

            $formWrapper = new Wrapper();
            $formWrapper
                ->setTitle("Edition du lot ".$lot->getLot()->getLabel())
                ->addBlock($formBlock);
            $blockManager->addBlock($formWrapper, 'form');

            return $this->render('ProjetBundle:Admin:lot/new.html.twig');


        }


        public function lotNewAction($projet, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

            $projetBlock = $this->get('uneak.admin.projet.block.helper');
            $blockManager->addBlock($projetBlock->getNavigation(), 'navigation');
            $blockManager->addBlock($projetBlock->getProjetNavigation($projet), 'user_navigation');

            $projetLot = new ProjetLot();
            $projetLot->setProjet($projet);

            $form = $this->createForm(new ProjetLotType($projet), $projetLot);
            $form->add('submit', 'submit', array('label' => 'Creer'));

            if ($request->getMethod() == 'POST') {

                $flash = $this->get('braincrafted_bootstrap.flash');
                $form->handleRequest($request);
                if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($projetLot);
                    $em->flush();

                    return $this->redirect($route->getChild('*/subject/show', array('projet' => $projet->getId()))->getRoutePath());
                } else {
                    $flash->error('Votre formulaire est invalide.');
                }
            }


            $formManager = $this->get("uneak.formsmanager");

            $formBlock = new Form($formManager->createView($form));
            $formBlock->setTemplate("ProjetBundle:Block:Form/block_lot_form.html.twig");

            $formWrapper = new Wrapper();
            $formWrapper
                ->setTitle("Création d'un lot pour le projet ".$projet->getLabel())
                ->addBlock($formBlock);
            $blockManager->addBlock($formWrapper, 'form');

            return $this->render('ProjetBundle:Admin:lot/new.html.twig');


        }


        public function lotDeleteAction($projet, $lot, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

            $projetBlock = $this->get('uneak.admin.projet.block.helper');
            $blockManager->addBlock($projetBlock->getNavigation(), 'navigation');
            $blockManager->addBlock($projetBlock->getProjetNavigation($projet), 'user_navigation');

            $form = $this->createFormBuilder(array());
            $form->add('cancel', 'submit', array('label' => 'Annuler'));
            $form->add('confirm', 'submit', array('label' => 'Confirmer'));
            $form = $form->getForm();


            if ($request->getMethod() == 'POST') {

                $flash = $this->get('braincrafted_bootstrap.flash');

                $form->handleRequest($request);
                if ($form->isValid()) {
                    if ($form->get('cancel')->isClicked()) {
                        $flash->info('Suppression annulée');
                    }
                    if ($form->get('confirm')->isClicked()) {

                        $em = $this->getDoctrine()->getManager();
                        $em->remove($lot);
                        $em->flush();

                        $flash->success('La suppression à été réalisée avec succès.');
                    }

                    return $this->redirect($route->getChild('*/subject/show', array('projet' => $projet->getId()))->getRoutePath());

                } else {
                    $flash->error('Votre formulaire est invalide.');
                }
            }


            $formBlock = new Form($form->createView());
            $formBlock->setTemplate("ProjetBundle:Block:Form/delete_form.html.twig");

            $formWrapper = new Wrapper();
            $formWrapper
                ->setTitle("Suppression du lot ".$lot->getLot()->getLabel()." pour le projet ".$projet->getLabel())
                ->addBlock($formBlock);
            $blockManager->addBlock($formWrapper, 'form');


            return $this->render('ProjetBundle:Admin:delete.html.twig');

        }



		public function indexGridAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$gridHelper = $this->get("uneak.routesmanager.grid.helper");
			$menuHelper = $this->get("uneak.routesmanager.menu.helper");

			$params = $request->query->all();

			$gridData = $gridHelper->gridFields($gridHelper->createGridQueryBuilder('ProjetBundle\Entity\Projet', $params), $params);
			$recordsTotal = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('ProjetBundle\Entity\Projet', $params));
			$recordsFiltered = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('ProjetBundle\Entity\Projet', $params));


			$rowMenuTemplate = "UneakFlatSkinBundle:Block:Menu/block_menu.html.twig";

			$data = array();

			foreach ($gridData as $object) {
				$row = array();
				foreach ($params['columns'] as $columns) {
					if ($columns['name'] && substr($columns['name'], 0, 1) != '_') {
						$value = $object[str_replace(".", "_", $columns['name'])];
						if ($value instanceof \DateTime) {
							$value = $value->format('d/m/Y H:m:s');
						}
						$row[$columns['data']] = $value;
					} else {
						$row[$columns['data']] = "";
					}
				}
				$row['DT_RowId'] = $object['DT_RowId'];


				$menu = new Menu();
				$menu->setTemplate($rowMenuTemplate);
				$rowActions = $route->getParent()->getNestedRoute()->getRowActions();
				$root = $menuHelper->createMenu($rowActions, $route, array('projet' => $row['DT_RowId']));
				$root->setChildrenAttribute('class', 'nav nav-pills nav-inline text-nowrap');
				$menu->setRoot($root);

				$blockManager->addBlock($menu, 'rowMenu');
				$row['_actions'] = $this->renderView("{{ renderBlock('rowMenu') }}");

				array_push($data, $row);
			}

			return new JsonResponse(array(
				'draw'            => $params["draw"],
				'recordsTotal'    => $recordsTotal,
				'recordsFiltered' => $recordsFiltered,
				'data'            => $data,
			));
		}


	}
