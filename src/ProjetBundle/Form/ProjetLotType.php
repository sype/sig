<?php

	namespace ProjetBundle\Form;

    use Doctrine\ORM\EntityRepository;
	use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;
	use Uneak\FormsManagerBundle\Forms\AssetsComponentType;

	class ProjetLotType extends AssetsComponentType {

        protected $projet;
        protected $lot;

        public function __construct($project, $lot = null) {
            $this->projet = $project;
            $this->lot = $lot;
        }

		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {

            $projet = $this->projet;
            $lot = $this->lot;

            $builder
                ->add('lot', 'entity_select2', array(
                    'label' => "Lot",
                    'class' => 'LotBundle:Lot',
                    'query_builder' => function (EntityRepository $er) use ($projet, $lot) {
                        $qb = $er->getAvailableLotsQuery($projet, $lot);
                        return $qb;
                    },
                    'options' => array(
                        'placeholder' => "Sectionnez le lot",
                        'language' => 'fr'
                    ),
                    'multiple'  => false,

                    'empty_value' => 'Sectionnez le lot',
                ))

				->add('description', 'ckeditor', array(
					'label' => "Description",
                    'options' => array(
                        'customConfig' => '../../../bundles/app/js/ckeditor_config.js',
                    ),
                    'required'      => true,
				))

                ->add('count', null, array(
                    'label' => "Nombre de lot",
                ))

                ->add('surface', null, array(
                    'label' => "Surface",
                ))

                ->add('loyer', null, array(
                    'label' => "Loyer",
                ))

                ->add('amenage', null, array(
                    'label' => "Aménagé",
                    'required'      => false,
                ))

                ->add('accessibilite', null, array(
                    'label' => "Accessible aux handicapés",
                    'required'      => false,
                ))

				->add('images', 'bootstrap_collection', array(
					'label'              => "Galerie d'images",
					'by_reference'			=> false,
					'type'               => new ProjetLotImageType(),
					'allow_add'          => true,
					'allow_delete'       => true,
					'add_button_text'    => "Ajouter une image",
					'delete_button_text' => "x",
					'sub_widget_col'     => 10,
					'button_col'         => 2
				))

				->add('active', null, array(
					'label' => "Activé",
					'required'      => false,
				));


        }





		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'ProjetBundle\Entity\ProjetLot',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'projetbundle_projetlot';
		}
	}
