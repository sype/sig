<?php

	namespace ProjetBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;

	class ProjetImageType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder

				->add('label', null, array(
					'label' => "Titre",
				))

                ->add('imageFile', 'vich_file', array(
                        'label'         => "Photo",
                        'allow_delete'  => false, // not mandatory, default is true
                        'download_link' => true, // not mandatory, default is true
                        'required'      => false,
                    )
                )

				->add('active', null, array(
					'label' => "Activé",
					'required'      => false
				));

		}



		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'ProjetBundle\Entity\ProjetImage',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'gallerybundle_projetimage';
		}
	}
