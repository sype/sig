<?php

	namespace ProjetBundle\Form;

	use Doctrine\ORM\EntityRepository;
    use LocalisationBundle\Entity\Communes;
    use LocalisationBundle\Entity\Departements;
    use ProjetBundle\Entity\Projet;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
    use Symfony\Component\Form\FormView;
    use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;
	use Uneak\AssetsManagerBundle\Assets\AssetBuilder;
	use Uneak\AssetsManagerBundle\Assets\Js\AssetInternalJs;
	use Uneak\FormsManagerBundle\Forms\AssetsComponentType;

	class ProjetType extends AssetsComponentType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('label', null, array(
					'label' => "Nom",
				))

                ->add('slug', null, array(
                    'label' => "Slug",
                ))

                ->add('description', 'ckeditor', array(
                    'label' => "Description",
                        'options' => array(
                            'customConfig' => '../../../bundles/app/js/ckeditor_config.js',
//                            'extraPlugins' => 'news'
                        )
                ))

                ->add('imageFile', 'vich_file', array(
                        'label'         => "Photo",
                        'required'      => false,
                        'allow_delete'  => true, // not mandatory, default is true
                        'download_link' => true, // not mandatory, default is true
                        'required'      => false,
                    )
                )


                ->add('year', null, array(
                    'label' => "Année",
                ))


                ->add('agence', 'entity_select2', array(
                    'label' => "Agence",
                    'class' => 'AgenceBundle:Agence',
                    'query_builder'   => function (EntityRepository $er) {
                        return $er->getAgencesQuery();
                    },
                    'options' => array(
                        'placeholder' => "Sectionnez l'agence",
                        'language' => 'fr'
                    ),
                    'multiple'  => false,

                    'empty_value' => "Sectionnez l'agence",
                ))



				->add('location', 'uneak_googlemap', array(
					'label' => "Position géographique",
					'search_fields' => array('departement', 'commune'),
					'bounds' => array(
						'editable' => false,
					)
				))

				->add('images', 'bootstrap_collection', array(
					'label'              => "Galerie d'images",
					'by_reference'			=> false,
					'type'               => new ProjetImageType(),
					'allow_add'          => true,
					'allow_delete'       => true,
					'add_button_text'    => "Ajouter une image",
					'delete_button_text' => "x",
					'sub_widget_col'     => 10,
					'button_col'         => 2
				))

				->add('active', null, array(
					'label' => "Activé",
					'required'      => false,
				));




            $factory = $builder->getFormFactory();


            $refreshSections = function ($form, $communes) use ($factory) {

                $form->add($factory->createNamed('section', 'entity_select2', null, array(
                    'class'           => 'LocalisationBundle:Sections',
                    'property'        => 'label',
                    'empty_value'     => 'Sélectionnez votre section',
                    'auto_initialize' => false,
                    'required'        => true,
                    'attr'            => array(
                        'input_group' => array(
                            'prepend' => '.icon-map-marker'
                        )
                    ),
                    'query_builder'   => function (EntityRepository $er) use ($communes) {
                        $qb = $er->getSectionsQuery();

                        if ($communes instanceof Communes) {
                            $qb->where('sections.commune = :commune')->setParameter('commune', $communes);
                        } elseif (is_numeric($communes)) {
                            $qb->where('commune.id = :commune')->setParameter('commune', $communes);
                        } else {
                            $qb->where('commune.id = :commune')->setParameter('commune', 0);
                        }

                        return $qb;
                    }
                )));
            };



            $setCommunes = function ($form, $commune, $departement) use ($factory) {

                $form->add($factory->createNamed('commune', 'entity_select2', null, array(
                    'class'           => 'LocalisationBundle:Communes',
                    'property'        => 'label',
                    'empty_value'     => 'Sélectionnez votre commune',
                    'auto_initialize' => false,
                    'required'        => true,
                    'mapped'          => false,
                    'data'            => $commune,
                    'attr'            => array(
                        'input_group' => array(
                            'prepend' => '.icon-map-marker'
                        )
                    ),
                    'query_builder'   => function (EntityRepository $er) use ($departement) {
                        $qb = $er->getCommunesQuery();

                        if ($departement instanceof Departements) {
                            $qb->where('communes.departement = :departement')->setParameter('departement', $departement);
                        } elseif (is_numeric($departement)) {
                            $qb->where('departement.id = :departement')->setParameter('departement', $departement);
                        } else {
                            $qb->where('departement.id = :departement')->setParameter('departement', 0);
                        }

                        return $qb;
                    }
                )));
            };

            $setDepartement = function ($form, $departement) use ($factory) {
                $form->add($factory->createNamed('departement', 'entity_select2', null, array(
                    'class'           => 'LocalisationBundle:Departements',
                    'property'        => 'label',
                    'query_builder'   => function (EntityRepository $er) {
                        return $er->getDepartementsQuery();
                    },
                    'empty_value'     => 'Sélectionnez votre département',
                    'empty_data'      => null,
                    'auto_initialize' => false,
                    'data'            => $departement,
                    'label'           => 'Département',
                    'required'        => true,
                    'mapped'          => false,
                    'attr'            => array(
                        'input_group' => array(
                            'prepend' => '.icon-globe'
                        )
                    )
                )));
            };


            $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($setCommunes, $setDepartement, $refreshSections) {
                $form = $event->getForm();
                $data = $event->getData();

                if ($data == null) {
                    return;
                }

                if ($data instanceof Projet) {

                    $commune = null;
                    $departement = null;
                    if ($data->getId()) {
                        $commune = ($data->getSection()) ? $data->getSection()->getCommune() : null;
                        $departement = ($commune) ? $commune->getDepartement() : null;
                    }


                    $refreshSections($form, $commune);
                    $setCommunes($form, $commune, $departement);
                    $setDepartement($form, $departement);

                }
            });

            $builder->addEventListener(FormEvents::PRE_BIND, function (FormEvent $event) use ($setCommunes, $refreshSections) {
                $form = $event->getForm();
                $data = $event->getData();


                if (array_key_exists('commune', $data)) {
                    $refreshSections($form, $data['commune']);
                    if (array_key_exists('departement', $data)) {
                        $setCommunes($form, $data['commune'], $data['departement']);
                    }
                }


            });





        }



		public function buildAsset(AssetBuilder $builder, $parameters) {

			$builder
				->add("script_projet", new AssetInternalJs(), array(
					"template" => "LocalisationBundle:Form:localisation_script.html.twig",
					"parameters" => array('item' => $parameters)
				));

		}




		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'ProjetBundle\Entity\Projet',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'projetbundle_projet';
		}
	}
