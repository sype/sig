<?php

	namespace ProjetBundle\Admin;

	use Uneak\RoutesManagerBundle\Routes\NestedAdminRoute;
	use Uneak\RoutesManagerBundle\Routes\NestedCRUDRoute;
	use Uneak\RoutesManagerBundle\Routes\NestedEntityRoute;
	use Uneak\RoutesManagerBundle\Routes\NestedGridRoute;

	class Projet extends NestedCRUDRoute {

		protected $entity = 'ProjetBundle\Entity\Projet';

		public function initialize() {
			parent::initialize();

			$this->setMetaData('_icon', 'building');
			$this->setMetaData('_label', 'Projets');
			$this->setMetaData('_description', 'Gestion des projets');
			$this->setMetaData('_menu', array(
				'index'  => '*/index',
				'new'    => '*/new',
			));
		}


		protected function buildCRUD() {
			$indexRoute = new NestedGridRoute('index');
			$indexRoute
				->setPath('')
				->setAction('index')
				->setMetaData('_icon', 'list')
				->setMetaData('_label', 'Liste')
				->addRowAction('show', '*/subject/show')
				->addRowAction('edit', '*/subject/edit')
				->addRowAction('delete', '*/subject/delete')

				->addColumn(array('title' => 'Nom', 'name' => 'label'))
				->addColumn(array('title' => 'Année', 'name' => 'year'))
				->addColumn(array('title' => 'Agence', 'name' => 'agence.label'))
			;
			$this->addChild($indexRoute);


			$newRoute = new NestedAdminRoute('new');
			$newRoute
				->setPath('new')
				->setAction('new')
				->setMetaData('_icon', 'plus-circle')
				->setMetaData('_label', 'New');
			$this->addChild($newRoute);


			$subjectRoute = new NestedEntityRoute('subject');
			$subjectRoute
				->setParameterName($this->getId())
				->setParameterPattern('\d+')
				->setEnabled(false)
				->setMetaData('_menu', array(
					'show'   => '*/subject/show',
					'edit'   => '*/subject/edit',
                    'lot'   => '*/subject/lot/new',
					'delete' => '*/subject/delete',
				));
			$this->addChild($subjectRoute);


			$showRoute = new NestedAdminRoute('show');
			$showRoute
				->setAction('show')
				->setMetaData('_icon', 'eye')
				->setMetaData('_label', 'Show')
				->setRequirement('_method', 'GET');
			$subjectRoute->addChild($showRoute);


			$editRoute = new NestedAdminRoute('edit');
			$editRoute
				->setAction('edit')
				->setMetaData('_icon', 'edit')
				->setMetaData('_label', 'Edit');
			$subjectRoute->addChild($editRoute);


			$deleteRoute = new NestedAdminRoute('delete');
			$deleteRoute
				->setAction('delete')
				->setMetaData('_icon', 'times')
				->setMetaData('_label', 'Delete');
			$subjectRoute->addChild($deleteRoute);



            //
            $lotRoute = new NestedAdminRoute('lot');
            $lotRoute->setEnabled(false);
            $subjectRoute->addChild($lotRoute);

            $newLotRoute = new NestedAdminRoute('new');
            $newLotRoute
                ->setAction('lotNew')
                ->setMetaData('_icon', 'edit')
                ->setMetaData('_label', 'Ajouter un lot');
            $lotRoute->addChild($newLotRoute);

            $lotSubjectRoute = new NestedEntityRoute('subject');
            $lotSubjectRoute
                ->setParameterName('lot')
                ->setParameterPattern('\d+')
                ->setEntity('ProjetBundle\Entity\ProjetLot')
                ->setEnabled(false)
                ->setMetaData('_menu', array(
                    'edit'   => 'edit',
                    'delete' => 'delete',
                ));
            $lotRoute->addChild($lotSubjectRoute);

            $lotEditRoute = new NestedAdminRoute('edit');
            $lotEditRoute
                ->setAction('lotEdit')
                ->setMetaData('_icon', 'edit')
                ->setMetaData('_label', 'Edit');
            $lotSubjectRoute->addChild($lotEditRoute);

            $lotDeleteRoute = new NestedAdminRoute('delete');
            $lotDeleteRoute
                ->setAction('lotDelete')
                ->setMetaData('_icon', 'times')
                ->setMetaData('_label', 'Delete');
            $lotSubjectRoute->addChild($lotDeleteRoute);
		}


	}
