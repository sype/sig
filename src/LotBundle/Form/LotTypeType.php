<?php

	namespace LotBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;

	class LotTypeType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('label', null, array(
					'label' => "Nom",
				))

                ->add('slug', null, array(
                    'label' => "Slug",
                ))

                ->add('sort', null, array(
                    'label' => "Ordre",
                ))

                ->add('imageFile', 'vich_file', array(
                        'label'         => "Photo",
                        'required'      => false,
                        'allow_delete'  => true, // not mandatory, default is true
                        'download_link' => true, // not mandatory, default is true
                        'required'      => false,
                    )
                )

				->add('active', null, array(
					'label' => "Activé",
					'required'      => false,
				));

		}



		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'LotBundle\Entity\LotType',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'lotbundle_lottype';
		}
	}
