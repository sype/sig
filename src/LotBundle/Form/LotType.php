<?php

	namespace LotBundle\Form;

	use Doctrine\ORM\EntityRepository;
	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;

	class LotType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('label', null, array(
					'label' => "Nom",
				))

                ->add('slug', null, array(
                    'label' => "Slug",
                ))

				->add('type', 'entity_select2', array(
					'label' => "Type",
					'class' => 'LotBundle:LotType',
					'query_builder'   => function (EntityRepository $er) {
						return $er->getLotTypesQuery();
					},
					'multiple'  => false,
					'empty_value' => "Sectionnez le type",
				))


				->add('active', null, array(
					'label' => "Activé",
					'required'      => false,
				));

		}



		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'LotBundle\Entity\Lot',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'lotbundle_lot';
		}
	}
