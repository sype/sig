<?php

	namespace LotBundle\Block;


	use Doctrine\ORM\EntityManager;
	use LotBundle\Entity\LotType as LotTypeEntity;
	use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
    use Uneak\FlatSkinBundle\Block\Panel\PanelPeople;
    use Uneak\FlatSkinBundle\Block\Panel\PanelPeoples;
    use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
    use Uneak\FlatSkinBundle\Block\Info\Info;
	use Uneak\FlatSkinBundle\Block\Info\Infos;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\ProfileNav;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

	class LotType {

		private $blockManager;
		private $routeManager;
		private $menuHelper;
		private $vichHelper;
		private $autorization;
		private $em;

		public function __construct(Block $blockManager, FlattenRouteManager $routeManager, MenuHelper $menuHelper, UploaderHelper $vichHelper, AuthorizationChecker $autorization, EntityManager $em) {
			$this->blockManager = $blockManager;
			$this->routeManager = $routeManager;
			$this->menuHelper = $menuHelper;
			$this->vichHelper = $vichHelper;
			$this->autorization = $autorization;
			$this->em = $em;
		}


        public function getLots(LotTypeEntity $lottype) {

            $projetRepo = $this->em->getRepository('LotBundle:Lot');
            $lots = $projetRepo->findLots($lottype);

            $panelPeoples = new PanelPeoples();
            $panelPeoples->setTitle("<i class='fa fa-icon fa-bed'> Lot</i>");
            foreach ($lots as $lot) {

                $flattenCrud = $this->routeManager->getFlattenRoute('lot/subject');
                $menu = new Menu();
                $menu->setTemplate("UneakFlatSkinBundle:Block:Menu/block_menu.html.twig");
                $menu->setMeta('style', 'social-links');
                $root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('lot' => $lot->getId()) );
                $menu->setRoot($root);

                $panelPeople = new PanelPeople();
                $panelPeople->setTitle($lot->getLabel());
                $panelPeople->setMenu($menu);

                $panelPeoples->addPanel($panelPeople);
            }

            return $panelPeoples;

        }


		public function getInfos(LotTypeEntity $lottype) {
			$infos = new Infos();
			$infos
				->setColumns(1)
				->setStripeRow(true)
                ->addBlock(new Info("Nom", $lottype->getLabel()), null, 30)
                ->addBlock(new Info("Slug", $lottype->getSlug()), null, 20)
                ->addBlock(new Info("Activé", ($lottype->getActive()) ? "Oui" : "Non"), null, 0);

			$wrapper = new Wrapper();
			$wrapper
				->setTitle("Informations")
				->addBlock($infos);

			return $wrapper;
		}



		public function getNavigation() {

			$flattenCrud = $this->routeManager->getFlattenRoute('lottype');

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($flattenCrud->getMetaData("_label"))
				->setDescription($flattenCrud->getMetaData("_description"))
				->setColor("green")
				->setHeaderAlign('vertical');
			$profileNav->addBlock($this->getNavigationMenu());

			return $profileNav;

		}

		public function getNavigationMenu() {

			$flattenCrud = $this->routeManager->getFlattenRoute('lottype');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud);
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}

		public function getLotTypeNavigation(LotTypeEntity $lottype) {

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($lottype->getLabel())
				->setColor("green")
				->setHeaderAlign('horizontal');
			$profileNav->addBlock($this->getLotTypeNavigationMenu($lottype));

			return $profileNav;

		}

		public function getLotTypeNavigationMenu(LotTypeEntity $lottype) {

			$flattenCrud = $this->routeManager->getFlattenRoute('lottype/subject');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('lottype' => $lottype->getId()));
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}



	}
