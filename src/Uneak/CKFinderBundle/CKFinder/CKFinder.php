<?php

	namespace Uneak\CKFinderBundle\CKFinder;
    require_once __DIR__ . '/../lib/core/connector/php/vendor/autoload.php';

    use CKSource\CKFinder\CKFinder as BaseCKFinder;
    use CKSource\CKFinder\Error;
    use CKSource\CKFinder\Exception\CKFinderException;
    use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
    use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
    use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
    use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;

    class CKFinder extends BaseCKFinder {

        protected $tokenStorage;
        protected $authenticationManager;
        protected $alwaysAuthenticate;
        protected $authenticationCallback;

        public function __construct($config, TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authenticationManager, $alwaysAuthenticate = false){
            $this->tokenStorage = $tokenStorage;
            $this->authenticationManager = $authenticationManager;
            $this->alwaysAuthenticate = $alwaysAuthenticate;
            $this->authenticationCallback = array($this, "isAuthenticated");

            // Development
            error_reporting(E_ALL);
            ini_set('display_errors', 1);

            parent::__construct($config);
        }

        function setAuthenticationCallback($callable) {
            $this->authenticationCallback = $callable;
        }

        protected function isAuthenticated(TokenInterface $token) {
            $granted = false;
            foreach ($token->getRoles() as $role) {
                if ($role->getRole() == "ROLE_ADMIN") {
                    $granted = true;
                    break;
                }
            }
            return $granted;
        }

        /**
         * Method used to check authentication
         */
        public function checkAuth() {
            if (null === ($token = $this->tokenStorage->getToken())) {
                throw new AuthenticationCredentialsNotFoundException('The token storage contains no authentication token. One possible reason may be that there is no firewall configured for this URL.');
            }

            if ($this->alwaysAuthenticate || !$token->isAuthenticated()) {
                $this->tokenStorage->setToken($token = $this->authenticationManager->authenticate($token));
            }

            $isAuthenticated = call_user_func($this->authenticationCallback, $token);
            if (!$isAuthenticated) {
                ini_set('display_errors', 0);
                throw new CKFinderException('CKFinder is disabled', Error::CONNECTOR_DISABLED);
            }
        }

	}
