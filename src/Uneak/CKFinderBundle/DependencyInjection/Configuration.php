<?php

namespace Uneak\CKFinderBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('uneak_ck_finder');

        $rootNode
            ->children()
                ->scalarNode('licenseName')->end()
                ->scalarNode('licenseKey')->end()
                ->arrayNode('privateDir')
                    ->children()
                        ->scalarNode('backend')->end()
                        ->scalarNode('tags')->end()
                        ->scalarNode('logs')->end()
                        ->scalarNode('cache')->end()
                        ->scalarNode('thumbs')->end()
                    ->end()
                ->end()
                ->arrayNode('images')
                    ->children()
                        ->integerNode('maxWidth')->end()
                        ->integerNode('maxHeight')->end()
                        ->integerNode('quality')->end()
                        ->arrayNode('sizes')
                            ->useAttributeAsKey('name')
                            ->prototype('array')
                                ->children()
                                    ->integerNode('width')->end()
                                    ->integerNode('height')->end()
                                    ->integerNode('quality')->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()

                ->arrayNode('backends')
                    ->performNoDeepMerging()
                    ->prototype('array')
                        ->children()
                            ->scalarNode('name')->isRequired()->end()
                            ->scalarNode('adapter')->defaultValue('local')->end()
                            ->scalarNode('baseUrl')->defaultValue('/uploads/')->end()
                            ->scalarNode('root')->end()
                            ->scalarNode('chmodFiles')->defaultValue('0777')->end()
                            ->scalarNode('chmodFolders')->defaultValue('0777')->end()
                            ->scalarNode('filesystemEncoding')->defaultValue('UTF-8')->end()
                        ->end()
                    ->end()
                ->end()

                ->scalarNode('defaultResourceTypes')->end()

                ->arrayNode('resourceTypes')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('name')->end()
                            ->scalarNode('directory')->end()
                            ->integerNode('maxSize')->end()
                            ->scalarNode('allowedExtensions')->end()
                            ->scalarNode('deniedExtensions')->end()
                            ->scalarNode('backend')->end()
                        ->end()
                    ->end()
                ->end()

                ->scalarNode('roleSessionVar')->end()

                ->arrayNode('accessControl')
                    ->performNoDeepMerging()
                    ->prototype('array')
                        ->children()
                            ->scalarNode('role')->defaultValue('*')->end()
                            ->scalarNode('resourceType')->defaultValue('*')->end()
                            ->scalarNode('folder')->defaultValue('/')->end()
                            ->booleanNode('FOLDER_VIEW')->defaultTrue()->end()
                            ->booleanNode('FOLDER_CREATE')->defaultTrue()->end()
                            ->booleanNode('FOLDER_RENAME')->defaultTrue()->end()
                            ->booleanNode('FOLDER_DELETE')->defaultTrue()->end()
                            ->booleanNode('FILE_VIEW')->defaultTrue()->end()
                            ->booleanNode('FILE_UPLOAD')->defaultTrue()->end()
                            ->booleanNode('FILE_RENAME')->defaultTrue()->end()
                            ->booleanNode('FILE_DELETE')->defaultTrue()->end()
                            ->booleanNode('IMAGE_RESIZE')->defaultTrue()->end()
                            ->booleanNode('IMAGE_RESIZE_CUSTOM')->defaultTrue()->end()
                        ->end()
                    ->end()
                ->end()

                ->booleanNode('overwriteOnUpload')->end()
                ->booleanNode('checkDoubleExtension')->end()
                ->booleanNode('disallowUnsafeCharacters')->end()
                ->booleanNode('secureImageUploads')->end()
                ->booleanNode('checkSizeAfterScaling')->end()

                ->arrayNode('htmlExtensions')->prototype('scalar')->end()->end()
                ->arrayNode('hideFolders')->prototype('scalar')->end()->end()
                ->arrayNode('hideFiles')->prototype('scalar')->end()->end()
                ->booleanNode('forceAscii')->end()
                ->booleanNode('xSendfile')->end()

                ->booleanNode('debug')->end()
                ->scalarNode('pluginsDirectory')->end()
                ->arrayNode('plugins')->end()
                ->arrayNode('cache')
                    ->children()
                        ->integerNode('imagePreview')->end()
                        ->integerNode('thumbnails')->end()
                    ->end()

                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
