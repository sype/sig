<?php

namespace Uneak\CKFinderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{

    public function connectorAction() {

        $ckfinder = $this->get('uneak.ckfinder');
        $ckfinder->run();

        return new Response();
    }

}
