<?php

	namespace Uneak\GoogleMapBundle\Form\DataTransformer;

	use Symfony\Component\Form\DataTransformerInterface;

	class StringToJsonArrayTransformer implements DataTransformerInterface {

		public function reverseTransform($string) {
			return json_decode($string);
		}

		public function transform($array) {
			return json_encode($array);
		}
	}