<?php

	namespace Uneak\GoogleMapBundle\Form;

	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\FormInterface;
	use Symfony\Component\Form\FormView;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;
	use Uneak\AssetsManagerBundle\Assets\AssetBuilder;
	use Uneak\AssetsManagerBundle\Assets\Js\AssetExternalJs;
	use Uneak\AssetsManagerBundle\Assets\Js\AssetInternalJs;
	use Uneak\FormsManagerBundle\Forms\AssetsComponentType;
    use Uneak\GoogleMapBundle\Form\DataTransformer\StringToJsonArrayTransformer;

    class GoogleMapType extends AssetsComponentType {

		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder->addModelTransformer(new StringToJsonArrayTransformer());
		}

		public function buildView(FormView $view, FormInterface $form, array $options) {

			$view->vars['options'] = $options['options'];
			$view->vars['search_fields'] = $options['search_fields'];
            $view->vars['location'] = $options['location'];
            $view->vars['bounds'] = $options['bounds'];


			$value = json_decode($view->vars['value'], true);

//			UPDATE `Projet` SET `location`= null WHERE 1

			if ($value && isset($value['location']['latitude'])) {
				$view->vars['location']['latitude'] = $value['location']['latitude'];
				$view->vars['location']['longitude'] = $value['location']['longitude'];
				$view->vars['bounds']['ne']['latitude'] = $value['bounds']['ne']['latitude'];
				$view->vars['bounds']['ne']['longitude'] = $value['bounds']['ne']['longitude'];
				$view->vars['bounds']['sw']['latitude'] = $value['bounds']['sw']['latitude'];
				$view->vars['bounds']['sw']['longitude'] = $value['bounds']['sw']['longitude'];
            }

		}

		public function setDefaultOptions(OptionsResolverInterface $resolver) {

			$resolver->setDefaults(array(
				'location' => array(
					'editable' => true,
				),
				'bounds' => array(
					'editable' => true,
				),
				'search_fields' => array(),
				'options' => array(),
				'compound' => false
			));

			$resolver->setDefined(array('location', 'bounds', 'search_fields', 'options'));

		}

		public function buildAsset(AssetBuilder $builder, $parameters) {

			$builder
				->add("googlemap_lib_js", new AssetExternalJs(), array(
					"src" => "http://maps.google.com/maps/api/js?sensor=false&libraries=places"
				))
				->add("googlemap_form_js", new AssetExternalJs(), array(
					"src" => "/bundles/uneakgooglemap/googlemap_form.js",
					"dependencies" => array("googlemap_lib_js")
				))
				->add("googlemap_form_script", new AssetInternalJs(), array(
					"template" => "UneakGoogleMapBundle:Form:googlemap_script.html.twig",
					"parameters" => array('item' => $parameters)
				));

		}

		public function getTheme() {
			return "UneakGoogleMapBundle:Form:googlemap_theme.html.twig";
		}

		public function getName() {
			return 'uneak_googlemap';
		}

	}
