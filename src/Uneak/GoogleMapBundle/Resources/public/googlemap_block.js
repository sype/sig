
var UneakGoogleMapBlock = function(field, location, bounds) {
    this.$field = $('#'+field);

    var mapOptions = {
        center: location,
        disableDefaultUI: true,
        draggable: false,
        scrollwheel: false
    };

    this.map = new google.maps.Map(document.getElementById(field), mapOptions);

    this.bounds = bounds;

    this.marker = new google.maps.Marker({
        position: location,
        draggable: false,
        map: this.map,
        animation: google.maps.Animation.DROP
    });

    this.refreshView();
};


UneakGoogleMapBlock.prototype.refreshView = function () {
    this.map.setCenter(this.bounds.getCenter());
    this.map.fitBounds(this.bounds);
};



UneakGoogleMapBlock.prototype.geocode = function (address) {
    var self = this;
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            self.bounds = results[0].geometry.bounds;
            self.marker.setPosition(results[0].geometry.location);
            self.refreshView();
        }
    });
};

