
var UneakGoogleMapForm = function(field, location, bounds, options) {
    this.$field = $('#'+field);

    var mapOptions = {
        center: location,
        streetViewControl: false,
        zoom: 9
    };

    this.map = new google.maps.Map(document.getElementById(field+'_container'), mapOptions);

    var rectangleOptions = {
        map: this.map,
        fillColor: '#ff766c',
        fillOpacity: 0.1,
        strokeColor: '#ff766c',
        strokeOpacity: 0.2,
        strokeWeight: 1,
        editable: true,
        draggable: true,
        bounds: bounds
    };
    if (typeof options.bounds_editable !== "undefined" && options.bounds_editable === false) {
        rectangleOptions.editable = false;
        rectangleOptions.draggable = false;
    }



    this.rectangle = new google.maps.Rectangle(rectangleOptions);
    this.rectangle.setBounds(bounds);

    var markerOptions = {
        position: location,
        draggable: true,
        map: this.map,
        animation: google.maps.Animation.DROP
    };
    if (typeof options.location_editable !== "undefined" && options.location_editable === false) {
        rectangleOptions.draggable = false;
    }

    this.marker = new google.maps.Marker(markerOptions);

    var self = this;
    google.maps.event.addListener(this.rectangle, 'bounds_changed', function(){

        if (this.getBounds().contains(self.marker.getPosition()) == false) {
            self.marker.setPosition(this.getBounds().getCenter());
        }

        self.updateData();
    });


    var markerPosition;
    google.maps.event.addListener(this.marker, "dragstart", function(){
        markerPosition = this.getPosition();
    });

    google.maps.event.addListener(this.marker, "dragend", function(){

        if (self.rectangle.getBounds().contains(this.getPosition()) == false) {
            this.setPosition(markerPosition);
        }

        self.updateData();
    });

    this.refreshView();
    this.updateData();
};


UneakGoogleMapForm.prototype.refreshView = function () {
    this.map.setCenter(this.rectangle.getBounds().getCenter());
    this.map.fitBounds(this.rectangle.getBounds());
};

UneakGoogleMapForm.prototype.updateData = function () {

    var pos = this.marker.getPosition();
    var ne = this.rectangle.getBounds().getNorthEast();
    var sw = this.rectangle.getBounds().getSouthWest();

    var coord = {
        location: {
            latitude: pos.lat(),
            longitude: pos.lng(),
        },
        bounds: {
            ne: {
                latitude: ne.lat(),
                longitude: ne.lng(),
            },
            sw: {
                latitude: sw.lat(),
                longitude: sw.lng(),
            },
        }
    };

    this.$field.val(JSON.stringify(coord));
};

UneakGoogleMapForm.prototype.geocode = function (address) {
    var self = this;
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            self.rectangle.setBounds(results[0].geometry.bounds);
            self.marker.setPosition(results[0].geometry.location);

            self.refreshView();
            self.updateData();
        }
    });
};

