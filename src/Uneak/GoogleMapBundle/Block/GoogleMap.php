<?php

	namespace Uneak\GoogleMapBundle\Block;

	use Uneak\AssetsManagerBundle\Assets\AssetBuilder;
	use Uneak\AssetsManagerBundle\Assets\Js\AssetExternalJs;
	use Uneak\AssetsManagerBundle\Assets\Js\AssetInternalJs;
	use Uneak\BlocksManagerBundle\Blocks\Component;

	class GoogleMap extends Component {

		protected $locationData;
		protected $address;

		public function __construct($title = "", $locationData = array(), $address = "") {
			parent::__construct();
			$this->metas->_init($locationData);
			$this->address = $address;
			$this->template = 'UneakGoogleMapBundle:Block:googlemap.html.twig';
		}

		/**
		 * @return string
		 */
		public function getAddress() {
			return $this->address;
		}


		public function buildAsset(AssetBuilder $builder, $parameters) {

			$builder
				->add("googlemap_lib_js", new AssetExternalJs(), array(
					"src" => "http://maps.google.com/maps/api/js?sensor=false&libraries=places"
				))
				->add("googlemap_block_js", new AssetExternalJs(), array(
					"src" => "/bundles/uneakgooglemap/googlemap_block.js",
					"dependencies" => array("googlemap_lib_js")
				))
				->add("googlemap_block_script", new AssetInternalJs(), array(
					"template"   => "UneakGoogleMapBundle:Block:googlemap_script.html.twig",
					"parameters" => array('item' => $parameters)
				));

		}


		/**
		 * @return mixed
		 */
		public function getLocationData() {
			return $this->locationData;
		}

		/**
		 * @param mixed $locationData
		 */
		public function setLocationData($locationData) {
			$this->locationData = $locationData;

			return $this;
		}


	}
