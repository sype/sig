<?php

	namespace AppBundle\Form;

	use Doctrine\ORM\EntityRepository;
    use LocalisationBundle\Entity\Communes;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
	use Symfony\Component\Validator\Constraints\Collection;
	use Uneak\AssetsManagerBundle\Assets\AssetBuilder;
	use Uneak\AssetsManagerBundle\Assets\Js\AssetInternalJs;
	use Uneak\FormsManagerBundle\Forms\AssetsComponentType;

	class SearchType extends AssetsComponentType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
            $yearRange = range(Date('Y') - 15, date('Y'));

            $builder

                ->add('type', 'choice_select2', array(
                    'label' => "Type de contenu",
                    'choices' => array(
                        'page'   => 'Page',
                        'news' => 'Article',
                        'agence'   => 'Agence',
                        'projet'   => 'Projet'
                    ),
                    'multiple' => true
                ))

                ->add('content', 'text', array(
                    'label' => "Contenu",
                ))



                ->add('year', 'choice_select2', array(
                    'label' => "Année",
                    'choices' => array_combine($yearRange, $yearRange),
                    'options' => array(
                        'placeholder' => "Sectionnez l'année de construction",
                        'language' => 'fr'
                    ),
                    'required'        => false,
                    'empty_value' => 'Sectionnez l\'année de construction',
                ))
                ->add('lot', 'entity_select2', array(
                    'label' => "Lot",
                    'class' => 'LotBundle:Lot',
                    'query_builder' => function (EntityRepository $er) {
                        $qb = $er->getLotsQuery();
                        return $qb;
                    },
                    'options' => array(
                        'placeholder' => "Sectionnez le lot",
                        'language' => 'fr'
                    ),
                    'multiple'  => false,
                    'required'        => false,

                    'empty_value' => 'Sectionnez le lot',
                ))
            ;


            $factory = $builder->getFormFactory();

            $refreshSections = function ($form, $communes) use ($factory) {

                $form->add($factory->createNamed('section', 'entity_select2', null, array(
                    'class'           => 'LocalisationBundle:Sections',
                    'property'        => 'label',
                    'empty_value'     => 'Sélectionnez votre section',
                    'auto_initialize' => false,
                    'required'        => false,
                    'attr'            => array(
                        'input_group' => array(
                            'prepend' => '.icon-map-marker'
                        )
                    ),
                    'query_builder'   => function (EntityRepository $er) use ($communes) {
                        $qb = $er->getSectionsQuery();

                        if ($communes instanceof Communes) {
                            $qb->where('sections.commune = :commune')->setParameter('commune', $communes);
                        } elseif (is_numeric($communes)) {
                            $qb->where('commune.id = :commune')->setParameter('commune', $communes);
                        } else {
                            $qb->where('commune.id = :commune')->setParameter('commune', 0);
                        }

                        return $qb;
                    }
                )));
            };



            $setCommunes = function ($form, $commune, $departement) use ($factory) {

                $form->add($factory->createNamed('commune', 'entity_select2', null, array(
                    'class'           => 'LocalisationBundle:Communes',
                    'property'        => 'label',
                    'empty_value'     => 'Sélectionnez votre commune',
                    'auto_initialize' => false,
                    'required'        => false,
//                    'mapped'          => false,
                    'data'            => $commune,
                    'attr'            => array(
                        'input_group' => array(
                            'prepend' => '.icon-map-marker'
                        )
                    ),
                    'query_builder'   => function (EntityRepository $er) use ($departement) {
                        $qb = $er->getCommunesQuery();

                        if (is_numeric($departement)) {
                            $qb->where('departement.id = :departement')->setParameter('departement', $departement);
                        } else {
                            $qb->where('departement.id = :departement')->setParameter('departement', 0);
                        }

                        return $qb;
                    }
                )));
            };

            $setDepartement = function ($form, $departement) use ($factory) {
                $form->add($factory->createNamed('departement', 'entity_select2', null, array(
                    'class'           => 'LocalisationBundle:Departements',
                    'property'        => 'label',
                    'query_builder'   => function (EntityRepository $er) {
                        return $er->getDepartementsQuery();
                    },
                    'empty_value'     => 'Sélectionnez votre département',
                    'empty_data'      => null,
                    'auto_initialize' => false,
                    'data'            => $departement,
                    'label'           => 'Département',
                    'required'        => false,
//                    'mapped'          => false,
                    'attr'            => array(
                        'input_group' => array(
                            'prepend' => '.icon-globe'
                        )
                    )
                )));
            };


            $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($setCommunes, $setDepartement, $refreshSections) {
                $form = $event->getForm();
                $data = $event->getData();

                if ($data == null) {
                    return;
                }

//                if ($data instanceof Projet) {
                if ($data) {

                    $section = $data['section'];
                    $commune = $data['commune'];
                    $departement = $data['departement'];

//                    if ($data['section']) {
////                        $commune = ($data->getSection()) ? $data->getSection()->getCommune() : null;
////                        $departement = ($commune) ? $commune->getDepartement() : null;
//                    } else if ($data['commune']) {
//                        $commune = $data['commune'];
//                    } else if ($data['departement']) {
//                        $departement = $data['departement'];
//                    }


                    $refreshSections($form, $commune);
                    $setCommunes($form, $commune, $departement);
                    $setDepartement($form, $departement);

                }
            });

            $builder->addEventListener(FormEvents::PRE_BIND, function (FormEvent $event) use ($setCommunes, $refreshSections) {
                $form = $event->getForm();
                $data = $event->getData();


                if (array_key_exists('commune', $data)) {
                    $refreshSections($form, $data['commune']);
                    if (array_key_exists('departement', $data)) {
                        $setCommunes($form, $data['commune'], $data['departement']);
                    }
                }


            });





        }



		public function buildAsset(AssetBuilder $builder, $parameters) {

			$builder
				->add("script_projet", new AssetInternalJs(), array(
					"template" => "LocalisationBundle:Form:localisation_script.html.twig",
					"parameters" => array('item' => $parameters)
				));

		}






		/**
		 * @return string
		 */
		public function getName() {
			return 'appbundle_search';
		}
	}
