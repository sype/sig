<?php

	namespace AppBundle\Block\Messages;

	use Doctrine\ORM\EntityManager;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\FlatSkinBundle\Block\Message\Message;

	class Messages extends Block {

		protected $em;

		public function __construct(EntityManager $em) {
			parent::__construct();
			$this->em = $em;
			$this->title = "Online Customers";
			$this->getMetas()->__set("class", "quick-chat-list");
			$this->template = "UneakFlatSkinBundle:Block:blocks.html.twig";
		}

		public function preRender() {
			parent::preRender();

			$message = new Message();
			$message
				->getMetas()->__set("class", "online")
				->setTemplate("UneakFlatSkinBundle:Block:Message/block_sidebar_message.html.twig")
				->setTitle("John Doe")
				->setSubTitle("Guadeloupe")
				->setPhoto("img/hello.jpg")
				->setBadge("3");
			$this->addBlock($message);

			$message = new Message();
			$message
				->getMetas()->__set("class", "online")
				->setTemplate("UneakFlatSkinBundle:Block:Message/block_sidebar_message.html.twig")
				->setTitle("John Doe")
				->setSubTitle("Guadeloupe")
				->setPhoto("img/hello.jpg")
				->setBadge("3");

			$this->addBlock($message);
		}

	}
