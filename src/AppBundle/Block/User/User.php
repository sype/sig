<?php

	namespace AppBundle\Block\User;

	use Doctrine\ORM\EntityManager;
	use Knp\Menu\FactoryInterface;
	use Symfony\Bundle\FrameworkBundle\Routing\Router;
	use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
	use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\User\User as SkinUser;
	use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

	class User extends SkinUser {

		protected $em;
		protected $tokenStorage;
		protected $router;
		protected $uploaderHelper;
		protected $menuHelper;
		protected $fRouteManager;

		public function __construct(EntityManager $em, MenuHelper $menuHelper, TokenStorage $tokenStorage, UploaderHelper $uploaderHelper, Router $router, FlattenRouteManager $fRouteManager) {
			parent::__construct();
			$this->menuHelper = $menuHelper;
			$this->em = $em;
			$this->router = $router;
			$this->tokenStorage = $tokenStorage;
			$this->uploaderHelper = $uploaderHelper;
			$this->fRouteManager = $fRouteManager;
		}

		public function preRender() {
			parent::preRender();



			$user = $this->tokenStorage->getToken()->getUser();
			$this->setUsername($user->getFirstName()." ".$user->getLastName()." (".$user->getUserName().")");
			$this->setPhoto($this->uploaderHelper->asset($user, 'imageFile'));


			$menuBlock = new Menu();
			$flattenCrud = $this->fRouteManager->getFlattenRoute('user/subject');
			$menu = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('user' => $user->getId()));
			$menu->setChildrenAttribute('class', 'login-menu');


			$menu->addChild('logout', array(
				'label'          => 'Déconnection',
				'icon'           => 'key',
				'uri'            => $this->router->generate("fos_user_security_logout"),
				'linkAttributes' => array(
					'role'     => 'menuitem',
					'tabindex' => '-1',
				),
			));


			$menuBlock->setRoot($menu);


			$this->addBlock($menuBlock);
		}



	}
