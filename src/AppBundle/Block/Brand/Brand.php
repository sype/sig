<?php

	namespace AppBundle\Block\Brand;

	use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
	use Uneak\FlatSkinBundle\Block\Brand\Brand as SkinBrand;

	class Brand extends SkinBrand {

		protected $menuHelper;
		protected $fRouteManager;

		public function __construct(MenuHelper $menuHelper, FlattenRouteManager $fRouteManager) {
			parent::__construct();
			$this->menuHelper = $menuHelper;
			$this->fRouteManager = $fRouteManager;
		}


		public function preRender() {
			parent::preRender();

			$this->setTitle("SIG");
			$this->setSubTitle("GUADELOUPE");
			$this->setLink($this->fRouteManager->getFlattenRoute('admin')->getRoutePath());

		}

	}
