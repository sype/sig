<?php

	namespace AppBundle\Block\Menu;

	use Doctrine\ORM\EntityManager;
	use Knp\Menu\FactoryInterface;
	use Symfony\Bundle\FrameworkBundle\Routing\Router;
	use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
	use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\RoutesManagerBundle\Routes\FlattenEntityRoute;
	use Uneak\RoutesManagerBundle\Routes\FlattenParameterRoute;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
	use Uneak\RoutesManagerBundle\Routes\NestedRouteManager;
	use Uneak\FlatSkinBundle\Block\Menu\MainMenu as SkinMenu;

	class MainMenu extends SkinMenu {

		protected $em;
		protected $router;
		protected $fRouteManager;
		protected $menuHelper;
		protected $tokenStorage;

		public function __construct(EntityManager $em, MenuHelper $menuHelper, FlattenRouteManager $fRouteManager, Router $router, TokenStorage $tokenStorage) {
			parent::__construct();
			$this->em = $em;
			$this->menuHelper = $menuHelper;
			$this->router = $router;
			$this->fRouteManager = $fRouteManager;
			$this->tokenStorage = $tokenStorage;

			$factory = $this->menuHelper->getFactory();
			$this->root = $factory->createItem('root');
		}

		public function preRender() {
			parent::preRender();


			$userId = $this->tokenStorage->getToken()->getUser()->getId();
			$this->root->addChild($this->menuHelper->createItem($this->fRouteManager->getFlattenRoute('admin')));

			$itemNews = $this->menuHelper->createItem($this->fRouteManager->getFlattenRoute('news/index'));
			$itemNews->setIcon("newspaper-o");
			$itemNews->setLabel("Articles");
			$this->root->addChild($itemNews);

			$itemPages = $this->menuHelper->createItem($this->fRouteManager->getFlattenRoute('page/index'));
			$itemPages->setIcon("file-o");
			$itemPages->setLabel("Pages");
			$this->root->addChild($itemPages);

            $itemProjet = $this->menuHelper->createItem($this->fRouteManager->getFlattenRoute('projet/index'));
            $itemProjet->setIcon("building");
            $itemProjet->setLabel("Projets");
            $this->root->addChild($itemProjet);

			$itemGallery = $this->menuHelper->createItem($this->fRouteManager->getFlattenRoute('gallery/index'));
			$itemGallery->setIcon("picture-o");
			$itemGallery->setLabel("Galeries");
			$this->root->addChild($itemGallery);

			$itemPropositions = $this->menuHelper->createItem($this->fRouteManager->getFlattenRoute('proposition/index'));
			$itemPropositions->setIcon("star");
			$itemPropositions->setLabel("Propositions du mois");
			$this->root->addChild($itemPropositions);


//			$itemSlider = $this->menuHelper->createItem($this->fRouteManager->getFlattenRoute('slider/index'));
//			$itemSlider->setIcon("user");
//			$itemSlider->setLabel("Slider");
//			$this->root->addChild($itemSlider);


			$itemSlide = $this->menuHelper->createItem($this->fRouteManager->getFlattenRoute('slide/index'));
			$itemSlide->setIcon("list-alt");
			$itemSlide->setLabel("Slides de la page d'accueil");
			$this->root->addChild($itemSlide);


            $itemSlide = $this->menuHelper->createItem($this->fRouteManager->getFlattenRoute('video/index'));
            $itemSlide->setIcon("list-alt");
            $itemSlide->setLabel("Vidéos de la page d'accueil");
            $this->root->addChild($itemSlide);


			$this->root->addChild($this->menuHelper->createItem($this->fRouteManager->getFlattenRoute('configuration')));




			//			$this->root->addChild('dashboard', array(
			//				'label' => 'Dashboard',
			//				'icon'  => 'home',
			//				'uri'   => 'http://uneak.fr',
			//			));

//			$nRoutes = $this->nRouteManager->getNestedRoutes();
//			$routeCollection = $this->router->getRouteCollection();
//
//			foreach ($nRoutes as $nRoute) {
//
//				$flattenCrud = $routeCollection->get('uneak_admin.' . $nRoute->getId());
//				$rubrique = $this->root->addChild($flattenCrud->getId(), array(
//					'label' => $flattenCrud->getMetaData('_label'),
//					'icon'  => $flattenCrud->getMetaData('_icon'),
//					'uri'   => '#',
//				));
//
//				$children = $flattenCrud->getChildren();
//				foreach ($children as $flattenChild) {
//
//					if (!$flattenChild instanceof FlattenParameterRoute) {
//						$rubrique->addChild($flattenChild->getId(), array(
//							'label' => $flattenChild->getMetaData('_label'),
//							'icon'  => $flattenChild->getMetaData('_icon'),
//							'uri'   => $this->router->generate($flattenChild->getId()),
//						));
//					}
//				}
//			}


		}


	}
