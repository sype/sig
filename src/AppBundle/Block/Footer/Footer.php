<?php

	namespace AppBundle\Block\Footer;

	use Uneak\FlatSkinBundle\Block\Footer\Footer as SkinFooter;

	class Footer extends SkinFooter {

		public function __construct() {
			parent::__construct();
			$this->title = "<a href='http://www.uneak.fr'>uneak.fr</a> 2015 (c)";
		}

		public function preRender() {
			parent::preRender();
		}

	}
