<?php

	namespace AppBundle\Block\Stats;

	use Doctrine\ORM\EntityManager;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\FlatSkinBundle\Block\Stat\Stat;

	class Stats extends Block {

		protected $em;

		public function __construct(EntityManager $em) {
			parent::__construct();
			$this->em = $em;
			$this->template = "UneakFlatSkinBundle:Block:blocks.html.twig";
			$this->title = "Pending task";
			$this->getMetas()->__set("class", "p-task tasks-bar");
		}

		public function preRender() {
			parent::preRender();



			$message = new Stat();
			$message
				->setTemplate("UneakFlatSkinBundle:Block:Stat/block_sidebar_stat.html.twig")
				->setTitle("John Doe")
				->setComplete("78")
				->setContext("warning");

			$this->addBlock($message);

			$message = new Stat();
			$message
				->setTemplate("UneakFlatSkinBundle:Block:Stat/block_sidebar_stat.html.twig")
				->setTitle("John Allo")
				->setComplete("50")
				->setContext("danger");

			$this->addBlock($message);
		}

	}
