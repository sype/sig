<?php

	namespace AppBundle\Block\NotificationIcons;

	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\FlatSkinBundle\Block\Notification\Notification;
	use Uneak\FlatSkinBundle\Block\NotificationIcon\NotificationIcon;

	class NotificationsNotificationIcon extends NotificationIcon {

		public function __construct() {
			parent::__construct();
			$this->title = "Notification";
			$this->badge = "YEP";
			$this->icon = "users";
			$this->getMetas()->__set("class", "dropdown-menu extended notification");
		}

		public function preRender() {
			parent::preRender();

			$messages = new Block();

			$message = new Notification();
			$message
				->setTime("now")
				->setContext("danger")
				->setIcon("user")
				->setTitle("Marco");
			$messages->addBlock($message);

			$message = new Notification();
			$message
				->setTime("now")
				->setContext("warning")
				->setIcon("user")
				->setTitle("coucou");
			$messages->addBlock($message);


			$this->content = $messages;
		}

	}
