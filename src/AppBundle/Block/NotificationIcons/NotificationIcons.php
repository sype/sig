<?php

namespace AppBundle\Block\NotificationIcons;

use Doctrine\ORM\EntityManager;
use Uneak\FlatSkinBundle\Block\NotificationIcon\NotificationIcons as SkinNotificationIcons;

class NotificationIcons extends SkinNotificationIcons {

    protected $em;

    public function __construct(EntityManager $em) {
        parent::__construct();
        $this->em = $em;
    }

	public function preRender() {
		parent::preRender();
		$this->addBlock(new StatsNotificationIcon());
		$this->addBlock(new MessagesNotificationIcon());
		$this->addBlock(new NotificationsNotificationIcon());
	}

}
