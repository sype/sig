<?php

	namespace AppBundle\Block\NotificationIcons;

	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\FlatSkinBundle\Block\NotificationIcon\NotificationIcon;
	use Uneak\FlatSkinBundle\Block\Stat\Stat;

	class StatsNotificationIcon extends NotificationIcon {

		public function __construct() {
			parent::__construct();
			$this->title = "Statistiques";
			$this->badge = "6";
			$this->icon = "users";
			$this->getMetas()->__set("class", "dropdown-menu extended tasks-bar");
		}

		public function preRender() {
			parent::preRender();

			$stats = new Block();

			$stat = new Stat();
			$stat
				->setTitle("Uneak")
				->setComplete(10)
				->setContext("danger");
			$stats->addBlock($stat);

			$stat = new Stat();
			$stat
				->setTitle("Objectif 2010")
				->setComplete(60)
				->setContext("warning");
			$stats->addBlock($stat);

			$this->content = $stats;
		}

	}
