<?php

	namespace AppBundle\Block\NotificationIcons;

	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\FlatSkinBundle\Block\Message\Message;
	use Uneak\FlatSkinBundle\Block\NotificationIcon\NotificationIcon;

	class MessagesNotificationIcon extends NotificationIcon {

		public function __construct() {
			parent::__construct();
			$this->title = "Message";
			$this->badge = "10";
			$this->icon = "users";
			$this->getMetas()->__set("class", "dropdown-menu extended inbox");
		}

		public function preRender() {
			parent::preRender();

			$messages = new Block();

			$message = new Message();
			$message
				->setTitle("Marc Galoyer")
				->setSubTitle("Hello guy")
				->setTime("Maintenant")
				->setPhoto("./img/uneak.jpg")
			;
			$messages->addBlock($message);

			$message = new Message();
			$message
				->setTitle("Marc Galoyer")
				->setSubTitle("Hello guy")
				->setTime("Maintenant")
				->setPhoto("./img/uneak.jpg")
			;
			$messages->addBlock($message);

			$this->content = $messages;
		}
	}
