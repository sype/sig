<?php

	namespace AppBundle\Block;


	use Doctrine\ORM\EntityManager;
	use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
	use Uneak\FlatSkinBundle\Block\Panel\PanelPeople;
	use Uneak\FlatSkinBundle\Block\Panel\PanelPeoples;
	use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\ProfileNav;
	use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

	class Dashboard {

		private $blockManager;
		private $routeManager;
		private $menuHelper;
		private $vichHelper;
		private $autorization;
		private $em;

		public function __construct(Block $blockManager, FlattenRouteManager $routeManager, MenuHelper $menuHelper, UploaderHelper $vichHelper, AuthorizationChecker $autorization, EntityManager $em) {
			$this->blockManager = $blockManager;
			$this->routeManager = $routeManager;
			$this->menuHelper = $menuHelper;
			$this->vichHelper = $vichHelper;
			$this->autorization = $autorization;
			$this->em = $em;
		}



		public function getPropositions() {

			$propositionRepo = $this->em->getRepository('PropositionBundle:Proposition');
			$propositions = $propositionRepo->findProposition();

			$panelPeoples = new PanelPeoples();
			$panelPeoples->setTitle("<i class='fa fa-star'></i> <span>Propositions du mois</span>");

			$menu = new Menu();
			$menu->setTemplate("UneakFlatSkinBundle:Block:Menu/block_menu.html.twig");
			$menu->setMeta('style', 'nav nav-pills nav-stacked');

			$root = $this->menuHelper->getFactory()->createItem('root');
			$fRoute = $this->routeManager->getFlattenRoute('proposition');
			$itemSection = $this->menuHelper->createItem($fRoute->getChild('new'));
			$itemSection->setIcon($fRoute->getMetaData("_icon"));
			$itemSection->setLabel(" Ajouter une nouvelle proposition");
			$root->addChild($itemSection);

			$menu->setRoot($root);
			$panelPeoples->addMenu($menu);

			foreach ($propositions as $proposition) {

				$flattenCrud = $this->routeManager->getFlattenRoute('proposition/subject');
				$menu = new Menu();
				$menu->setTemplate("UneakFlatSkinBundle:Block:Menu/block_menu.html.twig");
				$menu->setMeta('style', 'social-links');
				$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('proposition' => $proposition->getId()) );
				$menu->setRoot($root);

				$panelPeople = new PanelPeople();
				$panelPeople->setTitle($proposition->getLabel());
				$panelPeople->setDescription($this->_resume($proposition->getDescription(), 50));
				$panelPeople->setPhoto($this->vichHelper->asset($proposition, 'imageFile'));
				$panelPeople->setMenu($menu);

				$panelPeoples->addPanel($panelPeople);
			}

			return $panelPeoples;

		}



		public function getSlides() {

			$slideRepo = $this->em->getRepository('SliderBundle:Slide');
			$slides = $slideRepo->findSlidesBySlug(false, 'site-home');

			$panelPeoples = new PanelPeoples();
			$panelPeoples->setTitle("<i class='fa fa-th-list'></i> <span>Slides de la page d'accueil</span>");

			$menu = new Menu();
			$menu->setTemplate("UneakFlatSkinBundle:Block:Menu/block_menu.html.twig");
			$menu->setMeta('style', 'nav nav-pills nav-stacked');

			$root = $this->menuHelper->getFactory()->createItem('root');
			$fRoute = $this->routeManager->getFlattenRoute('slide');
			$itemSection = $this->menuHelper->createItem($fRoute->getChild('new'));
			$itemSection->setIcon($fRoute->getMetaData("_icon"));
			$itemSection->setLabel(" Ajouter un nouveau slide");
			$root->addChild($itemSection);

			$menu->setRoot($root);
			$panelPeoples->addMenu($menu);

			foreach ($slides as $slide) {

				$flattenCrud = $this->routeManager->getFlattenRoute('slide/subject');
				$menu = new Menu();
				$menu->setTemplate("UneakFlatSkinBundle:Block:Menu/block_menu.html.twig");
				$menu->setMeta('style', 'social-links');
				$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('slide' => $slide->getId()) );
				$menu->setRoot($root);

				$panelPeople = new PanelPeople();
				$panelPeople->setTitle($slide->getLabel());
				$panelPeople->setDescription($this->_resume($slide->getDescription(), 50));
				$panelPeople->setPhoto($this->vichHelper->asset($slide, 'imageFile'));
				$panelPeople->setMenu($menu);

				$panelPeoples->addPanel($panelPeople);
			}

			return $panelPeoples;

		}



		public function getLastUpdatedNews($nb) {

			$newsRepo = $this->em->getRepository('NewsBundle:News');
			$news = $newsRepo->findLastUpdatedNews($nb);

			$panelPeoples = new PanelPeoples();
			$panelPeoples->setTitle("<i class='fa fa-newspaper-o'></i> <span>Articles récement modifié</span>");

			$menu = new Menu();
			$menu->setTemplate("UneakFlatSkinBundle:Block:Menu/block_menu.html.twig");
			$menu->setMeta('style', 'nav nav-pills nav-stacked');

			$root = $this->menuHelper->getFactory()->createItem('root');
			$fRoute = $this->routeManager->getFlattenRoute('news');
			$itemSection = $this->menuHelper->createItem($fRoute->getChild('index'));
			$itemSection->setIcon($fRoute->getMetaData("_icon"));
			$itemSection->setLabel(" Voir tout les articles");
			$root->addChild($itemSection);

			$menu->setRoot($root);
			$panelPeoples->addMenu($menu);

			foreach ($news as $article) {

				$flattenCrud = $this->routeManager->getFlattenRoute('news/subject');
				$menu = new Menu();
				$menu->setTemplate("UneakFlatSkinBundle:Block:Menu/block_menu.html.twig");
				$menu->setMeta('style', 'social-links');
				$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('news' => $article->getId()) );
				$menu->setRoot($root);

				$panelPeople = new PanelPeople();
				$panelPeople->setTitle($article->getLabel());
				$panelPeople->setSubtitle($article->getCategory()->getLabel());
				$panelPeople->setDescription($this->_resume($article->getDescription(), 50));
				$panelPeople->setPhoto($this->vichHelper->asset($article, 'imageFile'));
				$panelPeople->setMenu($menu);

				$panelPeoples->addPanel($panelPeople);
			}

			return $panelPeoples;

		}


		private function _resume($text, $max = 100, $endStr = '...') {
			$text = strip_tags($text);
			if (strlen($text) >= $max) {
				$text = substr($text, 0, $max);
				$espace = strrpos($text, " ");
				$text = substr($text, 0, $espace)." ".$endStr;
			}
			return $text;
		}

		public function getConfiguration($title, $description, $links) {

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($title)
				->setDescription($description)
				->setColor("grey")
				->setHeaderAlign('vertical');

			$root = $this->menuHelper->getFactory()->createItem('root');

			foreach ($links as $link) {
				$fRoute = $this->routeManager->getFlattenRoute($link['reference']);
				$itemSection = $this->menuHelper->createItem($fRoute->getChild($link['child']));
				$itemSection->setIcon($fRoute->getMetaData("_icon"));
				$itemSection->setLabel($fRoute->getMetaData("_label"));
				$root->addChild($itemSection);
			}

			$menu = new Menu();
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			$profileNav->addBlock($menu);

			return $profileNav;

		}

	}
