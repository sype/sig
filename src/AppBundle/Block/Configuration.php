<?php

	namespace AppBundle\Block;


	use Doctrine\ORM\EntityManager;
	use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
	use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\ProfileNav;
	use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

	class Configuration {

		private $blockManager;
		private $routeManager;
		private $menuHelper;
		private $vichHelper;
		private $autorization;
		private $em;

		public function __construct(Block $blockManager, FlattenRouteManager $routeManager, MenuHelper $menuHelper, UploaderHelper $vichHelper, AuthorizationChecker $autorization, EntityManager $em) {
			$this->blockManager = $blockManager;
			$this->routeManager = $routeManager;
			$this->menuHelper = $menuHelper;
			$this->vichHelper = $vichHelper;
			$this->autorization = $autorization;
			$this->em = $em;
		}


		public function getConfiguration($title, $description, $links) {

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($title)
				->setDescription($description)
				->setColor("grey")
				->setHeaderAlign('vertical');

			$root = $this->menuHelper->getFactory()->createItem('root');

			foreach ($links as $link) {
				$fRoute = $this->routeManager->getFlattenRoute($link['reference']);
				$itemSection = $this->menuHelper->createItem($fRoute->getChild($link['child']));
				$itemSection->setIcon($fRoute->getMetaData("_icon"));
				$itemSection->setLabel($fRoute->getMetaData("_label"));
				$root->addChild($itemSection);
			}

			$menu = new Menu();
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			$profileNav->addBlock($menu);

			return $profileNav;

		}

	}
