<?php

/*
 * This file is part of sonata-project.
 *
 * (c) 2010 Thomas Rabaix
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Twig\Extension;

class MathExtension extends \Twig_Extension {

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('ceil', array($this, 'ceilFilter')),
        );
    }

    public function ceilFilter($number) {
        return ceil($number);
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return 'math_extension';
    }

}
