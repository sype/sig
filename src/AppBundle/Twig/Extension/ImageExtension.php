<?php

/*
 * This file is part of sonata-project.
 *
 * (c) 2010 Thomas Rabaix
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Twig\Extension;

class ImageExtension extends \Twig_Extension {

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('resolution', array($this, 'resolutionFilter')),
            new \Twig_SimpleFilter('resolutionWidth', array($this, 'resolutionWidthFilter')),
            new \Twig_SimpleFilter('resolutionHeight', array($this, 'resolutionHeightFilter')),
        );
    }

    public function resolutionFilter($path) {
        list($width, $height) = getimagesize($_SERVER['DOCUMENT_ROOT'].$path);
        return $width."x".$height;
    }

    public function resolutionWidthFilter($path) {
        list($width, $height) = getimagesize($_SERVER['DOCUMENT_ROOT'].$path);
        return $width;
    }

    public function resolutionHeightFilter($path) {
        list($width, $height) = getimagesize($_SERVER['DOCUMENT_ROOT'].$path);
        return $height;
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return 'image_extension';
    }

}
