$(document).ready(function () {

  $('.slider-home .slider').slick({
    arrows: false,
    accessibility: false,
    adaptiveHeight: false,
    dots: true,
    autoplay : true,
    pauseOnDotsHover: false
  });

    $('.home-video .slider').slick({
        arrows: false,
        accessibility: false,
        adaptiveHeight: true,
        dots: true,
        autoplay : false,
        infinite: false,
        pauseOnDotsHover: false,
        onAfterChange : function() {
            $('.yvideo').each(function(){
                $(this).stopVideo();
            });
        }
    });


    $('.slider-proposition').slick({
        arrows: false,
        dots: true,
        infinite: true,
        speed: 300,

        accessibility: false,
        adaptiveHeight: true,
        autoplay : true,
        pauseOnDotsHover: true,

        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [
            {
                breakpoint: 480,
                dots: true,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]





    });


  /*Back to Top*/
  $('#back-to-top').on('click', function (e) {
    e.preventDefault();
    $('html,body').animate({
      scrollTop: 0
    }, 700);
  });
});
