<?php

	namespace AppBundle\Controller;

	use AppBundle\Form\SearchType;
    use CollaborateurBundle\Entity\Collaborateur;
    use ContactBundle\Entity\Contact;
    use ContactBundle\Entity\ContactService;
    use ContactBundle\Form\ContactType;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;

    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;

    class DefaultController extends Controller {

        /**
         * @Route("/recherche", name="search")
         */
        public function searchAction(Request $request) {

            $pages = null;
            $news = null;
            $projets = null;
            $agences = null;

            $searchData = array(
                'type' => array('page', 'news', 'agence', 'projet'),
                'content' => null,
                'departement' => null,
                'commune' => null,
                'section' => null,
                'lot' => null,
                'year' => null,
            );

            $form = $this->createForm(new SearchType(), $searchData);


            if ($request && $request->getMethod() == 'POST') {

                $form->handleRequest($request);

                if ($form->isValid()) {

                    $data = $form->getData();
                    $em = $this->getDoctrine();

                    if (in_array('page', $data['type'])) {
                        $pages = $em->getRepository('NavigationBundle:Page')->search($data);
                    }

                    if (in_array('news', $data['type'])) {
                        $news = $em->getRepository('NewsBundle:News')->search($data);
                    }

                    if (in_array('projet', $data['type'])) {
                        $projets = $em->getRepository('ProjetBundle:Projet')->search($data);
                    }

                    if (in_array('agence', $data['type'])) {
                        $agences = $em->getRepository('AgenceBundle:Agence')->search($data);
                    }

                }
            }

            $formManager = $this->get("uneak.formsmanager");
            $formView = $formManager->createView($form);



            return $this->render('default/search.html.twig', array(
                'r_pages' => $pages,
                'r_news' => $news,
                'r_projets' => $projets,
                'r_agences' => $agences,
                'form' => $formView,
            ));
        }



		/**
		 * @Route("/", name="homepage")
		 */
		public function indexAction() {

            $em = $this->getDoctrine();
            $news = $em->getRepository('NewsBundle:News')->findLatestNews(true, 'blog-category', 3);
            $propositions = $em->getRepository('PropositionBundle:Proposition')->findProposition(true);
            $slider = $em->getRepository('SliderBundle:Slide')->findSlidesBySlug(true, 'site-home');
            $videos = $em->getRepository('VideosBundle:Video')->findVideosBySlug(true, 'site-video');
            $lotTypes = $em->getRepository('LotBundle:LotType')->findLotTypes(true);

            $homepage = $em->getRepository('HomepageBundle:Homepage')->findHomepage();

            $contactFormRender = $this->contactAction()->getContent();


            $ggmapData = array();
            $vichHelper = $this->get("vich_uploader.templating.helper.uploader_helper");
            $cacheManager = $this->container->get('liip_imagine.cache.manager');

            $projets = $em->getRepository('ProjetBundle:Projet')->findProjets(true);

            foreach ($projets as $projet) {

                $data = array(
                    'label' => $projet->getLabel(),
                    'location' => $projet->getLocation(),
                    'cp' => $projet->getSection()->getCommune()->getCp(),
                    'url' => $this->generateUrl('projet', array('slug' => $projet->getSlug())),
                );

                if ($projet->getImage()) {
                    $data['image'] = $cacheManager->getBrowserPath($vichHelper->asset($projet, 'imageFile'), 'google_map_thumbs');
                }

                $ggmapData[] = $data;

            }

            $ggmap = json_encode($ggmapData);

            $communes = $em->getRepository('ProjetBundle:Projet')->findCommunesProjets(true);




			return $this->render('default/homepage.html.twig', array(
                'news' => $news,
                'propositions' => $propositions,
                'slider' => $slider,
                'videos' => $videos,
                'lotTypes' => $lotTypes,
                'contactFormRender' => $contactFormRender,
                'ggmap' => $ggmap,
                'communes' => $communes,
                'homepage' => $homepage
            ));
		}


        /**
         * @Route("/lot/type/{slug}", name="lot_type")
         */
        public function lottypeAction($slug) {

            $em = $this->getDoctrine();
            $type = $em->getRepository('LotBundle:LotType')->findLotTypeBySlug(true, $slug);
            $projets = $em->getRepository('ProjetBundle:ProjetLot')->findProjetLotByType(true, $type);

            return $this->render('default/lot_type.html.twig', array(
                'type' => $type,
                'projets' => $projets,
            ));
        }

        /**
         * @Route("/page/{slug}", name="page")
         */
        public function pageAction($slug) {

            $em = $this->getDoctrine();
            $page = $em->getRepository('NavigationBundle:Page')->findPageBySlug(true, $slug);

            return $this->render('default/page.html.twig', array(
                'page' => $page,
            ));
        }




        /**
         * @Route("/agence", name="agence_list")
         */
        public function agencesAction() {

            $em = $this->getDoctrine();
            $agences = $em->getRepository('AgenceBundle:Agence')->findAgences(true);

            return $this->render('default/agences.html.twig', array(
                'agences' => $agences,
            ));
        }

        /**
         * @Route("/agence/{slug}", name="agence")
         */
        public function agenceAction($slug) {

            $em = $this->getDoctrine();
            $agence = $em->getRepository('AgenceBundle:Agence')->findAgenceBySlug(true, $slug);
            $collaborateurs = $em->getRepository('AgenceBundle:AgenceCollaborateur')->findCollaborateurs(true, $agence);
            $projets = $em->getRepository('ProjetBundle:Projet')->findProjets(true, $agence);

            return $this->render('default/agence.html.twig', array(
                'agence' => $agence,
                'collaborateurs' => $collaborateurs,
                'projets' => $projets,
            ));
        }


        /**
         * @Route("/projet", name="projet_list")
         */
        public function projetsAction() {

            $em = $this->getDoctrine();
            $projets = $em->getRepository('ProjetBundle:Projet')->findProjets(true);

            return $this->render('default/projets.html.twig', array(
                'projets' => $projets,
            ));
        }

        /**
         * @Route("/projet/{slug}", name="projet")
         */
        public function projetAction($slug) {

            $em = $this->getDoctrine();
            $projet = $em->getRepository('ProjetBundle:Projet')->findProjetBySlug(true, $slug);

            return $this->render('default/projet.html.twig', array(
                'projet' => $projet,
            ));
        }


        /**
         * @Route("/projet/{slug}/{lot}", name="projet_lot")
         */
        public function projetLotAction($slug, $lot) {

            $em = $this->getDoctrine();
            $projetLot = $em->getRepository('ProjetBundle:ProjetLot')->findProjetLotBySlug(true, $slug, $lot);

            return $this->render('default/projet_lot.html.twig', array(
                'projetLot' => $projetLot,
            ));
        }


        /**
         * @Route("/article", name="news_list")
         */
        public function articlesAction() {

            $em = $this->getDoctrine();
            $newsCategories = $em->getRepository('NewsBundle:NewsCategory')->findNewsCategory(true);

            return $this->render('default/articles.html.twig', array(
                'newsCategories' => $newsCategories,
            ));
        }

        /**
         * @Route("/article/{slug}", name="news")
         */
        public function articleAction($slug) {

            $em = $this->getDoctrine();
            $news = $em->getRepository('NewsBundle:News')->findNewsBySlug(true, $slug);

            return $this->render('default/article.html.twig', array(
                'news' => $news,
            ));
        }

        /**
         * @Route("/article/categorie/{slug}", name="newscategorie")
         */
        public function newsCategorieAction($slug) {

            $em = $this->getDoctrine();
            $categorie = $em->getRepository('NewsBundle:NewsCategory')->findNewsCategoryBySlug(true, $slug);
            $news = $em->getRepository('NewsBundle:News')->findNewsByCategory(true, $categorie);

            return $this->render('default/news_categorie.html.twig', array(
                'categorie' => $categorie,
                'news' => $news,
            ));
        }


        /**
         * @Route("/galerie", name="gallery_list")
         */
        public function galeriesAction() {

            $em = $this->getDoctrine();
            $galerieTypes = $em->getRepository('GalleryBundle:GalleryType')->findGalleryTypes(true);

            return $this->render('default/galeries.html.twig', array(
                'galerieTypes' => $galerieTypes,
            ));
        }


        /**
         * @Route("/galerie/{slug}", name="gallery")
         */
        public function galerieAction($slug) {

            $em = $this->getDoctrine();
            $galerie = $em->getRepository('GalleryBundle:Gallery')->findGalleryBySlug(true, $slug);

            return $this->render('default/galerie.html.twig', array(
                'galerie' => $galerie,
            ));
        }


        /**
         * @Route("/galerie/type/{slug}", name="gallery_type")
         */
        public function galerieTypeAction($slug) {

            $em = $this->getDoctrine();
            $type = $em->getRepository('GalleryBundle:GalleryType')->findGalleryTypeBySlug(true, $slug);
            $galeries = $em->getRepository('GalleryBundle:Gallery')->findGalleryByType(true, $type);

            return $this->render('default/galerie_type.html.twig', array(
                'type' => $type,
                'galeries' => $galeries,
            ));
        }



        /**
         * @Route("/_projets", name="home_ggm_projets")
         */
        public function ggmProjetsAction(Request $request) {
            $data = array();
            if ($request->isXmlHttpRequest()) {
                $vichHelper = $this->get("vich_uploader.templating.helper.uploader_helper");
                $em = $this->getDoctrine()->getManager();
                $projets = $em->getRepository('ProjetBundle:Projet')->findProjets(true);

                foreach ($projets as $projet) {
                    $data[] = array(
                        'label' => $projet->getLabel(),
                        'location' => $projet->getLocation(),
                        'cp' => $projet->getSection()->getCommune()->getCp(),
                        'url' => $this->generateUrl('projet', array('slug' => $projet->getSlug())),
                        'image' => $vichHelper->asset($projet, 'imageFile'),
                    );
                }

            }
            return new JsonResponse($data);
        }



        /**
         * @Route("/_contact", name="home_contact_form")
         */
        public function contactAction() {

            $request = $this->get('request');
            $contact = new Contact();

            $form = $this->createForm(new ContactType(), $contact);

            if ($request && $request->getMethod() == 'POST') {

                $flash = $this->get('braincrafted_bootstrap.flash');
                $form->handleRequest($request);

                if ($request->isXmlHttpRequest() && $form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($contact);
                    $em->flush();



                    /** @var $service ContactService */
                    $service = $contact->getService();
                    $collaborateurs = $service->getCollaborateurs();
                    $recipient = array();
                    /** @var $collaborateur Collaborateur */
                    foreach ($collaborateurs as $collaborateur) {
                        $recipient[$collaborateur->getEmail()] = $collaborateur->getFirstName()." ".$collaborateur->getLastName();
                    }


                    // TODO
                    // envoi du mail

                    $message = \Swift_Message::newInstance()
                        ->setSubject('Nouvel email de sig-guadeloupe.fr')
                        ->setFrom('contact@sig-guadeloupe.fr')
                        ->setTo($recipient)
                        ->setBody(
                            $this->renderView(
                                'Partials/email.html.twig',
                                array('contact' => $contact)
                            ),
                            'text/html'
                        )
                        /*
						 * If you also want to include a plaintext version of the message
						->addPart(
							$this->renderView(
								'Emails/registration.txt.twig',
								array('name' => $name)
							),
							'text/plain'
						)
						*/
                    ;
                    $this->get('mailer')->send($message);





                    $flash->info('Votre message a bien été envoyé.');
                } else {
                    $flash->error('Votre formulaire est invalide.');
                }
            }


            $formManager = $this->get("uneak.formsmanager");
            $formView = $formManager->createView($form);


            return $this->render('Partials/contact_form.html.twig', array(
                'contactForm' => $form->createView(),
            ));
        }


        public function leftMenuAction($slug) {

            $em = $this->getDoctrine();
            $menu = $em->getRepository('NavigationBundle:MenuItem')->findMenuByPathSlug(true, $slug);

            return $this->render('Partials/left_menu.html.twig', array(
                'menu' => $menu,
                'slug' => $slug
            ));
        }


        public function topMenuAction($slug) {

            $em = $this->getDoctrine();
            $menu = $em->getRepository('NavigationBundle:MenuItem')->findMenuByPathSlug(true, $slug);

            return $this->render('Partials/top_menu.html.twig', array(
                'menu' => $menu,
                'slug' => $slug
            ));
        }


        public function homeMenuAction($slug) {

            $em = $this->getDoctrine();
            $menu = $em->getRepository('NavigationBundle:MenuItem')->findMenuByPathSlug(true, $slug);

            return $this->render('Partials/home_menu.html.twig', array(
                'menu' => $menu,
                'slug' => $slug
            ));
        }


        public function menuAction($slug, $itemTemplate) {

            $em = $this->getDoctrine();
            $menu = $em->getRepository('NavigationBundle:MenuItem')->findMenuByPathSlug(true, $slug);

            return $this->render('Partials/menu.html.twig', array(
                'menu' => $menu,
                'slug' => $slug,
                'itemTemplate' => $itemTemplate,
            ));
        }


        public function menuFooterAction($slug, $itemTemplate) {

            $menu = array();
            $em = $this->getDoctrine();
            $menusLvl1 = $em->getRepository('NavigationBundle:MenuItem')->findMenuByPathSlug(true, $slug, false);

            foreach ($menusLvl1 as $menuLvl1) {
                $menu[$menuLvl1->getPathSlug()]['main'] = $menuLvl1;
                $menusLvl2 = $em->getRepository('NavigationBundle:MenuItem')->findMenuByPathSlug(true, $menuLvl1->getPathSlug(), false);
                $menu[$menuLvl1->getPathSlug()]['children'] = $menusLvl2;
            }

            if (count($menusLvl1)) {
                $colSize = ceil(12/count($menusLvl1));
            } else {
                $colSize = 12;
            }

            return $this->render('Partials/menu_footer.html.twig', array(
                'menu' => $menu,
                'colSize' => $colSize,
                'slug' => $slug,
                'itemTemplate' => $itemTemplate,
            ));
        }

	}
