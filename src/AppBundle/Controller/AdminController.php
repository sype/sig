<?php

	namespace AppBundle\Controller;

	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\ProfileNav;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoute;
	use Doctrine\ORM\Query\Expr;

	class AdminController extends Controller {


		public function indexAction(FlattenRoute $route) {
			$blockManager = $this->get("uneak.blocksmanager");
			$dashboardBlock = $this->get('uneak.admin.dashboard.block.helper');

			$blockManager->addBlock($dashboardBlock->getSlides(), null, 50, 'config_left');
			$blockManager->addBlock($dashboardBlock->getPropositions(), null, 25, 'config_left');
			$blockManager->addBlock($dashboardBlock->getLastUpdatedNews(2), null, 0, 'config_left');

			return $this->render('default/dashboard.html.twig');

		}

		public function configurationAction(FlattenRoute $route) {
			$blockManager = $this->get("uneak.blocksmanager");
			$configurationBlock = $this->get('uneak.admin.configuration.block.helper');

			$links = array();
			$links[] = array('reference' => 'departement', 'child' => 'index');
			$links[] = array('reference' => 'commune', 'child' => 'index');
			$links[] = array('reference' => 'section', 'child' => 'index');
			$blockManager->addBlock($configurationBlock->getConfiguration("Localisation", "Configuration des locs", $links), null, 0, 'config_right');

			$links = array();
			$links[] = array('reference' => 'user', 'child' => 'index');
			$links[] = array('reference' => 'collaborateur', 'child' => 'index');
			$blockManager->addBlock($configurationBlock->getConfiguration("Utilisateurs", "Configuration des gens", $links), null, 0, 'config_right');

			$links = array();
			$links[] = array('reference' => 'agence', 'child' => 'index');
			$links[] = array('reference' => 'poste', 'child' => 'index');
			$blockManager->addBlock($configurationBlock->getConfiguration("Agences", "Configuration des agences", $links), null, 0, 'config_middle');

			$links = array();
			$links[] = array('reference' => 'lottype', 'child' => 'index');
			$links[] = array('reference' => 'lot', 'child' => 'index');
			$links[] = array('reference' => 'projet', 'child' => 'index');
			$blockManager->addBlock($configurationBlock->getConfiguration("Projets", "Configuration des projets", $links), null, 0, 'config_middle');

			$links = array();
			$links[] = array('reference' => 'contact', 'child' => 'index');
			$links[] = array('reference' => 'contactservice', 'child' => 'index');
			$blockManager->addBlock($configurationBlock->getConfiguration("Contacts", "Configuration des contacts", $links), null, 0, 'config_right');

			$links = array();
			$links[] = array('reference' => 'gallerytype', 'child' => 'index');
			$links[] = array('reference' => 'gallery', 'child' => 'index');
			$blockManager->addBlock($configurationBlock->getConfiguration("Galeries", "Gestion des galeries", $links), null, 0, 'config_left');

			$links = array();
			$links[] = array('reference' => 'newscategory', 'child' => 'index');
			$links[] = array('reference' => 'news', 'child' => 'index');
			$blockManager->addBlock($configurationBlock->getConfiguration("Articles", "Gestion des articles", $links), null, 0, 'config_left');

			$links = array();
			$links[] = array('reference' => 'menu', 'child' => 'index');
			$blockManager->addBlock($configurationBlock->getConfiguration("Menu", "Gestion des menu", $links), null, 0, 'config_middle');

			$links = array();
			$links[] = array('reference' => 'page', 'child' => 'index');
			$links[] = array('reference' => 'proposition', 'child' => 'index');
			$links[] = array('reference' => 'slide', 'child' => 'index');
			$links[] = array('reference' => 'video', 'child' => 'index');
			$links[] = array('reference' => 'homepage', 'child' => 'edit');
			$blockManager->addBlock($configurationBlock->getConfiguration("Contenu", "Gestion du contenu", $links), null, 0, 'config_left');

			return $this->render('default/configuration.html.twig');

		}


	}
