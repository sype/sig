<?php

namespace CollaborateurBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Collaborateur
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="CollaborateurBundle\Entity\CollaborateurRepository")
 * @Vich\Uploadable
 */
class Collaborateur {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var datetime $created
	 *
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var datetime $updated
	 *
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(type="datetime")
	 */
	protected $updated;

	/**
	 * @ORM\Column(name="active", type="boolean")
	 */
	protected $active = true;


    /**
     * @ORM\Column(name="firstName", type="string", length=64, nullable=false)
     * @Assert\NotBlank()
     */
    protected $firstName;

    /**
     * @ORM\Column(name="lastName", type="string", length=64, nullable=false)
     * @Assert\NotBlank()
     */
    protected $lastName;


    /**
     * @ORM\Column(name="gender", type="string", length=64, nullable=false)
     * @Assert\NotNull()
     */
    protected $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    protected $image;

    /**
     * @var File $imageFile
     * @Vich\UploadableField(mapping="collaborateur_image", fileNameProperty="image")
     */
    protected $imageFile;


    /**
     * @ORM\Column(name="phone", type="string", length=10, nullable=true)
     * @Assert\Regex(
     *     pattern="/^\d{10}$/",
     *     message="Le numéro de téléphone doit obligatoirement contenir 10 chiffres"
     * )
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\Email(
     *     message = "'{{ value }}' n'est pas un email valide.",
     *
     * )
     */
    protected $email;



	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set created
	 *
	 * @param \DateTime $created
	 * @return Publish
	 */
	public function setCreated($created) {
		$this->created = $created;

		return $this;
	}

	/**
	 * Get created
	 *
	 * @return \DateTime
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Set updated
	 *
	 * @param \DateTime $updated
	 * @return Publish
	 */
	public function setUpdated($updated) {
		$this->updated = $updated;

		return $this;
	}

	/**
	 * Get updated
	 *
	 * @return \DateTime
	 */
	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * Set active
	 *
	 * @param boolean $active
	 * @return Publish
	 */
	public function setActive($active) {
		$this->active = $active;

		return $this;
	}

	/**
	 * Get active
	 *
	 * @return boolean
	 */
	public function getActive() {
		return $this->active;
	}



    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    public function getPhone() {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }



    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image) {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile() {
        return $this->imageFile;
    }

    /**
     * Set image path
     *
     * @param string $image
     *
     * @return Modele
     */
    public function setImage($image = null) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image path
     *
     * @return string $image
     */
    public function getImage() {
        return $this->image;
    }



    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
        return $this;
    }

    public function getGender() {
        return $this->gender;
    }

    public function setGender($gender) {
        $this->gender = $gender;
        return $this;
    }




    public function __toString() {
        return $this->firstName . " " . $this->lastName;
    }



}
