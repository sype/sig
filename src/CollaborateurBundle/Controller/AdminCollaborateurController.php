<?php

	namespace CollaborateurBundle\Controller;

	use CollaborateurBundle\Entity\Collaborateur;
    use CollaborateurBundle\Entity\Collaborateurs;
    use CollaborateurBundle\Entity\CollaborateursRepository;
    use CollaborateurBundle\Form\CollaborateurType;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoute;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
	use Uneak\FlatSkinBundle\Block\Component\DataTable;
	use Uneak\FlatSkinBundle\Block\DataTable\DataTableFilters;
	use Uneak\FlatSkinBundle\Block\Form\Form;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Doctrine\ORM\Query\Expr;

	class AdminCollaborateurController extends Controller {


		public function indexAction( FlattenRoute $route) {
			$blockManager = $this->get("uneak.blocksmanager");

			$collaborateurBlock = $this->get('uneak.admin.collaborateur.block.helper');
			$blockManager->addBlock($collaborateurBlock->getNavigation(), 'navigation');

			$gridNested = $route->getNestedRoute();

			$datatable = new DataTable();
			$datatable->setAjax($route->getChild('_grid')->getRoutePath());
			$datatable->setColumns($gridNested->getColumns());

			$datatableWrapper = new Wrapper();
			$datatableWrapper
				->setTitle("Resultats")
				->setCollapsable(false)
				->addBlock($datatable);
			$blockManager->addBlock($datatableWrapper, 'datatable');

			$datatableFilterWrapper = new Wrapper();
			$datatableFilterWrapper
				->setTitle("Filtres")
				->setCollapsable(true)
				->addBlock(new DataTableFilters($datatable));
			$blockManager->addBlock($datatableFilterWrapper, 'datatable_filters');

			return $this->render('CollaborateurBundle:Admin:index.html.twig');

		}



		public function showAction($collaborateur) {
			$blockManager = $this->get("uneak.blocksmanager");

			$collaborateurBlock = $this->get('uneak.admin.collaborateur.block.helper');

			$blockManager->addBlock($collaborateurBlock->getInfos($collaborateur), 'infos');
			$blockManager->addBlock($collaborateurBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($collaborateurBlock->getCollaborateurNavigation($collaborateur), 'user_navigation');
			$blockManager->addBlock($collaborateurBlock->getCollaborateur($collaborateur), 'panelCollaborateur');
			$blockManager->addBlock($collaborateurBlock->getProposition($collaborateur), 'panelProposition');
			$blockManager->addBlock($collaborateurBlock->getService($collaborateur), 'panelService');

			return $this->render('CollaborateurBundle:Admin:show.html.twig');

		}


		public function editAction($collaborateur, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");


			$collaborateurBlock = $this->get('uneak.admin.collaborateur.block.helper');
			$blockManager->addBlock($collaborateurBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($collaborateurBlock->getCollaborateurNavigation($collaborateur), 'user_navigation');

			$form = $this->createForm(new CollaborateurType(), $collaborateur);
			$form->add('submit', 'submit', array('label' => 'Modifier'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

					return $this->redirect($route->getChild('*/subject/show', array('collaborateur' => $collaborateur->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("CollaborateurBundle:Block:Form/block_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Edition du collaborateur ".$collaborateur->getFirstName()." ".$collaborateur->getLastName())
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('CollaborateurBundle:Admin:edit.html.twig');

		}


		public function newAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$collaborateurBlock = $this->get('uneak.admin.collaborateur.block.helper');
			$blockManager->addBlock($collaborateurBlock->getNavigation(), 'navigation');

			$collaborateur = new Collaborateur();

			$form = $this->createForm(new CollaborateurType(), $collaborateur);
			$form->add('submit', 'submit', array('label' => 'Creer'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($collaborateur);
                    $em->flush();

					return $this->redirect($route->getChild('*/subject/show', array('collaborateur' => $collaborateur->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("CollaborateurBundle:Block:Form/block_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Création d'un collaborateur")
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');

			return $this->render('CollaborateurBundle:Admin:new.html.twig');


		}


		public function deleteAction($collaborateur, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$collaborateurBlock = $this->get('uneak.admin.collaborateur.block.helper');
			$blockManager->addBlock($collaborateurBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($collaborateurBlock->getCollaborateurNavigation($collaborateur), 'user_navigation');

			$form = $this->createFormBuilder(array());
			$form->add('cancel', 'submit', array('label' => 'Annuler'));
			$form->add('confirm', 'submit', array('label' => 'Confirmer'));
			$form = $form->getForm();


			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');

				$form->handleRequest($request);
				if ($form->isValid()) {
					if ($form->get('cancel')->isClicked()) {
						$flash->info('Suppression annulée');

						return $this->redirect($route->getChild('*/subject/show', array('collaborateur' => $collaborateur->getId()))->getRoutePath());
					}
					if ($form->get('confirm')->isClicked()) {

                        $em = $this->getDoctrine()->getManager();
                        $em->remove($collaborateur);
                        $em->flush();

						$flash->success('La suppression à été réalisée avec succès.');

						return $this->redirect($route->getChild('*/index')->getRoutePath());
					}

				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formBlock = new Form($form->createView());
			$formBlock->setTemplate("CollaborateurBundle:Block:Form/delete_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Suppression du collaborateur ".$collaborateur->getFirstName()." ".$collaborateur->getLastName())
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('CollaborateurBundle:Admin:delete.html.twig');

		}




		public function indexGridAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$gridHelper = $this->get("uneak.routesmanager.grid.helper");
			$menuHelper = $this->get("uneak.routesmanager.menu.helper");

			$params = $request->query->all();

			$gridData = $gridHelper->gridFields($gridHelper->createGridQueryBuilder('CollaborateurBundle\Entity\Collaborateur', $params), $params);
			$recordsTotal = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('CollaborateurBundle\Entity\Collaborateur', $params));
			$recordsFiltered = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('CollaborateurBundle\Entity\Collaborateur', $params));


			$rowMenuTemplate = "UneakFlatSkinBundle:Block:Menu/block_menu.html.twig";

			$data = array();

			foreach ($gridData as $object) {
				$row = array();
				foreach ($params['columns'] as $columns) {
					if ($columns['name'] && substr($columns['name'], 0, 1) != '_') {
						$value = $object[str_replace(".", "_", $columns['name'])];
						if ($value instanceof \DateTime) {
							$value = $value->format('d/m/Y H:m:s');
						}
						$row[$columns['data']] = $value;
					} else {
						$row[$columns['data']] = "";
					}
				}
				$row['DT_RowId'] = $object['DT_RowId'];


				$menu = new Menu();
				$menu->setTemplate($rowMenuTemplate);
				$rowActions = $route->getParent()->getNestedRoute()->getRowActions();
				$root = $menuHelper->createMenu($rowActions, $route, array('collaborateur' => $row['DT_RowId']));
				$root->setChildrenAttribute('class', 'nav nav-pills nav-inline text-nowrap');
				$menu->setRoot($root);

				$blockManager->addBlock($menu, 'rowMenu');
				$row['_actions'] = $this->renderView("{{ renderBlock('rowMenu') }}");

				array_push($data, $row);
			}

			return new JsonResponse(array(
				'draw'            => $params["draw"],
				'recordsTotal'    => $recordsTotal,
				'recordsFiltered' => $recordsFiltered,
				'data'            => $data,
			));
		}


	}
