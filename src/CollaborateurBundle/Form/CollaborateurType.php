<?php

	namespace CollaborateurBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;

	class CollaborateurType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
                ->add('gender', 'gender', array(
                    'label' => "Civilité",
                    'empty_value' => 'Sectionnez votre civilité',
                ))
                ->add('firstName', null, array(
                    'label' => "Prénom",
                ))
                ->add('lastName', null, array(
                    'label' => "Nom",
                ))
                ->add('imageFile', 'vich_file', array(
                        'label'         => "Photo",
                        'required'      => false,
                        'allow_delete'  => true, // not mandatory, default is true
                        'download_link' => true, // not mandatory, default is true
                        'required'      => false,
                    )
                )
                ->add('email', null, array(
                    'label' => "Email",
                ))

                ->add('phone', null, array(
                    'label'    => 'Numéro de téléphone',
                    'attr'     => array(
                        'input_group' => array(
                            'prepend' => '.icon-phone'
                        )
                    )
                ))

				->add('active', null, array(
					'label' => "Activé",
					'required'      => false,
				));

		}



		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'CollaborateurBundle\Entity\Collaborateur',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'collaborateurbundle_collaborateur';
		}
	}
