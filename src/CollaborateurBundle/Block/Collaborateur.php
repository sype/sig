<?php

	namespace CollaborateurBundle\Block;


	use Doctrine\ORM\EntityManager;
	use CollaborateurBundle\Entity\Collaborateur as CollaborateurEntity;
	use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
    use Uneak\FlatSkinBundle\Block\Panel\PanelPeople;
    use Uneak\FlatSkinBundle\Block\Panel\PanelPeoples;
    use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
    use Uneak\FlatSkinBundle\Block\Info\Info;
	use Uneak\FlatSkinBundle\Block\Info\Infos;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\ProfileNav;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

	class Collaborateur {

		private $blockManager;
		private $routeManager;
		private $menuHelper;
		private $vichHelper;
		private $autorization;
		private $em;

		public function __construct(Block $blockManager, FlattenRouteManager $routeManager, MenuHelper $menuHelper, UploaderHelper $vichHelper, AuthorizationChecker $autorization, EntityManager $em) {
			$this->blockManager = $blockManager;
			$this->routeManager = $routeManager;
			$this->menuHelper = $menuHelper;
			$this->vichHelper = $vichHelper;
			$this->autorization = $autorization;
			$this->em = $em;
		}


        public function getService(CollaborateurEntity $collaborateur) {

            $posteRepo = $this->em->getRepository('ContactBundle:ContactService');
            $services = $posteRepo->findServices($collaborateur);

            $panelPeoples = new PanelPeoples();
            $panelPeoples->setTitle("<i class='fa fa-icon fa-suitcase'> Service</i>");
            foreach ($services as $service) {

                $flattenCrud = $this->routeManager->getFlattenRoute('contactservice/subject');
                $menu = new Menu();
                $menu->setTemplate("UneakFlatSkinBundle:Block:Menu/block_menu.html.twig");
                $menu->setMeta('style', 'social-links');
                $root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('contactservice' => $service->getId()) );
                $menu->setRoot($root);

                $panelPeople = new PanelPeople();
                $panelPeople->setTitle($service->getLabel());
                $panelPeople->setMenu($menu);

                $panelPeoples->addPanel($panelPeople);
            }

            return $panelPeoples;

        }



        public function getProposition(CollaborateurEntity $proposition) {

            $posteRepo = $this->em->getRepository('PropositionBundle:Proposition');
            $propositions = $posteRepo->findProposition(false, $proposition);

            $panelPeoples = new PanelPeoples();
            $panelPeoples->setTitle("<i class='fa fa-icon fa-star'> Propositions du mois</i>");
            foreach ($propositions as $proposition) {

                $flattenCrud = $this->routeManager->getFlattenRoute('proposition/subject');
                $menu = new Menu();
                $menu->setTemplate("UneakFlatSkinBundle:Block:Menu/block_menu.html.twig");
                $menu->setMeta('style', 'social-links');
                $root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('proposition' => $proposition->getId()) );
                $menu->setRoot($root);

                $panelPeople = new PanelPeople();
                $panelPeople->setTitle($proposition->getLabel());
                $panelPeople->setDescription($proposition->getDescription());
                $panelPeople->setPhoto($this->vichHelper->asset($proposition, 'imageFile'));
                $panelPeople->setMenu($menu);

                $panelPeoples->addPanel($panelPeople);
            }

            return $panelPeoples;

        }


        public function getCollaborateur(CollaborateurEntity $collaborateur) {

            $posteRepo = $this->em->getRepository('AgenceBundle:AgenceCollaborateur');
            $collaborateurs = $posteRepo->findCollaborateurs(false, null, null, $collaborateur);

            $panelPeoples = new PanelPeoples();
            $panelPeoples->setTitle("<i class='fa fa-icon fa-briefcase'> Poste d'agence</i>");
            foreach ($collaborateurs as $collaborateur) {

                $flattenCrud = $this->routeManager->getFlattenRoute('agence/subject/collaborateur/subject');
                $menu = new Menu();
                $menu->setTemplate("UneakFlatSkinBundle:Block:Menu/block_menu.html.twig");
                $menu->setMeta('style', 'social-links');
                $root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('agence' => $collaborateur->getAgence()->getId(), 'collaborateur' => $collaborateur->getId()) );
                $menu->setRoot($root);

                $panelPeople = new PanelPeople();
                $panelPeople->setTitle($collaborateur->getPoste()->getLabel());
                $panelPeople->setSubtitle($collaborateur->getAgence()->getLabel());
                $panelPeople->setPhoto($this->vichHelper->asset($collaborateur->getCollaborateur(), 'imageFile'));
                $panelPeople->setMenu($menu);

                $panelPeoples->addPanel($panelPeople);
            }

            return $panelPeoples;

        }


        public function getInfos(CollaborateurEntity $collaborateur) {

			$infos = new Infos();
			$infos
				->setColumns(1)
				->setStripeRow(true)
                ->addBlock(new Info("Civilité", $collaborateur->getGender()), null, 60)
                ->addBlock(new Info("Prénom", $collaborateur->getFirstName()), null, 50)
                ->addBlock(new Info("Nom", $collaborateur->getLastName()), null, 40)
                ->addBlock(new Info("Email", $collaborateur->getEmail()), null, 30)
                ->addBlock(new Info("Téléphone", $collaborateur->getPhone()), null, 20)
                ->addBlock(new Info("Activé", ($collaborateur->getActive()) ? "Oui" : "Non"), null, 0);

			$wrapper = new Wrapper();
			$wrapper
				->setTitle("Informations")
				->addBlock($infos);

			return $wrapper;
		}



		public function getNavigation() {

			$flattenCrud = $this->routeManager->getFlattenRoute('collaborateur');

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($flattenCrud->getMetaData("_label"))
				->setDescription($flattenCrud->getMetaData("_description"))
				->setColor("green")
				->setHeaderAlign('vertical');
			$profileNav->addBlock($this->getNavigationMenu());

			return $profileNav;

		}

		public function getNavigationMenu() {

			$flattenCrud = $this->routeManager->getFlattenRoute('collaborateur');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud);
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}

		public function getCollaborateurNavigation(CollaborateurEntity $collaborateur) {

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($collaborateur->getFirstName()." ".$collaborateur->getLastName())
				->setDescription($collaborateur->getEmail())
				->setColor("green")
                ->setPhoto($this->vichHelper->asset($collaborateur, 'imageFile'))
				->setHeaderAlign('horizontal');
			$profileNav->addBlock($this->getCollaborateurNavigationMenu($collaborateur));

			return $profileNav;

		}

		public function getCollaborateurNavigationMenu(CollaborateurEntity $collaborateur) {

			$flattenCrud = $this->routeManager->getFlattenRoute('collaborateur/subject');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('collaborateur' => $collaborateur->getId()));
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}



	}
