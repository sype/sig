<?php

	namespace PropositionBundle\Form;

	use Doctrine\ORM\EntityRepository;
	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;

	class PropositionType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('label', null, array(
					'label' => "Titre",
				))

				->add('description', 'ckeditor', array(
					'label' => "Description",
                    'options' => array(
                        'customConfig' => '../../../bundles/app/js/ckeditor_config.js',
                    ),
				))

				->add('collaborateur', 'entity_select2', array(
					'label' => "Collaborateur",
					'class' => 'CollaborateurBundle:Collaborateur',
					'query_builder'   => function (EntityRepository $er) {
						return $er->getCollaborateurQuery();
					},
					'options' => array(
						'placeholder' => "Sectionnez le collaborateur",
						'language' => 'fr'
					),
					'multiple'  => false,

					'empty_value' => 'Sectionnez le collaborateur',
					'required' => false,
				))

				->add('route', 'routes', array(
					'label' => "Route",
					'required' => false,
				))

				->add('imageFile', 'vich_file', array(
						'label'         => "Photo",
						'required'      => false,
						'allow_delete'  => true, // not mandatory, default is true
						'download_link' => true, // not mandatory, default is true
						'required'      => false,
					)
				)

				->add('active', null, array(
					'label' => "Activé",
					'required'      => false,
				));

		}



		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'PropositionBundle\Entity\Proposition',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'propositionbundle_proposition';
		}
	}
