<?php

	namespace VideosBundle\Entity;

	use Doctrine\ORM\Mapping as ORM;
	use Gedmo\Mapping\Annotation as Gedmo;
    use Symfony\Component\Validator\Constraints\NotBlank;

	/**
	 * Video
	 *
	 * @ORM\Table()
	 * @ORM\Entity(repositoryClass="VideosBundle\Entity\VideoRepository")
	 */
	class Video {

		/**
		 * @var integer
		 *
		 * @ORM\Column(name="id", type="integer")
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="AUTO")
		 */
		protected $id;

		/**
		 * @ORM\Column(name="label", type="string", length=64, nullable=false)
         * @NotBlank()
		 */
		protected $label;

		/**
		 * @ORM\Column(name="description", type="text", nullable=false)
         * @NotBlank()
		 */
		protected $description;


        /**
         * @ORM\ManyToOne(targetEntity="VideosBundle\Entity\Videos", inversedBy="videos", cascade={"persist"})
         * @ORM\JoinColumn(name="videos_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
         */
        protected $videos;

        /**
         * @ORM\Column(name="video", type="string", length=128, nullable=false)
         * @NotBlank()
         */
		protected $video;


		/**
		 * @var datetime $created
		 *
		 * @Gedmo\Timestampable(on="create")
		 * @ORM\Column(type="datetime")
		 */
		protected $created;

		/**
		 * @var datetime $updated
		 *
		 * @Gedmo\Timestampable(on="update")
		 * @ORM\Column(type="datetime")
		 */
		protected $updated;

		/**
		 * @ORM\Column(name="active", type="boolean")
		 */
		protected $active = true;



		public function __toString() {
			return $this->label;
		}



		/**
		 * Get id
		 *
		 * @return integer
		 */
		public function getId() {
			return $this->id;
		}

		/**
		 * Set created
		 *
		 * @param \DateTime $created
		 *
		 * @return Publish
		 */
		public function setCreated($created) {
			$this->created = $created;

			return $this;
		}

		/**
		 * Get created
		 *
		 * @return \DateTime
		 */
		public function getCreated() {
			return $this->created;
		}

		/**
		 * Set updated
		 *
		 * @param \DateTime $updated
		 *
		 * @return Publish
		 */
		public function setUpdated($updated) {
			$this->updated = $updated;

			return $this;
		}

		/**
		 * Get updated
		 *
		 * @return \DateTime
		 */
		public function getUpdated() {
			return $this->updated;
		}

		/**
		 * Set active
		 *
		 * @param boolean $active
		 *
		 * @return Publish
		 */
		public function setActive($active) {
			$this->active = $active;

			return $this;
		}

		/**
		 * Get active
		 *
		 * @return boolean
		 */
		public function getActive() {
			return $this->active;
		}

		/**
		 * Set label
		 *
		 * @param string $label
		 *
		 * @return MenuItem
		 */
		public function setLabel($label) {
			$this->label = $label;

			return $this;
		}

		/**
		 * Get label
		 *
		 * @return string
		 */
		public function getLabel() {
			return $this->label;
		}


		/**
		 * @return mixed
		 */
		public function getVideo() {
			return $this->video;
		}

		/**
		 * @param mixed $video
		 */
		public function setVideo($video) {
			$this->video = $video;
		}

		/**
		 * @return mixed
		 */
		public function getDescription() {
			return $this->description;
		}

		/**
		 * @param mixed $description
		 */
		public function setDescription($description) {
			$this->description = $description;
		}


        /**
         * Set videos
         *
         * @param \VideosBundle\Entity\Videos
         * @return Video
         */
        public function setVideos(\VideosBundle\Entity\Videos $videos = null) {
            $this->videos = $videos;
            return $this;
        }

        /**
         * Get videos
         *
         * @return \VideosBundle\Entity\Videos
         */
        public function getVideos() {
            return $this->videos;
        }

	}
