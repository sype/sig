<?php

namespace VideosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Videos
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VideosBundle\Entity\VideosRepository")
 */
class Videos {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\Column(name="label", type="string", length=64)
	 */
	protected $label;

    /**
     * @ORM\Column(name="slug", type="string", length=64, unique=false, nullable=true)
     * @Gedmo\Slug(fields={"label"}, updatable=false, unique=true)
     *
     */
    protected $slug;

    /**
     * @ORM\Column(name="private", type="boolean")
     */
    protected $private = false;


    /**
     *
     * @ORM\OneToMany(targetEntity="VideosBundle\Entity\Video", mappedBy="videos", cascade={"persist", "remove"})
     */
    protected $videos;

	/**
	 * @var datetime $created
	 *
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var datetime $updated
	 *
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(type="datetime")
	 */
	protected $updated;

	/**
	 * @ORM\Column(name="active", type="boolean")
	 */
	protected $active = true;



    /**
     * Constructor
     */
    public function __construct() {
        $this->videos = new \Doctrine\Common\Collections\ArrayCollection();
    }



    /**
     * Add video
     *
     * @param \VideosBundle\Entity\Video $video
     * @return Videos
     */
    public function addVideo(\VideosBundle\Entity\Video $video) {
        $video->setVideos($this);
        $this->videos[] = $video;

        return $this;
    }

    /**
     * Remove video
     *
     * @param \VideosBundle\Entity\Video $video
     */
    public function removeVideo(\VideosBundle\Entity\Video $video) {
        $video->setVideos(null);
        $this->videos->removeElement($video);
    }

    /**
     * Get videos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideos() {
        return $this->videos;
    }

    
    
    

    /**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set created
	 *
	 * @param \DateTime $created
	 * @return Publish
	 */
	public function setCreated($created) {
		$this->created = $created;

		return $this;
	}

	/**
	 * Get created
	 *
	 * @return \DateTime
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Set updated
	 *
	 * @param \DateTime $updated
	 * @return Publish
	 */
	public function setUpdated($updated) {
		$this->updated = $updated;

		return $this;
	}

	/**
	 * Get updated
	 *
	 * @return \DateTime
	 */
	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * Set active
	 *
	 * @param boolean $active
	 * @return Publish
	 */
	public function setActive($active) {
		$this->active = $active;

		return $this;
	}

	/**
	 * Get active
	 *
	 * @return boolean
	 */
	public function getActive() {
		return $this->active;
	}

	/**
	 * Set label
	 *
	 * @param string $label
	 * @return MenuItem
	 */
	public function setLabel($label) {
		$this->label = $label;

		return $this;
	}

	/**
	 * Get label
	 *
	 * @return string
	 */
	public function getLabel() {
		return $this->label;
	}



	public function __toString() {
		return $this->label;
	}

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function isPrivate()
    {
        return $this->private;
    }

    /**
     * @param mixed $private
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    }


}
