<?php

	namespace VideosBundle\Form;

	use Doctrine\ORM\EntityRepository;
	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;

	class VideoType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder

//                ->add('videos', 'entity_select2', array(
//                    'label' => "Videos",
//                    'class' => 'VideosBundle:Videos',
//                    'query_builder'   => function (EntityRepository $er) {
//                        return $er->getVideosQuery();
//                    },
//                    'multiple'  => false,
//                    'empty_value' => "Sectionnez le videos",
//                ))

                ->add('label', null, array(
					'label' => "Nom",
				))

				->add('description', 'ckeditor', array(
					'label' => "Description",
                    'options' => array(
                        'customConfig' => '../../../bundles/app/js/ckeditor_config.js',
                    ),
				))

				->add('video', null, array(
					'label' => "Youtube ID",
					'required' => true,
				))

				->add('active', null, array(
					'label' => "Activé",
					'required'      => false,
				));

		}



		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'VideosBundle\Entity\Video',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'videobundle_video';
		}
	}
