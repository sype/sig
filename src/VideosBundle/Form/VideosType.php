<?php

	namespace VideosBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;

	class VideosType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('label', null, array(
					'label' => "Nom",
				))

				->add('active', null, array(
					'label' => "Activé",
					'required'      => false,
				));

		}



		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'VideosBundle\Entity\Videos',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'videosbundle_videos';
		}
	}
