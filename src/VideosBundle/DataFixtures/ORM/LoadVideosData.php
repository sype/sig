<?php

namespace VideosBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NavigationBundle\Entity\MenuItem;
use VideosBundle\Entity\Videos;

class LoadVideosData implements FixtureInterface {

    private $data_menus = array(
        array(
            'label' => "Videos d'accueil",
            'slug' => 'site-video',
            'private' => true
        ),

    );

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager) {

        foreach ($this->data_menus as $data_menu) {
            $menuItem = new Videos();
            $menuItem->setLabel($data_menu['label']);
            $menuItem->setSlug($data_menu['slug']);
            $menuItem->setPrivate($data_menu['private']);
            $manager->persist($menuItem);
        }
        $manager->flush();
    }

}
