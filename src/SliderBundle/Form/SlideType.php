<?php

	namespace SliderBundle\Form;

	use Doctrine\ORM\EntityRepository;
	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;

	class SlideType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder

//                ->add('slider', 'entity_select2', array(
//                    'label' => "Slider",
//                    'class' => 'SliderBundle:Slider',
//                    'query_builder'   => function (EntityRepository $er) {
//                        return $er->getSliderQuery();
//                    },
//                    'multiple'  => false,
//                    'empty_value' => "Sectionnez le slider",
//                ))

                ->add('label', null, array(
					'label' => "Nom",
				))

				->add('description', 'ckeditor', array(
					'label' => "Description",
                    'options' => array(
                        'customConfig' => '../../../bundles/app/js/ckeditor_config.js',
                    ),
				))

				->add('route', 'routes', array(
					'label' => "Route",
					'required' => false,
				))

				->add('imageFile', 'vich_file', array(
						'label'         => "Photo",
						'required'      => false,
						'allow_delete'  => true, // not mandatory, default is true
						'download_link' => true, // not mandatory, default is true
						'required'      => false,
					)
				)

				->add('active', null, array(
					'label' => "Activé",
					'required'      => false,
				));

		}



		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'SliderBundle\Entity\Slide',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'slidebundle_slide';
		}
	}
