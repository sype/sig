<?php

	namespace SliderBundle\Entity;

	use Doctrine\ORM\Mapping as ORM;
	use Gedmo\Mapping\Annotation as Gedmo;
	use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\Validator\Constraints\NotBlank;
    use Vich\UploaderBundle\Mapping\Annotation as Vich;

	/**
	 * Slide
	 *
	 * @ORM\Table()
	 * @ORM\Entity(repositoryClass="SliderBundle\Entity\SlideRepository")
	 * @Vich\Uploadable
	 */
	class Slide {

		/**
		 * @var integer
		 *
		 * @ORM\Column(name="id", type="integer")
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="AUTO")
		 */
		protected $id;

		/**
		 * @ORM\Column(name="label", type="string", length=64, nullable=false)
         * @NotBlank()
		 */
		protected $label;

		/**
		 * @ORM\Column(name="description", type="text", nullable=false)
         * @NotBlank()
		 */
		protected $description;


        /**
         * @ORM\ManyToOne(targetEntity="SliderBundle\Entity\Slider", inversedBy="slides", cascade={"persist"})
         * @ORM\JoinColumn(name="slider_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
         */
        protected $slider;

		/**
		 * @ORM\Column(name="route", type="json_array", nullable=true)
		 */
		protected $route;


		/**
		 * @var datetime $created
		 *
		 * @Gedmo\Timestampable(on="create")
		 * @ORM\Column(type="datetime")
		 */
		protected $created;

		/**
		 * @var datetime $updated
		 *
		 * @Gedmo\Timestampable(on="update")
		 * @ORM\Column(type="datetime")
		 */
		protected $updated;

		/**
		 * @ORM\Column(name="active", type="boolean")
		 */
		protected $active = true;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="image", type="string", length=255, nullable=true)
		 */
		protected $image;

		/**
		 * @var File $imageFile
		 * @Vich\UploadableField(mapping="slide_image", fileNameProperty="image")
		 */
		protected $imageFile;



		public function __toString() {
			return $this->label;
		}



		/**
		 * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
		 * of 'UploadedFile' is injected into this setter to trigger the  update. If this
		 * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
		 * must be able to accept an instance of 'File' as the bundle will inject one here
		 * during Doctrine hydration.
		 *
		 * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
		 */
		public function setImageFile(File $image) {
			$this->imageFile = $image;

			if ($image) {
				// It is required that at least one field changes if you are using doctrine
				// otherwise the event listeners won't be called and the file is lost
				$this->updatedAt = new \DateTime('now');
			}
		}

		/**
		 * @return File
		 */
		public function getImageFile() {
			return $this->imageFile;
		}

		/**
		 * Set image path
		 *
		 * @param string $image
		 *
		 * @return Modele
		 */
		public function setImage($image = null) {
			$this->image = $image;

			return $this;
		}

		/**
		 * Get image path
		 *
		 * @return string $image
		 */
		public function getImage() {
			return $this->image;
		}


		/**
		 * Get id
		 *
		 * @return integer
		 */
		public function getId() {
			return $this->id;
		}

		/**
		 * Set created
		 *
		 * @param \DateTime $created
		 *
		 * @return Publish
		 */
		public function setCreated($created) {
			$this->created = $created;

			return $this;
		}

		/**
		 * Get created
		 *
		 * @return \DateTime
		 */
		public function getCreated() {
			return $this->created;
		}

		/**
		 * Set updated
		 *
		 * @param \DateTime $updated
		 *
		 * @return Publish
		 */
		public function setUpdated($updated) {
			$this->updated = $updated;

			return $this;
		}

		/**
		 * Get updated
		 *
		 * @return \DateTime
		 */
		public function getUpdated() {
			return $this->updated;
		}

		/**
		 * Set active
		 *
		 * @param boolean $active
		 *
		 * @return Publish
		 */
		public function setActive($active) {
			$this->active = $active;

			return $this;
		}

		/**
		 * Get active
		 *
		 * @return boolean
		 */
		public function getActive() {
			return $this->active;
		}

		/**
		 * Set label
		 *
		 * @param string $label
		 *
		 * @return MenuItem
		 */
		public function setLabel($label) {
			$this->label = $label;

			return $this;
		}

		/**
		 * Get label
		 *
		 * @return string
		 */
		public function getLabel() {
			return $this->label;
		}


		/**
		 * @return mixed
		 */
		public function getRoute() {
			return $this->route;
		}

		/**
		 * @param mixed $route
		 */
		public function setRoute($route) {
			$this->route = $route;
		}

		/**
		 * @return mixed
		 */
		public function getDescription() {
			return $this->description;
		}

		/**
		 * @param mixed $description
		 */
		public function setDescription($description) {
			$this->description = $description;
		}


        /**
         * Set slider
         *
         * @param \SliderBundle\Entity\Slider
         * @return Slide
         */
        public function setSlider(\SliderBundle\Entity\Slider $slider = null) {
            $this->slider = $slider;
            return $this;
        }

        /**
         * Get slider
         *
         * @return \SliderBundle\Entity\Slider
         */
        public function getSlider() {
            return $this->slider;
        }

	}
