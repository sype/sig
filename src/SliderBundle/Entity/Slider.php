<?php

namespace SliderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Slider
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="SliderBundle\Entity\SliderRepository")
 */
class Slider {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\Column(name="label", type="string", length=64)
	 */
	protected $label;

    /**
     * @ORM\Column(name="slug", type="string", length=64, unique=false, nullable=true)
     * @Gedmo\Slug(fields={"label"}, updatable=false, unique=true)
     *
     */
    protected $slug;

    /**
     * @ORM\Column(name="private", type="boolean")
     */
    protected $private = false;


    /**
     *
     * @ORM\OneToMany(targetEntity="SliderBundle\Entity\Slide", mappedBy="slider", cascade={"persist", "remove"})
     */
    protected $slides;

	/**
	 * @var datetime $created
	 *
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var datetime $updated
	 *
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(type="datetime")
	 */
	protected $updated;

	/**
	 * @ORM\Column(name="active", type="boolean")
	 */
	protected $active = true;



    /**
     * Constructor
     */
    public function __construct() {
        $this->slides = new \Doctrine\Common\Collections\ArrayCollection();
    }



    /**
     * Add slide
     *
     * @param \SliderBundle\Entity\Slide $slide
     * @return Slider
     */
    public function addSlide(\SliderBundle\Entity\Slide $slide) {
        $slide->setSlider($this);
        $this->slides[] = $slide;

        return $this;
    }

    /**
     * Remove slide
     *
     * @param \SliderBundle\Entity\Slide $slide
     */
    public function removeSlide(\SliderBundle\Entity\Slide $slide) {
        $slide->setSlider(null);
        $this->slides->removeElement($slide);
    }

    /**
     * Get slides
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSlides() {
        return $this->slides;
    }

    
    
    

    /**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set created
	 *
	 * @param \DateTime $created
	 * @return Publish
	 */
	public function setCreated($created) {
		$this->created = $created;

		return $this;
	}

	/**
	 * Get created
	 *
	 * @return \DateTime
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Set updated
	 *
	 * @param \DateTime $updated
	 * @return Publish
	 */
	public function setUpdated($updated) {
		$this->updated = $updated;

		return $this;
	}

	/**
	 * Get updated
	 *
	 * @return \DateTime
	 */
	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * Set active
	 *
	 * @param boolean $active
	 * @return Publish
	 */
	public function setActive($active) {
		$this->active = $active;

		return $this;
	}

	/**
	 * Get active
	 *
	 * @return boolean
	 */
	public function getActive() {
		return $this->active;
	}

	/**
	 * Set label
	 *
	 * @param string $label
	 * @return MenuItem
	 */
	public function setLabel($label) {
		$this->label = $label;

		return $this;
	}

	/**
	 * Get label
	 *
	 * @return string
	 */
	public function getLabel() {
		return $this->label;
	}



	public function __toString() {
		return $this->label;
	}

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function isPrivate()
    {
        return $this->private;
    }

    /**
     * @param mixed $private
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    }


}
