<?php

namespace SliderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NavigationBundle\Entity\MenuItem;
use SliderBundle\Entity\Slider;

class LoadSliderData implements FixtureInterface {

    private $data_menus = array(
        array(
            'label' => "Page d'accueil",
            'slug' => 'site-home',
            'private' => true
        ),

    );

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager) {

        foreach ($this->data_menus as $data_menu) {
            $menuItem = new Slider();
            $menuItem->setLabel($data_menu['label']);
            $menuItem->setSlug($data_menu['slug']);
            $menuItem->setPrivate($data_menu['private']);
            $manager->persist($menuItem);
        }
        $manager->flush();
    }

}
