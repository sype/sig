<?php

namespace LocalisationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use LocalisationBundle\Entity\Departements;
use LocalisationBundle\Entity\Communes;

class LoadLocalisationData implements FixtureInterface {

	private $data_departements = array(
		array(// row #0
			'id' => 1,
			'numero' => '971',
			'label' => 'Guadeloupe',
			'fuseau' => 'America/Guadeloupe',
		),
		array(// row #1
			'id' => 2,
			'numero' => '972',
			'label' => 'Martinique',
			'fuseau' => 'America/Martinique',
		),
		array(// row #2
			'id' => 3,
			'numero' => '973',
			'label' => 'Guyane',
			'fuseau' => 'America/Cayenne',
		),
		array(// row #3
			'id' => 4,
			'numero' => '974',
			'label' => 'Réunion',
			'fuseau' => 'Indian/Reunion',
		),
		array(// row #4
			'id' => 5,
			'numero' => '976',
			'label' => 'Mayotte',
			'fuseau' => 'Indian/Mayotte',
		),
	);
	private $data_communes = array(
		array(// row #0
			'departement_id' => 1,
			'label' => 'Abymes',
		),
		array(// row #1
			'departement_id' => 1,
			'label' => 'Anse-Bertrand',
		),
		array(// row #2
			'departement_id' => 1,
			'label' => 'Baie-Mahault',
		),
		array(// row #3
			'departement_id' => 1,
			'label' => 'Baillif',
		),
		array(// row #4
			'departement_id' => 1,
			'label' => 'Basse-Terre',
		),
		array(// row #5
			'departement_id' => 1,
			'label' => 'Bouillante',
		),
		array(// row #6
			'departement_id' => 1,
			'label' => 'Capesterre-Belle-Eau',
		),
		array(// row #7
			'departement_id' => 1,
			'label' => 'Capesterre-de-Marie-Galante',
		),
		array(// row #8
			'departement_id' => 1,
			'label' => 'Gourbeyre',
		),
		array(// row #9
			'departement_id' => 1,
			'label' => 'Désirade',
		),
		array(// row #10
			'departement_id' => 1,
			'label' => 'Deshaies',
		),
		array(// row #11
			'departement_id' => 1,
			'label' => 'Grand-Bourg',
		),
		array(// row #12
			'departement_id' => 1,
			'label' => 'Gosier',
		),
		array(// row #13
			'departement_id' => 1,
			'label' => 'Goyave',
		),
		array(// row #14
			'departement_id' => 1,
			'label' => 'Lamentin',
		),
		array(// row #15
			'departement_id' => 1,
			'label' => 'Morne-à-l\'Eau',
		),
		array(// row #16
			'departement_id' => 1,
			'label' => 'Moule',
		),
		array(// row #17
			'departement_id' => 1,
			'label' => 'Petit-Bourg',
		),
		array(// row #18
			'departement_id' => 1,
			'label' => 'Petit-Canal',
		),
		array(// row #19
			'departement_id' => 1,
			'label' => 'Pointe-à-Pitre',
		),
		array(// row #20
			'departement_id' => 1,
			'label' => 'Pointe-Noire',
		),
		array(// row #21
			'departement_id' => 1,
			'label' => 'Port-Louis',
		),
		array(// row #22
			'departement_id' => 1,
			'label' => 'Saint-Claude',
		),
		array(// row #23
			'departement_id' => 1,
			'label' => 'Saint-François',
		),
		array(// row #24
			'departement_id' => 1,
			'label' => 'Saint-Louis',
		),
		array(// row #25
			'departement_id' => 1,
			'label' => 'Sainte-Anne',
		),
		array(// row #26
			'departement_id' => 1,
			'label' => 'Sainte-Rose',
		),
		array(// row #27
			'departement_id' => 1,
			'label' => 'Terre-de-Bas',
		),
		array(// row #28
			'departement_id' => 1,
			'label' => 'Terre-de-Haut',
		),
		array(// row #29
			'departement_id' => 1,
			'label' => 'Trois-Rivières',
		),
		array(// row #30
			'departement_id' => 1,
			'label' => 'Vieux-Fort',
		),
		array(// row #31
			'departement_id' => 1,
			'label' => 'Vieux-Habitants',
		),
		array(// row #32
			'departement_id' => 2,
			'label' => 'Ajoupa-Bouillon',
		),
		array(// row #33
			'departement_id' => 2,
			'label' => 'Anses-d\'Arlet',
		),
		array(// row #34
			'departement_id' => 2,
			'label' => 'Basse-Pointe',
		),
		array(// row #35
			'departement_id' => 2,
			'label' => 'Carbet',
		),
		array(// row #36
			'departement_id' => 2,
			'label' => 'Case-Pilote',
		),
		array(// row #37
			'departement_id' => 2,
			'label' => 'Diamant',
		),
		array(// row #38
			'departement_id' => 2,
			'label' => 'Ducos',
		),
		array(// row #39
			'departement_id' => 2,
			'label' => 'Fonds-Saint-Denis',
		),
		array(// row #40
			'departement_id' => 2,
			'label' => 'Fort-de-France',
		),
		array(// row #41
			'departement_id' => 2,
			'label' => 'François',
		),
		array(// row #42
			'departement_id' => 2,
			'label' => 'Grand\'Rivière',
		),
		array(// row #43
			'departement_id' => 2,
			'label' => 'Gros-Morne',
		),
		array(// row #44
			'departement_id' => 2,
			'label' => 'Lamentin',
		),
		array(// row #45
			'departement_id' => 2,
			'label' => 'Lorrain',
		),
		array(// row #46
			'departement_id' => 2,
			'label' => 'Macouba',
		),
		array(// row #47
			'departement_id' => 2,
			'label' => 'Marigot',
		),
		array(// row #48
			'departement_id' => 2,
			'label' => 'Marin',
		),
		array(// row #49
			'departement_id' => 2,
			'label' => 'Morne-Rouge',
		),
		array(// row #50
			'departement_id' => 2,
			'label' => 'Prêcheur',
		),
		array(// row #51
			'departement_id' => 2,
			'label' => 'Rivière-Pilote',
		),
		array(// row #52
			'departement_id' => 2,
			'label' => 'Rivière-Salée',
		),
		array(// row #53
			'departement_id' => 2,
			'label' => 'Robert',
		),
		array(// row #54
			'departement_id' => 2,
			'label' => 'Saint-Esprit',
		),
		array(// row #55
			'departement_id' => 2,
			'label' => 'Saint-Joseph',
		),
		array(// row #56
			'departement_id' => 2,
			'label' => 'Saint-Pierre',
		),
		array(// row #57
			'departement_id' => 2,
			'label' => 'Sainte-Anne',
		),
		array(// row #58
			'departement_id' => 2,
			'label' => 'Sainte-Luce',
		),
		array(// row #59
			'departement_id' => 2,
			'label' => 'Sainte-Marie',
		),
		array(// row #60
			'departement_id' => 2,
			'label' => 'Schœlcher',
		),
		array(// row #61
			'departement_id' => 2,
			'label' => 'Trinité',
		),
		array(// row #62
			'departement_id' => 2,
			'label' => 'Trois-Îlets',
		),
		array(// row #63
			'departement_id' => 2,
			'label' => 'Vauclin',
		),
		array(// row #64
			'departement_id' => 2,
			'label' => 'Morne-Vert',
		),
		array(// row #65
			'departement_id' => 2,
			'label' => 'Bellefontaine',
		),
		array(// row #66
			'departement_id' => 3,
			'label' => 'Régina',
		),
		array(// row #67
			'departement_id' => 3,
			'label' => 'Cayenne',
		),
		array(// row #68
			'departement_id' => 3,
			'label' => 'Iracoubo',
		),
		array(// row #69
			'departement_id' => 3,
			'label' => 'Kourou',
		),
		array(// row #70
			'departement_id' => 3,
			'label' => 'Macouria',
		),
		array(// row #71
			'departement_id' => 3,
			'label' => 'Mana',
		),
		array(// row #72
			'departement_id' => 3,
			'label' => 'Matoury',
		),
		array(// row #73
			'departement_id' => 3,
			'label' => 'Saint-Georges',
		),
		array(// row #74
			'departement_id' => 3,
			'label' => 'Remire-Montjoly',
		),
		array(// row #75
			'departement_id' => 3,
			'label' => 'Roura',
		),
		array(// row #76
			'departement_id' => 3,
			'label' => 'Saint-Laurent-du-Maroni',
		),
		array(// row #77
			'departement_id' => 3,
			'label' => 'Sinnamary',
		),
		array(// row #78
			'departement_id' => 3,
			'label' => 'Montsinéry-Tonnegrande',
		),
		array(// row #79
			'departement_id' => 3,
			'label' => 'Ouanary',
		),
		array(// row #80
			'departement_id' => 3,
			'label' => 'Approuague (chef-lieu Régina)',
		),
		array(// row #81
			'departement_id' => 3,
			'label' => 'Saül',
		),
		array(// row #82
			'departement_id' => 3,
			'label' => 'Maripasoula',
		),
		array(// row #83
			'departement_id' => 3,
			'label' => 'Samson (chef-lieu Dégrad-Samson)',
		),
		array(// row #84
			'departement_id' => 3,
			'label' => 'Moyenne-Mana (chef-lieu Mana)',
		),
		array(// row #85
			'departement_id' => 3,
			'label' => 'Camopi',
		),
		array(// row #86
			'departement_id' => 3,
			'label' => 'Grand-Santi',
		),
		array(// row #87
			'departement_id' => 3,
			'label' => 'Saint-Élie',
		),
		array(// row #88
			'departement_id' => 3,
			'label' => 'Comté (chef-lieu Dégrad-Edmond)',
		),
		array(// row #89
			'departement_id' => 3,
			'label' => 'Apatou',
		),
		array(// row #90
			'departement_id' => 3,
			'label' => 'Awala-Yalimapo',
		),
		array(// row #91
			'departement_id' => 3,
			'label' => 'Papaichton',
		),
		array(// row #92
			'departement_id' => 4,
			'label' => 'Les Avirons',
		),
		array(// row #93
			'departement_id' => 4,
			'label' => 'Bras-Panon',
		),
		array(// row #94
			'departement_id' => 4,
			'label' => 'Cilaos',
		),
		array(// row #95
			'departement_id' => 4,
			'label' => 'Entre-Deux',
		),
		array(// row #96
			'departement_id' => 4,
			'label' => 'L\'Étang-Salé',
		),
		array(// row #97
			'departement_id' => 4,
			'label' => 'Petite-Île',
		),
		array(// row #98
			'departement_id' => 4,
			'label' => 'La Plaine-des-Palmistes',
		),
		array(// row #99
			'departement_id' => 4,
			'label' => 'Le Port',
		),
		array(// row #100
			'departement_id' => 4,
			'label' => 'La Possession',
		),
		array(// row #101
			'departement_id' => 4,
			'label' => 'Saint-André',
		),
		array(// row #102
			'departement_id' => 4,
			'label' => 'Saint-Benoît',
		),
		array(// row #103
			'departement_id' => 4,
			'label' => 'Saint-Denis',
		),
		array(// row #104
			'departement_id' => 4,
			'label' => 'Saint-Joseph',
		),
		array(// row #105
			'departement_id' => 4,
			'label' => 'Saint-Leu',
		),
		array(// row #106
			'departement_id' => 4,
			'label' => 'Saint-Louis',
		),
		array(// row #107
			'departement_id' => 4,
			'label' => 'Saint-Paul',
		),
		array(// row #108
			'departement_id' => 4,
			'label' => 'Saint-Pierre',
		),
		array(// row #109
			'departement_id' => 4,
			'label' => 'Saint-Philippe',
		),
		array(// row #110
			'departement_id' => 4,
			'label' => 'Sainte-Marie',
		),
		array(// row #111
			'departement_id' => 4,
			'label' => 'Sainte-Rose',
		),
		array(// row #112
			'departement_id' => 4,
			'label' => 'Sainte-Suzanne',
		),
		array(// row #113
			'departement_id' => 4,
			'label' => 'Salazie',
		),
		array(// row #114
			'departement_id' => 4,
			'label' => 'Le Tampon',
		),
		array(// row #115
			'departement_id' => 4,
			'label' => 'Les Trois-Bassins',
		),
		array(// row #116
			'departement_id' => 5,
			'label' => 'Acoua',
		),
		array(// row #117
			'departement_id' => 5,
			'label' => 'Bandraboua',
		),
		array(// row #118
			'departement_id' => 5,
			'label' => 'Bandrele',
		),
		array(// row #119
			'departement_id' => 5,
			'label' => 'Bouéni',
		),
		array(// row #120
			'departement_id' => 5,
			'label' => 'Chiconi',
		),
		array(// row #121
			'departement_id' => 5,
			'label' => 'Chirongui',
		),
		array(// row #122
			'departement_id' => 5,
			'label' => 'Dembeni',
		),
		array(// row #123
			'departement_id' => 5,
			'label' => 'Dzaoudzi',
		),
		array(// row #124
			'departement_id' => 5,
			'label' => 'Kani-Kéli',
		),
		array(// row #125
			'departement_id' => 5,
			'label' => 'Koungou',
		),
		array(// row #126
			'departement_id' => 5,
			'label' => 'Mamoudzou',
		),
		array(// row #127
			'departement_id' => 5,
			'label' => 'Mtsamboro',
		),
		array(// row #128
			'departement_id' => 5,
			'label' => 'M\'Tsangamouji',
		),
		array(// row #129
			'departement_id' => 5,
			'label' => 'Ouangani',
		),
		array(// row #130
			'departement_id' => 5,
			'label' => 'Pamandzi',
		),
		array(// row #131
			'departement_id' => 5,
			'label' => 'Sada',
		),
		array(// row #132
			'departement_id' => 5,
			'label' => 'Tsingoni',
		),
	);

	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager) {

		foreach ($this->data_departements as $data_departement) {
			$departement = new Departements();
			$departement->setFuseau($data_departement['fuseau']);
			$departement->setLabel($data_departement['label']);
			$departement->setNumero($data_departement['numero']);
			$manager->persist($departement);

			foreach ($this->data_communes as $data_commune) {
				if ($data_commune["departement_id"] == $data_departement['id']) {
					$commune = new Communes();
					$commune->setLabel($data_commune["label"]);
					$commune->setDepartement($departement);
					$commune->setCp($departement->getNumero());
					$departement->addCommunes($commune);
					$manager->persist($commune);
				}
			}
		}
		$manager->flush();
	}

}
