<?php

	namespace LocalisationBundle\Form;

	use Doctrine\ORM\EntityRepository;
	use LocalisationBundle\Entity\Communes;
	use LocalisationBundle\Entity\Departements;
	use LocalisationBundle\Entity\Sections;
	use Uneak\AssetsManagerBundle\Assets\AssetBuilder;
	use Uneak\AssetsManagerBundle\Assets\Js\AssetInternalJs;
	use Uneak\FormsManagerBundle\Forms\AssetsComponentType;
	use UserBundle\Validator\Password;
	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\FormEvent;
	use Symfony\Component\Form\FormEvents;
	use Symfony\Component\Form\FormInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Callback;
	use Symfony\Component\Validator\Constraints\Collection;
	use Symfony\Component\Validator\Context\ExecutionContextInterface;

	class SectionType extends AssetsComponentType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
//                ->add('commune', 'entity_select2', array(
//                    'label' => "Commune",
//                    'class' => 'LocalisationBundle:Communes',
//                    'property' => 'label',
//
//                    'options' => array(
//                        'placeholder' => "Sectionnez le commune",
//                        'allowClear' => true,
//                        'maximumSelectionLength' => 2,
//                        'language' => 'fr'
//                    ),
//                    'multiple'  => false,
////					'expanded'  => true,
//
//                    'empty_value' => 'Sectionnez le commune',
//                ))

				->add('label', null, array(
					'label' => "Nom",
				))

				->add('location', 'uneak_googlemap', array(
					'label' => "Position géographique",
					'search_fields' => array('departement', 'commune', 'label'),
				))

				->add('active', null, array(
					'label' => "Activé",
					'required'      => false,
				));


			$factory = $builder->getFormFactory();

			$refreshCommunes = function ($form, $departement) use ($factory) {

				$form->add($factory->createNamed('commune', 'entity_select2', null, array(
					'class'           => 'LocalisationBundle:Communes',
					'property'        => 'label',
					'empty_value'     => 'Sélectionnez votre commune',
					'auto_initialize' => false,
					'required'        => true,
					'attr'            => array(
						'input_group' => array(
							'prepend' => '.icon-map-marker'
						)
					),
					'query_builder'   => function (EntityRepository $er) use ($departement) {
						$qb = $er->getCommunesQuery();

						if ($departement instanceof Departements) {
							$qb->where('communes.departement = :departement')->setParameter('departement', $departement);
						} elseif (is_numeric($departement)) {
							$qb->where('departement.id = :departement')->setParameter('departement', $departement);
						} else {
							$qb->where('departement.id = :departement')->setParameter('departement', 0);
						}

						return $qb;
					}
				)));
			};

			$setDepartement = function ($form, $departement) use ($factory) {
				$form->add($factory->createNamed('departement', 'entity_select2', null, array(
					'class'           => 'LocalisationBundle:Departements',
					'property'        => 'label',
					'query_builder'   => function (EntityRepository $er) {
						return $er->getDepartementsQuery();
					},
					'empty_value'     => 'Sélectionnez votre département',
					'empty_data'      => null,
					'auto_initialize' => false,
					'data'            => $departement,
					'label'           => 'Département',
					'required'        => true,
					'mapped'          => false,
					'attr'            => array(
						'input_group' => array(
							'prepend' => '.icon-globe'
						)
					)
				)));
			};


			$builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($refreshCommunes, $setDepartement) {
				$form = $event->getForm();
				$data = $event->getData();


				if ($data == null) {
					return;
				}


				if ($data instanceof Sections) {

					$departement = ($data->getCommune() && $data->getId()) ? $data->getCommune()->getDepartement() : null;
					$refreshCommunes($form, $departement);
					$setDepartement($form, $departement);

				}
			});

			$builder->addEventListener(FormEvents::PRE_BIND, function (FormEvent $event) use ($refreshCommunes) {
				$form = $event->getForm();
				$data = $event->getData();

				if (array_key_exists('departement', $data)) {
					$refreshCommunes($form, $data['departement']);
				}
			});


		}


		public function buildAsset(AssetBuilder $builder, $parameters) {

			$builder
				->add("script_projet", new AssetInternalJs(), array(
					"template" => "LocalisationBundle:Form:localisation_script.html.twig",
					"parameters" => array('item' => $parameters)
				));

		}



		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'LocalisationBundle\Entity\Sections',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'locationbundle_section';
		}
	}
