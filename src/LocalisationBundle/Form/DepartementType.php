<?php

	namespace LocalisationBundle\Form;

	use UserBundle\Validator\Password;
	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\FormEvent;
	use Symfony\Component\Form\FormEvents;
	use Symfony\Component\Form\FormInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Callback;
	use Symfony\Component\Validator\Constraints\Collection;
	use Symfony\Component\Validator\Context\ExecutionContextInterface;

	class DepartementType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('label', null, array(
					'label' => "Nom",
				))

                ->add('numero', null, array(
                    'label' => "Code postal",
                ))

                ->add('fuseau', null, array(
                    'label' => "Fuseau horaire",
                ))

				->add('location', 'uneak_googlemap', array(
					'label' => "Position géographique",
                    'search_fields' => array('label'),
				))

				->add('active', null, array(
					'label' => "Activé",
					'required'      => false,
				));


		}


		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'LocalisationBundle\Entity\Departements',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'locationbundle_departement';
		}
	}
