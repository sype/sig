<?php

	namespace LocalisationBundle\Form;

	use UserBundle\Validator\Password;
	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\FormEvent;
	use Symfony\Component\Form\FormEvents;
	use Symfony\Component\Form\FormInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Callback;
	use Symfony\Component\Validator\Constraints\Collection;
	use Symfony\Component\Validator\Context\ExecutionContextInterface;

	class CommuneType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
                ->add('departement', 'entity_select2', array(
                    'label' => "Département",
                    'class' => 'LocalisationBundle:Departements',
                    'property' => 'label',

                    'options' => array(
                        'placeholder' => "Sectionnez le département",
                        'allowClear' => true,
                        'maximumSelectionLength' => 2,
                        'language' => 'fr'
                    ),
                    'multiple'  => false,
//					'expanded'  => true,

                    'empty_value' => 'Sectionnez le département',
                ))

				->add('label', null, array(
					'label' => "Nom",
				))

                ->add('cp', null, array(
                    'label' => "Code postal",
                ))

				->add('location', 'uneak_googlemap', array(
					'label' => "Position géographique",
					'search_fields' => array('departement', 'label'),
				))

				->add('active', null, array(
					'label' => "Activé",
					'required'      => false,
				));


		}


		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'LocalisationBundle\Entity\Communes',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'locationbundle_commune';
		}
	}
