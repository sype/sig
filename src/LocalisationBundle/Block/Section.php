<?php

	namespace LocalisationBundle\Block;


	use Doctrine\ORM\EntityManager;
	use LocalisationBundle\Entity\Sections;
	use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
	use Uneak\GoogleMapBundle\Block\GoogleMap;
	use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
    use Uneak\FlatSkinBundle\Block\Info\Info;
	use Uneak\FlatSkinBundle\Block\Info\Infos;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\ProfileNav;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

	class Section {

		private $blockManager;
		private $routeManager;
		private $menuHelper;
		private $vichHelper;
		private $autorization;
		private $em;

		public function __construct(Block $blockManager, FlattenRouteManager $routeManager, MenuHelper $menuHelper, UploaderHelper $vichHelper, AuthorizationChecker $autorization, EntityManager $em) {
			$this->blockManager = $blockManager;
			$this->routeManager = $routeManager;
			$this->menuHelper = $menuHelper;
			$this->vichHelper = $vichHelper;
			$this->autorization = $autorization;
			$this->em = $em;
		}


		public function getInfos(Sections $section) {


			$infos = new Infos();
			$infos
				->setColumns(1)
				->setStripeRow(true)
				->addBlock(new Info("Département", $section->getCommune()->getDepartement()->getLabel()), null, 40)
				->addBlock(new Info("Commune", $section->getCommune()->getLabel()), null, 40)
                ->addBlock(new Info("Section", $section->getLabel()), null, 30)
				->addBlock(new GoogleMap("Position", $section->getLocation(), $section->getCommune()->getDepartement()->getLabel().", ".$section->getCommune()->getLabel()), null, 10)
                ->addBlock(new Info("Activé", ($section->getActive()) ? "Oui" : "Non"), null, 0);

			$wrapper = new Wrapper();
			$wrapper
				->setTitle("Informations")
				->addBlock($infos);

			return $wrapper;
		}



		public function getNavigation() {

			$flattenCrud = $this->routeManager->getFlattenRoute('section');

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($flattenCrud->getMetaData("_label"))
				->setDescription($flattenCrud->getMetaData("_description"))
				->setColor("green")
				->setHeaderAlign('vertical');
			$profileNav->addBlock($this->getNavigationMenu());

			return $profileNav;

		}

		public function getNavigationMenu() {

			$flattenCrud = $this->routeManager->getFlattenRoute('section');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud);
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}

		public function getSectionNavigation(Sections $section) {

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($section->getLabel())
				->setDescription($section->getCommune()->getLabel())
				->setColor("green")
				->setHeaderAlign('horizontal');
			$profileNav->addBlock($this->getSectionNavigationMenu($section));

			return $profileNav;

		}

		public function getSectionNavigationMenu(Sections $section) {

			$flattenCrud = $this->routeManager->getFlattenRoute('section/subject');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('section' => $section->getId()));
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}



	}
