<?php

	namespace LocalisationBundle\Block;


	use Doctrine\ORM\EntityManager;
	use LocalisationBundle\Entity\Departements;
    use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
	use Uneak\GoogleMapBundle\Block\GoogleMap;
	use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
    use Uneak\FlatSkinBundle\Block\Info\Info;
	use Uneak\FlatSkinBundle\Block\Info\Infos;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\ProfileNav;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

	class Departement {

		private $blockManager;
		private $routeManager;
		private $menuHelper;
		private $vichHelper;
		private $autorization;
		private $em;

		public function __construct(Block $blockManager, FlattenRouteManager $routeManager, MenuHelper $menuHelper, UploaderHelper $vichHelper, AuthorizationChecker $autorization, EntityManager $em) {
			$this->blockManager = $blockManager;
			$this->routeManager = $routeManager;
			$this->menuHelper = $menuHelper;
			$this->vichHelper = $vichHelper;
			$this->autorization = $autorization;
			$this->em = $em;
		}


		public function getInfos(Departements $departement) {


			$infos = new Infos();
			$infos
				->setColumns(1)
				->setStripeRow(true)
				->addBlock(new Info("Département", $departement->getLabel()), null, 40)
                ->addBlock(new Info("Code postal", $departement->getNumero()), null, 20)
                ->addBlock(new Info("Fuseau Horaire", $departement->getFuseau()), null, 15)
                ->addBlock(new GoogleMap("Position", $departement->getLocation(), $departement->getLabel()), null, 10)
                ->addBlock(new Info("Activé", ($departement->getActive()) ? "Oui" : "Non"), null, 0);

			$wrapper = new Wrapper();
			$wrapper
				->setTitle("Informations")
				->addBlock($infos);

			return $wrapper;
		}



		public function getNavigation() {

			$flattenCrud = $this->routeManager->getFlattenRoute('departement');

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($flattenCrud->getMetaData("_label"))
				->setDescription($flattenCrud->getMetaData("_description"))
				->setColor("green")
				->setHeaderAlign('vertical');
			$profileNav->addBlock($this->getNavigationMenu());

			return $profileNav;

		}

		public function getNavigationMenu() {

			$flattenCrud = $this->routeManager->getFlattenRoute('departement');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud);
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}

		public function getDepartementNavigation(Departements $departement) {

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($departement->getLabel())
				->setDescription($departement->getNumero())
				->setColor("green")
				->setHeaderAlign('horizontal');
			$profileNav->addBlock($this->getDepartementNavigationMenu($departement));

			return $profileNav;

		}

		public function getDepartementNavigationMenu(Departements $departement) {

			$flattenCrud = $this->routeManager->getFlattenRoute('departement/subject');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('departement' => $departement->getId()));
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}



	}
