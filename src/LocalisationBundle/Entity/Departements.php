<?php

namespace LocalisationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Departements
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="LocalisationBundle\Entity\DepartementsRepository")
 */
class Departements {


	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\Column(name="label", type="string", length=64)
	 */
	protected $label;

	/**
	 * @var datetime $created
	 *
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var datetime $updated
	 *
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(type="datetime")
	 */
	protected $updated;

	/**
	 * @ORM\Column(name="active", type="boolean")
	 */
	protected $active = true;

	/**
	 * @ORM\Column(name="numero", type="string", length=10, nullable=true)
	 */
	protected $numero;

	/**
	 * @ORM\Column(name="fuseau", type="string", length=64, nullable=true)
	 */
	protected $fuseau;

    /**
     * @ORM\Column(name="location", type="json_array", nullable=true)
     */
    protected $location;

	/**
	 *
	 * @ORM\OneToMany(targetEntity="Communes", mappedBy="departement", cascade={"persist"})
	 */
	protected $communes;


	/**
	 * Constructor
	 */
	public function __construct() {
		$this->communes = new \Doctrine\Common\Collections\ArrayCollection();
	}


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set created
	 *
	 * @param \DateTime $created
	 * @return Publish
	 */
	public function setCreated($created) {
		$this->created = $created;

		return $this;
	}

	/**
	 * Get created
	 *
	 * @return \DateTime
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Set updated
	 *
	 * @param \DateTime $updated
	 * @return Publish
	 */
	public function setUpdated($updated) {
		$this->updated = $updated;

		return $this;
	}

	/**
	 * Get updated
	 *
	 * @return \DateTime
	 */
	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * Set active
	 *
	 * @param boolean $active
	 * @return Publish
	 */
	public function setActive($active) {
		$this->active = $active;

		return $this;
	}

	/**
	 * Get active
	 *
	 * @return boolean
	 */
	public function getActive() {
		return $this->active;
	}

	/**
	 * Set label
	 *
	 * @param string $label
	 * @return MenuItem
	 */
	public function setLabel($label) {
		$this->label = $label;

		return $this;
	}

	/**
	 * Get label
	 *
	 * @return string
	 */
	public function getLabel() {
		return $this->label;
	}


	public function __toString() {
		return $this->label;
	}






	/**
	 * Add communes
	 *
	 * @param \LocalisationBundle\Entity\Communes $communes
	 * @return Categorie
	 */
	public function addCommunes(\LocalisationBundle\Entity\Communes $communes) {
		$communes->setDepartement($this);
		$this->communes[] = $communes;

		return $this;
	}

	/**
	 * Remove communes
	 *
	 * @param \LocalisationBundle\Entity\Communes $communes
	 */
	public function removeCommunes(\LocalisationBundle\Entity\Communes $communes) {
		$communes->setDepartement(null);
		$this->communes->removeElement($communes);
	}

	/**
	 * Get communes
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getCommunes() {
		return $this->communes;
	}

	/**
	 * Set numero
	 *
	 * @param string $numero
	 * @return Departements
	 */
	public function setNumero($numero) {
		$this->numero = $numero;

		return $this;
	}

	/**
	 * Get numero
	 *
	 * @return string 
	 */
	public function getNumero() {
		return $this->numero;
	}

	/**
	 * Set fuseau
	 *
	 * @param string $fuseau
	 * @return Departements
	 */
	public function setFuseau($fuseau) {
		$this->fuseau = $fuseau;

		return $this;
	}

	/**
	 * Get fuseau
	 *
	 * @return string 
	 */
	public function getFuseau() {
		return $this->fuseau;
	}

    /**
     * @return mixed
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * @param mixed $newPassword
     */
    public function setLocation($location) {
        $this->location = $location;
    }

}
