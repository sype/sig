<?php

namespace LocalisationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Communes
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="LocalisationBundle\Entity\CommunesRepository")
 */
class Communes {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\Column(name="label", type="string", length=64)
	 */
	protected $label;

    /**
     * @ORM\Column(name="cp", type="string", length=5)
     */
    protected $cp;

	/**
	 * @var datetime $created
	 *
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var datetime $updated
	 *
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(type="datetime")
	 */
	protected $updated;

    /**
     * @ORM\Column(name="location", type="json_array", nullable=true)
     */
    protected $location;

	/**
	 * @ORM\Column(name="active", type="boolean")
	 */
	protected $active = true;


	/**
	 * @ORM\ManyToOne(targetEntity="Departements", inversedBy="communes", cascade={"persist"})
	 * @ORM\JoinColumn(name="departement_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
	 */
	protected $departement;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set created
	 *
	 * @param \DateTime $created
	 * @return Publish
	 */
	public function setCreated($created) {
		$this->created = $created;

		return $this;
	}

	/**
	 * Get created
	 *
	 * @return \DateTime
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Set updated
	 *
	 * @param \DateTime $updated
	 * @return Publish
	 */
	public function setUpdated($updated) {
		$this->updated = $updated;

		return $this;
	}

	/**
	 * Get updated
	 *
	 * @return \DateTime
	 */
	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * Set active
	 *
	 * @param boolean $active
	 * @return Publish
	 */
	public function setActive($active) {
		$this->active = $active;

		return $this;
	}

	/**
	 * Get active
	 *
	 * @return boolean
	 */
	public function getActive() {
		return $this->active;
	}

	/**
	 * Set label
	 *
	 * @param string $label
	 * @return MenuItem
	 */
	public function setLabel($label) {
		$this->label = $label;

		return $this;
	}

	/**
	 * Get label
	 *
	 * @return string
	 */
	public function getLabel() {
		return $this->label;
	}



	public function __toString() {
		return $this->label;
	}


	/**
	 * Set departement
	 *
	 * @param \LocalisationBundle\Entity\Departements $departement
	 * @return Game
	 */
	public function setDepartement(\LocalisationBundle\Entity\Departements $departement = null) {
		$this->departement = $departement;

		return $this;
	}

	/**
	 * Get departement
	 *
	 * @return \LocalisationBundle\Entity\Departements
	 */
	public function getDepartement() {
		return $this->departement;
	}

    /**
     * @return mixed
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * @param mixed $newPassword
     */
    public function setLocation($location) {
        $this->location = $location;
    }

    /**
     * @return mixed
     */
    public function getCp() {
        return $this->cp;
    }

    /**
     * @param mixed $cp
     */
    public function setCp($cp) {
        $this->cp = $cp;
    }


}
