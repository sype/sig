<?php

	namespace LocalisationBundle\Controller;

	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;

	class DefaultController extends Controller {


		/**
		 * @Route("/_communes/data", name="_communeByDepartementId")
		 */
		public function communesAction(Request $request) {

			$em = $this->get('doctrine')->getEntityManager();
			$repository = $em->getRepository('LocalisationBundle:Communes');
			$departement_id = $this->get('request')->query->get('data');

			$communes = $repository->findCommunesByDepartementId($departement_id);

//			$html = '';
//			$html = $html . sprintf("<option value=\"%d\">%s</option>", null, 'Sélectionnez votre commune');
//
//			foreach ($communes as $commune) {
//				$html = $html . sprintf("<option value=\"%d\">%s</option>", $commune->getId(), $commune->getLabel());
//			}
//
//			return new Response($html);

			$data = array();
			foreach ($communes as $commune) {
				$data[] = array(
					'id' => $commune->getId(),
					'text' => $commune->getLabel()
				);
			}

			return new JsonResponse($data);
		}



        /**
         * @Route("/_sections/data", name="_sectionByCommuneId")
         */
        public function sectionsAction(Request $request) {

            $em = $this->get('doctrine')->getEntityManager();
            $repository = $em->getRepository('LocalisationBundle:Sections');
            $commune_id = $this->get('request')->query->get('data');

            $sections = $repository->findSectionsByCommuneId($commune_id);

//            $html = '';
//            $html = $html . sprintf("<option value=\"%d\">%s</option>", null, 'Sélectionnez votre section');
//
//            foreach ($sections as $commune) {
//                $html = $html . sprintf("<option value=\"%d\">%s</option>", $commune->getId(), $commune->getLabel());
//            }
//
//            return new Response($html);


			$data = array();
			foreach ($sections as $section) {
				$data[] = array(
					'id' => $section->getId(),
					'text' => $section->getLabel()
				);
			}

			return new JsonResponse($data);
        }
	}
