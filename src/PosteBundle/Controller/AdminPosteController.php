<?php

	namespace PosteBundle\Controller;

	use PosteBundle\Entity\Poste;
    use PosteBundle\Entity\Postes;
    use PosteBundle\Entity\PostesRepository;
    use PosteBundle\Form\PosteType;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoute;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
	use Uneak\FlatSkinBundle\Block\Component\DataTable;
	use Uneak\FlatSkinBundle\Block\DataTable\DataTableFilters;
	use Uneak\FlatSkinBundle\Block\Form\Form;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Doctrine\ORM\Query\Expr;

	class AdminPosteController extends Controller {


		public function indexAction( FlattenRoute $route) {
			$blockManager = $this->get("uneak.blocksmanager");

			$posteBlock = $this->get('uneak.admin.poste.block.helper');
			$blockManager->addBlock($posteBlock->getNavigation(), 'navigation');

			$gridNested = $route->getNestedRoute();

			$datatable = new DataTable();
			$datatable->setAjax($route->getChild('_grid')->getRoutePath());
			$datatable->setColumns($gridNested->getColumns());

			$datatableWrapper = new Wrapper();
			$datatableWrapper
				->setTitle("Resultats")
				->setCollapsable(false)
				->addBlock($datatable);
			$blockManager->addBlock($datatableWrapper, 'datatable');

			$datatableFilterWrapper = new Wrapper();
			$datatableFilterWrapper
				->setTitle("Filtres")
				->setCollapsable(true)
				->addBlock(new DataTableFilters($datatable));
			$blockManager->addBlock($datatableFilterWrapper, 'datatable_filters');

			return $this->render('PosteBundle:Admin:index.html.twig');

		}



		public function showAction($poste) {
			$blockManager = $this->get("uneak.blocksmanager");

			$posteBlock = $this->get('uneak.admin.poste.block.helper');

			$blockManager->addBlock($posteBlock->getInfos($poste), 'infos');
			$blockManager->addBlock($posteBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($posteBlock->getPosteNavigation($poste), 'user_navigation');
            $blockManager->addBlock($posteBlock->getCollaborateur($poste), 'panelCollab');

			return $this->render('PosteBundle:Admin:show.html.twig');

		}


		public function editAction($poste, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");


			$posteBlock = $this->get('uneak.admin.poste.block.helper');
			$blockManager->addBlock($posteBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($posteBlock->getPosteNavigation($poste), 'user_navigation');

			$form = $this->createForm(new PosteType(), $poste);
			$form->add('submit', 'submit', array('label' => 'Modifier'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

					return $this->redirect($route->getChild('*/subject/show', array('poste' => $poste->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("PosteBundle:Block:Form/block_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Edition du poste ".$poste->getLabel())
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('PosteBundle:Admin:edit.html.twig');

		}


		public function newAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$posteBlock = $this->get('uneak.admin.poste.block.helper');
			$blockManager->addBlock($posteBlock->getNavigation(), 'navigation');

			$poste = new Poste();

			$form = $this->createForm(new PosteType(), $poste);
			$form->add('submit', 'submit', array('label' => 'Creer'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($poste);
                    $em->flush();

					return $this->redirect($route->getChild('*/subject/show', array('poste' => $poste->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("PosteBundle:Block:Form/block_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Création d'un poste")
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');

			return $this->render('PosteBundle:Admin:new.html.twig');


		}


		public function deleteAction($poste, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$posteBlock = $this->get('uneak.admin.poste.block.helper');
			$blockManager->addBlock($posteBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($posteBlock->getPosteNavigation($poste), 'user_navigation');

			$form = $this->createFormBuilder(array());
			$form->add('cancel', 'submit', array('label' => 'Annuler'));
			$form->add('confirm', 'submit', array('label' => 'Confirmer'));
			$form = $form->getForm();


			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');

				$form->handleRequest($request);
				if ($form->isValid()) {
					if ($form->get('cancel')->isClicked()) {
						$flash->info('Suppression annulée');

						return $this->redirect($route->getChild('*/subject/show', array('poste' => $poste->getId()))->getRoutePath());
					}
					if ($form->get('confirm')->isClicked()) {

                        $em = $this->getDoctrine()->getManager();
                        $em->remove($poste);
                        $em->flush();

						$flash->success('La suppression à été réalisée avec succès.');

						return $this->redirect($route->getChild('*/index')->getRoutePath());
					}

				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formBlock = new Form($form->createView());
			$formBlock->setTemplate("PosteBundle:Block:Form/delete_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Suppression du poste ".$poste->getLabel())
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('PosteBundle:Admin:delete.html.twig');

		}




		public function indexGridAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$gridHelper = $this->get("uneak.routesmanager.grid.helper");
			$menuHelper = $this->get("uneak.routesmanager.menu.helper");

			$params = $request->query->all();

			$gridData = $gridHelper->gridFields($gridHelper->createGridQueryBuilder('PosteBundle\Entity\Poste', $params), $params);
			$recordsTotal = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('PosteBundle\Entity\Poste', $params));
			$recordsFiltered = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('PosteBundle\Entity\Poste', $params));


			$rowMenuTemplate = "UneakFlatSkinBundle:Block:Menu/block_menu.html.twig";

			$data = array();

			foreach ($gridData as $object) {
				$row = array();
				foreach ($params['columns'] as $columns) {
					if ($columns['name'] && substr($columns['name'], 0, 1) != '_') {
						$value = $object[str_replace(".", "_", $columns['name'])];
						if ($value instanceof \DateTime) {
							$value = $value->format('d/m/Y H:m:s');
						}
						$row[$columns['data']] = $value;
					} else {
						$row[$columns['data']] = "";
					}
				}
				$row['DT_RowId'] = $object['DT_RowId'];


				$menu = new Menu();
				$menu->setTemplate($rowMenuTemplate);
				$rowActions = $route->getParent()->getNestedRoute()->getRowActions();
				$root = $menuHelper->createMenu($rowActions, $route, array('poste' => $row['DT_RowId']));
				$root->setChildrenAttribute('class', 'nav nav-pills nav-inline text-nowrap');
				$menu->setRoot($root);

				$blockManager->addBlock($menu, 'rowMenu');
				$row['_actions'] = $this->renderView("{{ renderBlock('rowMenu') }}");

				array_push($data, $row);
			}

			return new JsonResponse(array(
				'draw'            => $params["draw"],
				'recordsTotal'    => $recordsTotal,
				'recordsFiltered' => $recordsFiltered,
				'data'            => $data,
			));
		}


	}
