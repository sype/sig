<?php

	namespace PosteBundle\Block;


	use Doctrine\ORM\EntityManager;
	use PosteBundle\Entity\Poste as PosteEntity;
	use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
    use Uneak\FlatSkinBundle\Block\Panel\PanelPeople;
    use Uneak\FlatSkinBundle\Block\Panel\PanelPeoples;
    use Uneak\RoutesManagerBundle\Helper\MenuHelper;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
    use Uneak\FlatSkinBundle\Block\Info\Info;
	use Uneak\FlatSkinBundle\Block\Info\Infos;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\ProfileNav;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

	class Poste {

		private $blockManager;
		private $routeManager;
		private $menuHelper;
		private $vichHelper;
		private $autorization;
		private $em;

		public function __construct(Block $blockManager, FlattenRouteManager $routeManager, MenuHelper $menuHelper, UploaderHelper $vichHelper, AuthorizationChecker $autorization, EntityManager $em) {
			$this->blockManager = $blockManager;
			$this->routeManager = $routeManager;
			$this->menuHelper = $menuHelper;
			$this->vichHelper = $vichHelper;
			$this->autorization = $autorization;
			$this->em = $em;
		}



        public function getCollaborateur(PosteEntity $poste) {

            $posteRepo = $this->em->getRepository('AgenceBundle:AgenceCollaborateur');
            $collaborateurs = $posteRepo->findCollaborateurs(false, null, $poste);

            $panelPeoples = new PanelPeoples();
            $panelPeoples->setTitle("<i class='fa fa-icon fa-users'> Agences</i>");
            foreach ($collaborateurs as $collaborateur) {

                $flattenCrud = $this->routeManager->getFlattenRoute('agence/subject/collaborateur/subject');
                $menu = new Menu();
                $menu->setTemplate("UneakFlatSkinBundle:Block:Menu/block_menu.html.twig");
                $menu->setMeta('style', 'social-links');
                $root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('agence' => $poste->getId(), 'collaborateur' => $collaborateur->getId()) );
                $menu->setRoot($root);

                $description = $collaborateur->getEmail().'<br/>';
                $description .= $collaborateur->getPhone();

                $panelPeople = new PanelPeople();
                $panelPeople->setTitle($collaborateur->getCollaborateur()->getFirstName()." ".$collaborateur->getCollaborateur()->getLastName());
                $panelPeople->setSubtitle($collaborateur->getAgence()->getLabel());
                $panelPeople->setDescription($description);
                $panelPeople->setPhoto($this->vichHelper->asset($collaborateur->getCollaborateur(), 'imageFile'));
                $panelPeople->setMenu($menu);

                $panelPeoples->addPanel($panelPeople);
            }

            return $panelPeoples;

        }


        public function getInfos(PosteEntity $poste) {


			$infos = new Infos();
			$infos
				->setColumns(1)
				->setStripeRow(true)
                ->addBlock(new Info("Nom", $poste->getLabel()), null, 30)
                ->addBlock(new Info("Activé", ($poste->getActive()) ? "Oui" : "Non"), null, 0);

			$wrapper = new Wrapper();
			$wrapper
				->setTitle("Informations")
				->addBlock($infos);

			return $wrapper;
		}



		public function getNavigation() {

			$flattenCrud = $this->routeManager->getFlattenRoute('poste');

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($flattenCrud->getMetaData("_label"))
				->setDescription($flattenCrud->getMetaData("_description"))
				->setColor("green")
				->setHeaderAlign('vertical');
			$profileNav->addBlock($this->getNavigationMenu());

			return $profileNav;

		}

		public function getNavigationMenu() {

			$flattenCrud = $this->routeManager->getFlattenRoute('poste');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud);
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}

		public function getPosteNavigation(PosteEntity $poste) {

			$profileNav = new ProfileNav();
			$profileNav
				->setTitle($poste->getLabel())
				->setColor("green")
				->setHeaderAlign('horizontal');
			$profileNav->addBlock($this->getPosteNavigationMenu($poste));

			return $profileNav;

		}

		public function getPosteNavigationMenu(PosteEntity $poste) {

			$flattenCrud = $this->routeManager->getFlattenRoute('poste/subject');

			$menu = new Menu();
			$root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('poste' => $poste->getId()));
			$root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
			$menu->setRoot($root);

			return $menu;

		}



	}
