<?php

	namespace ContactBundle\Form;

	use Doctrine\ORM\EntityRepository;
    use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;

	class ContactType extends AbstractType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder

                ->add('service', 'entity', array(
                    'label' => false,
                    'class' => 'ContactBundle:ContactService',
                    'query_builder'   => function (EntityRepository $er) {
                        return $er->getContactServicesQuery();
                    },
                    'multiple'  => false,

                    'empty_value' => 'Sectionnez le service',
                ))

                ->add('firstName', null, array(
                    'label' => false,
                    'attr'     => array(
                    	'placeholder' => 'Prénom',
                        'input_group' => array(
                            'prepend' => '.icon-user'
                        )
                    )                    
                ))
                ->add('lastName', null, array(
                    'label' => false,
                    'attr'     => array(
                    	'placeholder' => 'Nom',
                        'input_group' => array(
                            'prepend' => '.icon-user'
                        )
                    )                    
                ))

                ->add('email', null, array(
                    'label' => false,
                    'attr'     => array(
                    	'placeholder' => 'Email',
                        'input_group' => array(
                            'prepend' => '.icon-envelope'
                        )
                    )                    
                ))

                ->add('phone', null, array(
                    'label'    => false,
                    'attr'     => array(
                    	'placeholder' => 'Numéro de téléphone',
                        'input_group' => array(
                            'prepend' => '.icon-phone'
                        )
                    )
                ))

				->add('message', null, array(
					'label' => "Message",
				));

		}



		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'ContactBundle\Entity\Contact',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'contactbundle_contact';
		}
	}
