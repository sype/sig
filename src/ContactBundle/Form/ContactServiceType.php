<?php

	namespace ContactBundle\Form;

    use Doctrine\ORM\EntityRepository;
	use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolverInterface;
	use Symfony\Component\Validator\Constraints\Collection;
	use Uneak\FormsManagerBundle\Forms\AssetsComponentType;

	class ContactServiceType extends AssetsComponentType {
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
                ->add('label', null, array(
                    'label' => "Nom",
                    'required' => true,
                ))

                ->add('collaborateurs', 'entity_select2', array(
                    'label' => "Collaborateurs",
                    'class' => 'CollaborateurBundle:Collaborateur',
                    'query_builder'   => function (EntityRepository $er) {
                        return $er->getCollaborateurQuery();
                    },
                    'options' => array(
                        'language' => 'fr'
                    ),
                    'multiple'  => true,

                    'empty_value' => 'Sectionnez les collaborateurs',
                    'required' => true,
                ))

            ->add('active', null, array(
                'label' => "Activé",
                'required'      => false,
            ));


        }



		/**
		 * @param OptionsResolverInterface $resolver
		 */
		public function setDefaultOptions(OptionsResolverInterface $resolver) {
			$resolver->setDefaults(array(
				'data_class' => 'ContactBundle\Entity\ContactService',
			));
		}


		/**
		 * @return string
		 */
		public function getName() {
			return 'contactbundle_contactservice';
		}
	}
