<?php

namespace ContactBundle\Block;


use Doctrine\ORM\EntityManager;
use ContactBundle\Entity\Contact as ContactEntity;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Uneak\RoutesManagerBundle\Helper\MenuHelper;
use Uneak\BlocksManagerBundle\Blocks\Block;
use Uneak\RoutesManagerBundle\Routes\FlattenRouteManager;
use Uneak\FlatSkinBundle\Block\Info\Info;
use Uneak\FlatSkinBundle\Block\Info\Infos;
use Uneak\FlatSkinBundle\Block\Menu\Menu;
use Uneak\FlatSkinBundle\Block\Panel\ProfileNav;
use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class Contact {

    private $blockManager;
    private $routeManager;
    private $menuHelper;
    private $vichHelper;
    private $autorization;
    private $em;

    public function __construct(Block $blockManager, FlattenRouteManager $routeManager, MenuHelper $menuHelper, UploaderHelper $vichHelper, AuthorizationChecker $autorization, EntityManager $em) {
        $this->blockManager = $blockManager;
        $this->routeManager = $routeManager;
        $this->menuHelper = $menuHelper;
        $this->vichHelper = $vichHelper;
        $this->autorization = $autorization;
        $this->em = $em;
    }


    public function getInfos(ContactEntity $contact) {


        $infos = new Infos();
        $infos
            ->setColumns(1)
            ->setStripeRow(true)
            ->addBlock(new Info("Prénom", $contact->getFirstName()), null, 50)
            ->addBlock(new Info("Nom", $contact->getLastName()), null, 40)
            ->addBlock(new Info("Email", $contact->getEmail()), null, 30)
            ->addBlock(new Info("Téléphone", $contact->getPhone()), null, 20)
			->addBlock(new Info("Service", $contact->getService()->getLabel()), null, 10)
            ->addBlock(new Info("Message", nl2br($contact->getMessage())), null, 0);

        $wrapper = new Wrapper();
        $wrapper
            ->setTitle("Informations")
            ->addBlock($infos);

        return $wrapper;
    }



    public function getNavigation() {

        $flattenCrud = $this->routeManager->getFlattenRoute('contact');

        $profileNav = new ProfileNav();
        $profileNav
            ->setTitle($flattenCrud->getMetaData("_label"))
            ->setDescription($flattenCrud->getMetaData("_description"))
            ->setColor("green")
            ->setHeaderAlign('vertical');
        $profileNav->addBlock($this->getNavigationMenu());

        return $profileNav;

    }

    public function getNavigationMenu() {

        $flattenCrud = $this->routeManager->getFlattenRoute('contact');

        $menu = new Menu();
        $root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud);
        $root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
        $menu->setRoot($root);

        return $menu;

    }

    public function getContactNavigation(ContactEntity $contact) {

        $profileNav = new ProfileNav();
        $profileNav
            ->setTitle($contact->getFirstName()." ".$contact->getLastName())
            ->setDescription($contact->getEmail())
            ->setColor("green")
            ->setHeaderAlign('horizontal');
        $profileNav->addBlock($this->getContactNavigationMenu($contact));

        return $profileNav;

    }

    public function getContactNavigationMenu(ContactEntity $contact) {

        $flattenCrud = $this->routeManager->getFlattenRoute('contact/subject');

        $menu = new Menu();
        $root = $this->menuHelper->createMenu($flattenCrud->getMetaData('_menu'), $flattenCrud, array('contact' => $contact->getId()));
        $root->setChildrenAttribute('class', 'nav nav-pills nav-stacked');
        $menu->setRoot($root);

        return $menu;

    }



}
