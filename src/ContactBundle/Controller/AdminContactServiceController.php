<?php

	namespace ContactBundle\Controller;

	use ContactBundle\Entity\ContactService;
    use ContactBundle\Entity\ContactServices;
    use ContactBundle\Entity\ContactServicesRepository;
    use ContactBundle\Form\ContactServiceType;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use Uneak\BlocksManagerBundle\Blocks\Block;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoute;
	use Uneak\RoutesManagerBundle\Routes\FlattenRoutePool;
	use Uneak\FlatSkinBundle\Block\Component\DataTable;
	use Uneak\FlatSkinBundle\Block\DataTable\DataTableFilters;
	use Uneak\FlatSkinBundle\Block\Form\Form;
	use Uneak\FlatSkinBundle\Block\Menu\Menu;
	use Uneak\FlatSkinBundle\Block\Panel\Wrapper;
	use Doctrine\ORM\Query\Expr;

	class AdminContactServiceController extends Controller {


		public function indexAction( FlattenRoute $route) {
			$blockManager = $this->get("uneak.blocksmanager");

			$contactserviceBlock = $this->get('uneak.admin.contactservice.block.helper');
			$blockManager->addBlock($contactserviceBlock->getNavigation(), 'navigation');

			$gridNested = $route->getNestedRoute();

			$datatable = new DataTable();
			$datatable->setAjax($route->getChild('_grid')->getRoutePath());
			$datatable->setColumns($gridNested->getColumns());

			$datatableWrapper = new Wrapper();
			$datatableWrapper
				->setTitle("Resultats")
				->setCollapsable(false)
				->addBlock($datatable);
			$blockManager->addBlock($datatableWrapper, 'datatable');

			$datatableFilterWrapper = new Wrapper();
			$datatableFilterWrapper
				->setTitle("Filtres")
				->setCollapsable(true)
				->addBlock(new DataTableFilters($datatable));
			$blockManager->addBlock($datatableFilterWrapper, 'datatable_filters');

			return $this->render('ContactBundle:Admin:index.html.twig');

		}



		public function showAction($contactservice) {
			$blockManager = $this->get("uneak.blocksmanager");

			$contactserviceBlock = $this->get('uneak.admin.contactservice.block.helper');

			$blockManager->addBlock($contactserviceBlock->getInfos($contactservice), 'infos');
			$blockManager->addBlock($contactserviceBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($contactserviceBlock->getContactServiceNavigation($contactservice), 'user_navigation');

			return $this->render('ContactBundle:Admin:show.html.twig');

		}


		public function editAction($contactservice, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");


			$contactserviceBlock = $this->get('uneak.admin.contactservice.block.helper');
			$blockManager->addBlock($contactserviceBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($contactserviceBlock->getContactServiceNavigation($contactservice), 'user_navigation');

			$form = $this->createForm(new ContactServiceType(), $contactservice);
			$form->add('submit', 'submit', array('label' => 'Modifier'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

					return $this->redirect($route->getChild('*/subject/show', array('contactservice' => $contactservice->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("ContactBundle:Block:Form/block_contactservice_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Edition du service ".$contactservice->getLabel())
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('ContactBundle:Admin:edit.html.twig');

		}


		public function newAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$contactserviceBlock = $this->get('uneak.admin.contactservice.block.helper');
			$blockManager->addBlock($contactserviceBlock->getNavigation(), 'navigation');

			$contactservice = new ContactService();

			$form = $this->createForm(new ContactServiceType(), $contactservice);
			$form->add('submit', 'submit', array('label' => 'Creer'));

			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');
				$form->handleRequest($request);
				if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($contactservice);
                    $em->flush();

					return $this->redirect($route->getChild('*/subject/show', array('contactservice' => $contactservice->getId()))->getRoutePath());
				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formManager = $this->get("uneak.formsmanager");

			$formBlock = new Form($formManager->createView($form));
			$formBlock->setTemplate("ContactBundle:Block:Form/block_contactservice_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Création d'un service")
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');

			return $this->render('ContactBundle:Admin:new.html.twig');


		}


		public function deleteAction($contactservice, FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$contactserviceBlock = $this->get('uneak.admin.contactservice.block.helper');
			$blockManager->addBlock($contactserviceBlock->getNavigation(), 'navigation');
			$blockManager->addBlock($contactserviceBlock->getContactServiceNavigation($contactservice), 'user_navigation');

			$form = $this->createFormBuilder(array());
			$form->add('cancel', 'submit', array('label' => 'Annuler'));
			$form->add('confirm', 'submit', array('label' => 'Confirmer'));
			$form = $form->getForm();


			if ($request->getMethod() == 'POST') {

				$flash = $this->get('braincrafted_bootstrap.flash');

				$form->handleRequest($request);
				if ($form->isValid()) {
					if ($form->get('cancel')->isClicked()) {
						$flash->info('Suppression annulée');

						return $this->redirect($route->getChild('*/subject/show', array('contactservice' => $contactservice->getId()))->getRoutePath());
					}
					if ($form->get('confirm')->isClicked()) {

                        $em = $this->getDoctrine()->getManager();
                        $em->remove($contactservice);
                        $em->flush();

						$flash->success('La suppression à été réalisée avec succès.');

						return $this->redirect($route->getChild('*/index')->getRoutePath());
					}

				} else {
					$flash->error('Votre formulaire est invalide.');
				}
			}


			$formBlock = new Form($form->createView());
			$formBlock->setTemplate("ContactBundle:Block:Form/delete_form.html.twig");

			$formWrapper = new Wrapper();
			$formWrapper
				->setTitle("Suppression du service ".$contactservice->getLabel())
				->addBlock($formBlock);
			$blockManager->addBlock($formWrapper, 'form');


			return $this->render('ContactBundle:Admin:delete.html.twig');

		}




		public function indexGridAction( FlattenRoute $route, Request $request) {
			$blockManager = $this->get("uneak.blocksmanager");

			$gridHelper = $this->get("uneak.routesmanager.grid.helper");
			$menuHelper = $this->get("uneak.routesmanager.menu.helper");

			$params = $request->query->all();

			$gridData = $gridHelper->gridFields($gridHelper->createGridQueryBuilder('ContactBundle\Entity\ContactService', $params), $params);
			$recordsTotal = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('ContactBundle\Entity\ContactService', $params));
			$recordsFiltered = $gridHelper->gridFieldsCount($gridHelper->createGridQueryBuilder('ContactBundle\Entity\ContactService', $params));


			$rowMenuTemplate = "UneakFlatSkinBundle:Block:Menu/block_menu.html.twig";

			$data = array();

			foreach ($gridData as $object) {
				$row = array();
				foreach ($params['columns'] as $columns) {
					if ($columns['name'] && substr($columns['name'], 0, 1) != '_') {
						$value = $object[str_replace(".", "_", $columns['name'])];
						if ($value instanceof \DateTime) {
							$value = $value->format('d/m/Y H:m:s');
						}
						$row[$columns['data']] = $value;
					} else {
						$row[$columns['data']] = "";
					}
				}
				$row['DT_RowId'] = $object['DT_RowId'];


				$menu = new Menu();
				$menu->setTemplate($rowMenuTemplate);
				$rowActions = $route->getParent()->getNestedRoute()->getRowActions();
				$root = $menuHelper->createMenu($rowActions, $route, array('contactservice' => $row['DT_RowId']));
				$root->setChildrenAttribute('class', 'nav nav-pills nav-inline text-nowrap');
				$menu->setRoot($root);

				$blockManager->addBlock($menu, 'rowMenu');
				$row['_actions'] = $this->renderView("{{ renderBlock('rowMenu') }}");

				array_push($data, $row);
			}

			return new JsonResponse(array(
				'draw'            => $params["draw"],
				'recordsTotal'    => $recordsTotal,
				'recordsFiltered' => $recordsFiltered,
				'data'            => $data,
			));
		}


	}
