<?php

namespace ContactBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ContactService
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ContactBundle\Entity\ContactServiceRepository")
 */
class ContactService {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var datetime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    /**
     * @ORM\Column(name="label", type="string", length=64, nullable=false)
     * @Assert\NotBlank()
     */
    protected $label;

    /**
     * @ORM\Column(name="active", type="boolean")
     */
    protected $active = true;

    /**
     * @ORM\ManyToMany(targetEntity="\CollaborateurBundle\Entity\Collaborateur")
     * @ORM\JoinTable(name="ContactCollaborateur",
     *      joinColumns={@ORM\JoinColumn(name="type_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="collaborateur_id", referencedColumnName="id")}
     *      )
     **/
    protected $collaborateurs;



    /**
     * Constructor
     */
    public function __construct() {
        $this->collaborateurs = new \Doctrine\Common\Collections\ArrayCollection();
    }



    /**
     * Set active
     *
     * @param boolean $active
     * @return Publish
     */
    public function setActive($active) {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive() {
        return $this->active;
    }


    /**
     * Add collaborateurs
     *
     * @param \CollaborateurBundle\Entity\Collaborateur $collaborateur
     *
     * @return ContactService
     */
    public function addCollaborateur(\CollaborateurBundle\Entity\Collaborateur $collaborateur) {
        $this->collaborateurs[] = $collaborateur;

        return $this;
    }

    /**
     * Remove collaborateurs
     *
     * @param \CollaborateurBundle\Entity\Collaborateur $collaborateur
     */
    public function removeCollaborateur(\CollaborateurBundle\Entity\Collaborateur $collaborateur) {
        $this->collaborateurs->removeElement($collaborateur);
    }

    /**
     * Get collaborateurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCollaborateurs() {
        return $this->collaborateurs;
    }

    /**
     * Set collaborateurs
     *
     * @param \Doctrine\Common\Collections\ArrayCollection
     *
     * @return ContactService
     */
    public function setCollaborateurs(\Doctrine\Common\Collections\ArrayCollection $collaborateurs) {
        $this->$collaborateurs = $collaborateurs;

        return $this;
    }








	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set created
	 *
	 * @param \DateTime $created
	 * @return Publish
	 */
	public function setCreated($created) {
		$this->created = $created;

		return $this;
	}

	/**
	 * Get created
	 *
	 * @return \DateTime
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Set updated
	 *
	 * @param \DateTime $updated
	 * @return Publish
	 */
	public function setUpdated($updated) {
		$this->updated = $updated;

		return $this;
	}

	/**
	 * Get updated
	 *
	 * @return \DateTime
	 */
	public function getUpdated() {
		return $this->updated;
	}


    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }


    public function __toString() {
        return $this->label;
    }


}
