<?php

namespace ContactBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contact
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ContactBundle\Entity\ContactRepository")
 */
class Contact {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;


    /**
     * @ORM\Column(name="firstName", type="string", length=64, nullable=false)
     * @Assert\NotBlank()
     */
    protected $firstName;

    /**
     * @ORM\Column(name="lastName", type="string", length=64, nullable=false)
     * @Assert\NotBlank()
     */
    protected $lastName;


    /**
     * @ORM\ManyToOne(targetEntity="\ContactBundle\Entity\ContactService", cascade={"persist"})
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    protected $service;


    /**
     * @ORM\Column(name="phone", type="string", length=10, nullable=false)
     * @Assert\Regex(
     *     pattern="/^\d{10}$/",
     *     message="Le numéro de téléphone doit obligatoirement contenir 10 chiffres"
     * )
     * @Assert\NotBlank()
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     * @Assert\Email(
     *     message = "'{{ value }}' n'est pas un email valide.",
     *
     * )
     * @Assert\NotBlank()
     */
    protected $email;


    /**
     * @ORM\Column(name="message", type="text", nullable=false)
     * @Assert\NotBlank()
     */
    protected $message;

    /**
	 * @var datetime $created
	 *
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var datetime $updated
	 *
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(type="datetime")
	 */
	protected $updated;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set created
	 *
	 * @param \DateTime $created
	 * @return Publish
	 */
	public function setCreated($created) {
		$this->created = $created;

		return $this;
	}

	/**
	 * Get created
	 *
	 * @return \DateTime
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Set updated
	 *
	 * @param \DateTime $updated
	 * @return Publish
	 */
	public function setUpdated($updated) {
		$this->updated = $updated;

		return $this;
	}

	/**
	 * Get updated
	 *
	 * @return \DateTime
	 */
	public function getUpdated() {
		return $this->updated;
	}




    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    public function getPhone() {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }



    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param mixed $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }



    public function __toString() {
        return $this->firstName . " " . $this->lastName;
    }


}
