<?php

	namespace ContactBundle\Admin;

	use Uneak\RoutesManagerBundle\Routes\NestedAdminRoute;
	use Uneak\RoutesManagerBundle\Routes\NestedCRUDRoute;
	use Uneak\RoutesManagerBundle\Routes\NestedEntityRoute;
	use Uneak\RoutesManagerBundle\Routes\NestedGridRoute;

	class Contact extends NestedCRUDRoute {

		protected $entity = 'ContactBundle\Entity\Contact';

		public function initialize() {
			parent::initialize();

			$this->setMetaData('_icon', 'inbox');
			$this->setMetaData('_label', 'Contacts');
			$this->setMetaData('_description', 'Gestion des mails de contacts');
			$this->setMetaData('_menu', array(
				'index'  => '*/index',
			));
		}


		protected function buildCRUD() {
			$indexRoute = new NestedGridRoute('index');
			$indexRoute
				->setPath('')
				->setAction('index')
				->setMetaData('_icon', 'list')
				->setMetaData('_label', 'Liste')
				->addRowAction('show', '*/subject/show')
				->addRowAction('delete', '*/subject/delete')

				->addColumn(array('title' => 'Service', 'name' => 'service.label'))
				->addColumn(array('title' => 'Prénom', 'name' => 'firstName'))
				->addColumn(array('title' => 'Nom', 'name' => 'lastName'))
				->addColumn(array('title' => 'Message', 'name' => 'message'))
			;
			$this->addChild($indexRoute);


            $newRoute = new NestedAdminRoute('new');
            $newRoute
                ->setPath('new')
                ->setAction('new')
                ->setMetaData('_icon', 'plus-circle')
                ->setMetaData('_label', 'New');
            $this->addChild($newRoute);



			$subjectRoute = new NestedEntityRoute('subject');
			$subjectRoute
				->setParameterName($this->getId())
				->setParameterPattern('\d+')
				->setEnabled(false)
				->setMetaData('_menu', array(
					'show'   => '*/subject/show',
					'delete' => '*/subject/delete',
				));
			$this->addChild($subjectRoute);


			$showRoute = new NestedAdminRoute('show');
			$showRoute
				->setAction('show')
				->setMetaData('_icon', 'eye')
				->setMetaData('_label', 'Show')
				->setRequirement('_method', 'GET');
			$subjectRoute->addChild($showRoute);


			$deleteRoute = new NestedAdminRoute('delete');
			$deleteRoute
				->setAction('delete')
				->setMetaData('_icon', 'times')
				->setMetaData('_label', 'Delete');
			$subjectRoute->addChild($deleteRoute);
		}


	}
